<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */
global $eshkool_option;
get_header(); ?>
<?php get_template_part( 'inc/page-header/breadcrumbs-archive'); ?>
<div class="rs-blog">
    <div class="container">
        <div id="content">
          <div class="row">           
            <div class="col-md-8"> 
              <?php
              if ( have_posts() ) :           
                /* Start the Loop */
                while ( have_posts() ) : the_post();      
              ?>
               <article <?php post_class(); ?>>
                  <div class="col-sm-12 col-xs-12">

                    <?php 
                         if ( has_post_thumbnail() ) {
                              $featured_img_yes = ' has-featured-img';
                         }else {
                             $featured_img_yes = '';
                         } 
                    ?>

                    <div class="blog-item<?php echo esc_attr($featured_img_yes); ?>">
                      <?php if ( has_post_thumbnail() ) {?>
                         <div class="blog-img">
                            <?php
                                the_post_thumbnail();
                            ?>
                            <a href="<?php the_permalink();?>" class="blog-link"><i class="fa fa-link"></i></a>      
                        </div><!-- .blog-img -->
                      <?php
                        }       
                      ?>
                      <div class="blog-full-area">
                      <div class="full-blog-content">                         
                        <h3 class="blog-title">
                            <a href="<?php the_permalink();?>">
                                <?php the_title();?>
                            </a>
                        </h3>                           
                         <div class="blog-meta">                            
                            <div class="blog-date">                                
                                <ul class="btm-cate">

                                <?php if(get_the_date()){?>
                                    <li>
                                        <i class="fa fa-calendar"></i>
                                        <?php $post_date = get_the_date(); echo esc_attr($post_date);?>
                                    </li>
                                <?php }?> 


                                <?php if(get_the_author()){?>
                                    <li>
                                        <i class="fa fa-user-o"></i>
                                        <?php the_author();?>
                                    </li>
                                <?php }?>

                                <?php if(get_the_category()){?>
                                    <li class="category-name"><i class="fa fa-folder-open-o"></i>
                                      <?php the_category(', '); 
                                    ?>
                                  </li>
                                <?php }?>
                              
                                  <?php if(has_tag()): ?>
                                    <li>
                                        <?php
                                       //tag add
                                          $seperator = ', '; // blank instead of comma
                                          $after = '';
                                          $before = '';
                                          echo '<div class="tag-line">';
                                          the_tags( $before, $seperator, $after );
                                          echo '</div>';
                                          ?>
                                    </li>
                                      <?php endif; ?>                     
                                </ul>
                            </div>                                                     
                        </div> 
                        <div class="blog-desc">   
                          <?php the_excerpt();?>            
                        </div>
                        <?php if(!empty($eshkool_option['blog-readmore'])){ 
                          $blog_more = $eshkool_option['blog-readmore'];
                          if($blog_more == 1): ?>
                            <div class="blog-button">
                              <a href="<?php the_permalink();?>" class="readmore"><?php echo esc_attr($eshkool_option['blog_button_text']);?> <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        <?php endif; 
                        }
                     ?>
                    </div>
                  </div>
                  </div>
                </div>
            </article>
              <?php  
              endwhile;   
              ?>
              <div class="pagination-area">
                <?php
                    the_posts_pagination();
                  ?>
              </div>
              <?php
              else :
              get_template_part( 'template-parts/content', 'none' );
              endif; ?> 
          </div>
          <?php 
             get_sidebar();   
          
          ?>
        </div>  
    </div>
</div>
<?php
get_footer();