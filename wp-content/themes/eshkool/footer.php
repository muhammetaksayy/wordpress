<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */
?>
  </div><!-- .main-container -->
    <?php
          global $eshkool_option;
          get_template_part('inc/footer_style/footer');    
    ?>
  
</div><!-- #page -->

    <?php 
    if(!empty($eshkool_option['show_top_bottom'])){  
      $rstop_bottom=$eshkool_option['show_top_bottom'];
      if($rstop_bottom == 1){
        ?>
        <!-- start scrollUp  -->
        <div id="scrollUp">
           <i class="fa fa-angle-up"></i>
        </div>   
      <?php }
      }
    ?>

    <?php       
      wp_footer(); 
    ?>  
     </body>
</html>