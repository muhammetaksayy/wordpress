<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */

if ( ! function_exists( 'eshkool_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */ 

 
function eshkool_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on consturctlab, use a find and replace
	 * to change 'eshkool' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'eshkool', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );	


	
	
	
	function eshkool_custom_excerpt_length( $length ) {
		return 28;
	}	
	add_filter( 'excerpt_length', 'eshkool_custom_excerpt_length', 999 );

	

	function eshkool_change_excerpt( $text )
	{
		$pos = strrpos( $text, '[');
		if ($pos === false)
		{
			return $text;
		}
		
		return rtrim (substr($text, 0, $pos) ) . '...';
	}
	add_filter('get_the_excerpt', 'eshkool_change_excerpt');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.	
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'eshkool' ),
		'menu-2' => esc_html__( 'Onepage Menu', 'eshkool' ),
		'menu-3' => esc_html__( 'Category Menu', 'eshkool' ),
	) );

	

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'eshkool_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'eshkool_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eshkool_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'eshkool_content_width', 640 );
}
add_action( 'after_setup_theme', 'eshkool_content_width', 0 );

/**
* Tgm Activation
*/

if (is_admin()) {
	require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';
	require_once get_template_directory() . '/framework/tgm-config.php';	
}

/**
 * Implement the Custom Header feature.
 */
require_once get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 *  Enqueue scripts and styles.
 */
require_once get_template_directory() . '/inc/template-scripts.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require_once get_template_directory() . '/inc/template-functions.php';

/**
 * Leaernpress hooks and functions include
 */
require_once get_template_directory() . '/inc/lp-functions.php';

/**
 * woocommerce customize
 */
require_once get_template_directory() . '/inc/woocommerce-functions.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require_once get_template_directory() . '/inc/template-sidebar.php';

/**
 * Customizer additions.
 */
require_once get_template_directory() . '/inc/customizer.php';


/**
 * Custom Style
 */
require_once get_template_directory() . '/framework/custom.php';


/**
 * Redux frameworks additions
 */
require_once get_template_directory() . '/libs/theme-option/config.php';


/**
||-> add_image_size //Resize images
*/
/* ========= RESIZE IMAGES ===================================== */

add_image_size( 'eshkool_blog_slider_pic',  367, 426, true );
add_image_size( 'eshkool_latest_blog_big',  530, 248, true );
add_image_size( 'eshkool_latest_blog_big2',  540, 360, true );
add_image_size( 'eshkool_latest_blog_small',  154, 116, true );
add_image_size( 'eshkool_course_main_iamge',  732, 415, true );
add_image_size( 'eshkool_course_slider_iamge',  360, 270, true );
add_image_size( 'event_slider_iamge',  357, 285, true );
add_image_size( 'events_col_iamge',  252, 300, true );


//remove revolution slid metabox

function eshkool_remove_revolution_slider_meta_boxes() {
	
	remove_meta_box( 'mymetabox_revslider_0', 'client', 'normal' );
	remove_meta_box( 'mymetabox_revslider_0', 'portfolios', 'normal' );
	remove_meta_box( 'mymetabox_revslider_0', 'teams', 'normal' );
	remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
	remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
}

add_action( 'do_meta_boxes', 'eshkool_remove_revolution_slider_meta_boxes' );	


add_filter( 'get_the_archive_title', 'eshkool_archive_title_remove_prefix' );
function eshkool_archive_title_remove_prefix( $title ) {
	if ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	}
	// return "UES Publishing"; //değiştirildi
	return $title;
}

//----------------------------------------------------------------------
// Remove Redux Framework NewsFlash
//----------------------------------------------------------------------
if ( ! class_exists( 'reduxNewsflash' ) ):
    class reduxNewsflash {
        public function __construct( $parent, $params ) {

        }
    }
endif;

function eshkool_removeDemoModeLink() { // Be sure to rename this function to something more unique
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
    }
}
add_action('init', 'eshkool_removeDemoModeLink');


/**
 * Registers an editor stylesheet for the theme.
 */
function eshkool_theme_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'admin_init', 'eshkool_theme_add_editor_styles' );


//------------------------------------------------------------------------
//Organize Comments form field
//-----------------------------------------------------------------------
function eshkool_wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'eshkool_wpb_move_comment_field_to_bottom' );