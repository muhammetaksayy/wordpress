<?php
/**
 * Template for displaying archive course content.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-archive-course.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

global $post, $wp_query, $lp_tax_query, $wp_query, $eshkool_option;

/**
 * @deprecated
 */
do_action( 'learn_press_before_main_content' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/before-main-content' );

/**
 * @deprecated
 */
do_action( 'learn_press_archive_description' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/archive-description' );?>

<?php
global $post, $wp_query, $lp_tax_query, $wp_query;
$show_description = get_theme_mod( 'thim_learnpress_cate_show_description' );
$show_desc   = !empty( $show_description ) ? $show_description : '';
$cat_desc = term_description();

$total = $wp_query->found_posts;

if ( $total == 0 ) {
    $message = '<p class="message message-error">' . esc_html__( 'No courses found!', 'eshkool' ) . '</p>';
    $index   = esc_html__( 'There are no available courses!', 'eshkool' );
} elseif ( $total == 1 ) {
    $index = esc_html__( 'Showing only one result', 'eshkool' );
} else {
    $courses_per_page = absint( LP()->settings->get( 'archive_course_limit' ) );
    $paged            = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

    $from = 1 + ( $paged - 1 ) * $courses_per_page;
    $to   = ( $paged * $courses_per_page > $total ) ? $total : $paged * $courses_per_page;

    if ( $from == $to ) {
        $index = sprintf(
            esc_html__( 'Showing last course of %s results', 'eshkool' ),
            $total
        );
    } else {
        $index = sprintf(
            esc_html__( 'Showing %s-%s of %s results', 'eshkool' ),
            $from,
            $to,
            $total
        );
    }
}
?>


<div class="rs-course-archive-top">
    <div class="row">
        <div class="col-sm-6">
            <div class="course-left">
                <div class="course-icons">
                    <a href="#" class="rs-grid active-grid"><i class="fa fa-th-large"></i></a>
                    <a href="#" class="rs-list active-list"><i class="fa fa-list-ul"></i></a>
                </div>
                <div class="course-index">
                    <span><?php echo esc_html( $index ); ?></span>
                 </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="rs-search">
                <form method="get" action="<?php echo esc_url( get_post_type_archive_link( 'lp_course' ) ); ?>">
                    <input type="hidden" name="ref" value="course">
                    <input type="text" value="<?php echo esc_attr( get_search_query() );?>" name="s" placeholder="<?php esc_attr_e( 'Search our publishers', 'eshkool' ) ?>" class="form-control" />
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
if ( LP()->wp_query->have_posts() ) :

	/**
	 * @deprecated
	 */
	do_action( 'learn_press_before_courses_loop' );

	/**
	 * @since 3.0.0
	 */
	do_action( 'learn-press/before-courses-loop' );

	learn_press_begin_courses_loop();

	while ( LP()->wp_query->have_posts() ) : LP()->wp_query->the_post();

		//learn_press_get_template_part( 'content', 'course' );
?>
    <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="rs-courses-3">


            <?php
            // @deprecated
            do_action( 'learn_press_before_courses_loop_item' );

            // @since 3.0.0
            do_action( 'learn-press/before-courses-loop-item' );

            $course_id = get_the_ID();
            $rstheme_course = LP()->global['course'];
           
            if ( empty( $rstheme_course ) ) return;

            $course_author = get_post_field( 'post_author', $course_id );
            $course_enroll_count = $rstheme_course->get_users_enrolled();
            $course_enroll_count = $course_enroll_count ? $course_enroll_count : 0;                     
            
            if ( function_exists( 'learn_press_get_course_rate' ) ) {
                $course_rate_res = learn_press_get_course_rate( $course_id, false );
                $course_rate     = $course_rate_res['rated'];
                $course_rate_total = $course_rate_res['total'];
                $course_rate_text = $course_rate_total > 1 ? esc_html__( 'Reviews', 'eshkool' ) : esc_html__( 'Review', 'eshkool' );
            }
            $taxonomy = 'course_category';        
            ?>



            <div class="course-item">
                <div class="course-img" style="padding:35px 0px;">
                    <?php the_post_thumbnail(); ?>
                    <div class="avatar-info">
                    	<?php if(!empty($eshkool_option['off_course_author'])): ?>
	                        <div class="course_author_img">
	                            <a class="rs-author" href="<?php echo esc_url( learn_press_user_profile_link( $course_author ) );?>"><?php echo get_avatar( $course_author, 35 ); ?>
	                                
	                            </a>
	                        </div>
                    	<?php endif; ?>

                        <?php if(!empty($eshkool_option['off_course_price'])): 
                        	?><div class="price-inner"><?php learn_press_course_price();?>
                                
                            </div>
                        	<?php endif;
                        ?>
                    </div>
                    <div class="clear-fix"></div>
                    
                    <?php if(!empty($eshkool_option['off_course_cat'])): ?>
	                    <div class="course-toolbar">
	                        <h4 class="course-category">
	                            <?php echo wp_kses_post($cats_show    = get_the_term_list($course_id,$taxonomy, ' ', '<span class="separator">,</span> ? ')); 
	                            
	                            ?>
	                        </h4>           
	                    </div>
	                <?php endif; ?>
                </div>
                <div class="course-right">
                    <div class="course-body">
                        <div class="course-desc">
                        	<?php if(!empty($eshkool_option['off_course_title'])): ?>
	                            <h4 class="course-title-archive">
	                                <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
	                            </h4>
                        	<?php endif; ?>

                            <h4 class="course-category list-course-title">
                                <?php echo wp_kses_post($cats_show    = get_the_term_list($course_id,$taxonomy, ' ', '<span class="separator">,</span> ? '));                  
                                ?>
                            </h4> 

                           
                            <?php
                            	if(!empty($eshkool_option['off_course_shortdes'])):
                                 the_excerpt();
                                endif;
                            ?>
                           
                        </div>
                    </div>
                    <?php if(!empty($eshkool_option['off_course_enroll_count']) || !empty($eshkool_option['off_course_review']) ){?>
	                    <div class="course-footer">
	                    	<?php if(!empty($eshkool_option['off_course_enroll_count'])):?>
		                        <div class="course-seats">
		                            <i class="fa fa-users"></i> <?php echo esc_attr($course_enroll_count); ?>
		                        </div>

	                        <?php endif; if(!empty($eshkool_option['off_course_review'])):?>

		                        <div class="course-button">
		                            <?php learn_press_course_review_template( 'rating-stars.php', array( 'rated' => $course_rate ) );?><span class="course-rating-total"> </span>
		                        </div>

	                    	<?php endif; ?>
	                    </div>
	                <?php }?>
                </div>
            </div>                    
                          
            <?php // @since 3.0.0
            do_action( 'learn-press/after-courses-loop-item' );

            // @deprecated
            do_action( 'learn_press_after_courses_loop_item' );
            ?>
        </div>
    </li>
<?php

	endwhile;

	learn_press_end_courses_loop();

	/**
	 * @since 3.0.0
	 */
	do_action( 'learn_press_after_courses_loop' );

	/**
	 * @deprecated
	 */
	do_action( 'learn-press/after-courses-loop' );

	wp_reset_postdata();

else:
	learn_press_display_message( __( 'No course found.', 'eshkool' ), 'error' );
endif;

/**
 * @since 3.0.0
 */
do_action( 'learn-press/after-main-content' );

/**
 * @deprecated
 */
do_action( 'learn_press_after_main_content' );