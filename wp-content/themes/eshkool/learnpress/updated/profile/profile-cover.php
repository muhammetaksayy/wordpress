<?php
/**
 * Template for displaying user profile cover image.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/profile/profile-cover.php.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$profile = LP_Profile::instance();

$user = $profile->get_user();
//echo '<pre>';
//var_dump($user->get_id());
?>

<div class="user-info">

    <div class="author-avatar"><?php echo wp_kses_post($user->get_profile_picture( null, '270' )); ?></div>
    <?php $lp_designation = get_the_author_meta( 'designation', $user->get_id() ); ?>
    <div class="user-information">
    	<h3 class="author-name"><?php echo learn_press_get_profile_display_name( $user ); ?>
    		<?php if(!empty($lp_designation)):?><span><?php echo esc_html( $lp_designation );?></span>
    		<?php endif; ?>
    	</h3>

        <?php
			$lp_facebook = get_the_author_meta( 'facebook', $user->get_id() );
			$lp_twitter  = get_the_author_meta( 'twitter', $user->get_id() );
			$lp_linkadin = get_the_author_meta( 'linkedin', $user->get_id() );
			$lp_google   = get_the_author_meta( 'google', $user->get_id() );
			$lp_youtube   = get_the_author_meta( 'youtube', $user->get_id() );

        ?>
        <ul class="eshkool-author-social">
            <?php if ( $lp_facebook ) : ?>
                <li>
                    <a href="<?php echo esc_url( $lp_facebook ); ?>" class="facebook"><i class="fa fa-facebook"></i></a>
                </li>
            <?php endif; ?>

            <?php if ( $lp_twitter ) : ?>
                <li>
                    <a href="<?php echo esc_url( $lp_twitter ); ?>" class="twitter"><i class="fa fa-twitter"></i></a>
                </li>
            <?php endif; ?>

            <?php if ( $lp_google ) : ?>
                <li>
                    <a href="<?php echo esc_url( $lp_google ); ?>" class="google-plus"><i class="fa fa-google-plus"></i></a>
                </li>
            <?php endif; ?>

            <?php if ( $lp_linkadin ) : ?>
                <li>
                    <a href="<?php echo esc_url( $lp_linkadin); ?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
                </li>
            <?php endif; ?>

            <?php if ( $lp_youtube ) : ?>
                <li>
                    <a href="<?php echo esc_url( $lp_youtube ); ?>" class="youtube"><i class="fa fa-youtube"></i></a>
                </li>
            <?php endif; ?>
        </ul>
        

       
    </div>
</div>


