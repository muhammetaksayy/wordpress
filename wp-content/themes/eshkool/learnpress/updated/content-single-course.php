<?php
/**
 * Template for displaying course content within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-single-course.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */
global $eshkool_option;
/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}


$user        = learn_press_get_current_user();
$course_id   = get_the_ID();
$course      = LP()->global['course'];
$course_enroll_count = $course->get_users_enrolled();
$course_enroll_count = $course_enroll_count ? $course_enroll_count : 0; 

/**
 * @deprecated
 */
do_action( 'learn_press_before_main_content' );
do_action( 'learn_press_before_single_course' );
do_action( 'learn_press_before_single_course_summary' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/before-main-content' );

do_action( 'learn-press/before-single-course' );

?>

<div class="rs-courses-details">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="detail-img">
                <?php the_post_thumbnail('eshkool_course_main_iamge');?>
<!--
                <?php if(!empty($eshkool_option['off_course_price'])): ?>
                    <div class="course-seats price">
                       <?php learn_press_course_price(); ?>
                    </div>-->
                <?php endif; ?>

                <?php if(!empty($eshkool_option['off_course_enroll_count'])): ?>
                    <div class="course-seats">
                        <i class="fa fa-user"></i> <span><?php echo esc_attr($course_enroll_count);?></span>
                    </div>
                <?php endif; ?>


            </div>
        </div>
        <div style="display:none;" class="col-lg-4 col-md-12">
            <?php do_action('eshkool_single_course_feature'); ?>
            
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div id="learn-press-course" class="course-summary learn-press">       

                <div class="course-meta">
                    <?php //do_action( 'eshkool_single_course_meta' );?>
                </div>
                <div class="course-payment">
                    <?php //do_action( 'eshkool_single_course_payment' );?>
                </div>
                <div class="course-summary">
                    <?php
                    /**
                     * @since 3.0.0
                     *
                     * @see learn_press_single_course_summary()
                     */
                    do_action( 'learn-press/single-course-summary' );
                    ?>
                </div>
                <?php eshkool_related_courses(); ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <?php get_sidebar('course') ?>
        </div>
    </div>

<?php

/**
 * @since 3.0.0
 */
do_action( 'learn-press/after-main-content' );

do_action( 'learn-press/after-single-course' );

/**
 * @deprecated
 */
do_action( 'learn_press_after_single_course_summary' );
do_action( 'learn_press_after_single_course' );
do_action( 'learn_press_after_main_content' );
?>