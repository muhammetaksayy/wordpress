<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */

if ( ! is_active_sidebar( 'sidebar_courses' ) ) {
  return;}
?>
  <aside id="secondary" class="widget-area">
    <div class="bs-sidebar dynamic-sidebar">
      <?php
        dynamic_sidebar( 'sidebar_courses' );
      ?>
    </div>
  </aside>
  <!-- #secondary --> 
</div>
<?php
?>
