<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 * Template Name:  Event Page
 */

get_header();	
if( is_post_type_archive( 'lp_course' ) || is_singular( $post_types = 'lp_course') || is_category()){
	get_template_part( 'inc/page-header/breadcrumbs-course' );
}

?>
<div class="container">
	<div id="content" class="site-content">		
		<div id="rs-events" class="rs-events-2 rs-events-page-default">
			<div class="row">		    	
	        	<?php 

	        		$desc = !empty($eshkool_option['order_event']) ? $eshkool_option['order_event'] : 'DESC';
	        		$perpage = !empty($eshkool_option['event_count']) ? $eshkool_option['event_count'] : '6';

	        		global  $paged;
		            $paged = get_query_var("paged") ? get_query_var("paged"): 1; 

	        		$best_wp = new wp_Query(array(
					'post_type'      => 'events',
					'posts_per_page' => $perpage,
					'meta_key'       => 'ev_start_date',
					'orderby'        => 'meta_value',
					'paged'          => $paged,					
					'order'          => $desc
											
				));			      

				while($best_wp->have_posts()): $best_wp->the_post();
				$ev_start_date = get_post_meta(  get_the_ID(), 'ev_start_date', true );
				$ev_end_date   = get_post_meta(  get_the_ID(), 'ev_end_date', true );
				$ev_start_time = get_post_meta(  get_the_ID(), 'ev_start_time', true );
				$ev_end_time   = get_post_meta(  get_the_ID(), 'ev_end_time', true );
				$ev_location   = get_post_meta ( get_the_ID(), 'ev_location', true);

				$new_sDate = date("d/m/Y", strtotime($ev_start_date));  
				$new_eDate = date("d/m/Y", strtotime($ev_end_date));  
				$date_style = $eshkool_option['date_style'];
				if( 'style2' == $date_style ){
					$ev_start_date = $new_sDate;
					$ev_end_date = $new_eDate;
				}

				$time_style = $eshkool_option['time_style'];
				$new_stime  = date("H:i", strtotime($ev_start_time));
				$new_etime  = date("H:i", strtotime($ev_end_time));
				if( 'style2' == $time_style ){
					$ev_start_time = $new_stime;
					$ev_end_time   = $new_etime;
				}	
				  
			  ?>
            	<div class="col-lg-6 col-md-12">
                    	<div class="event-item">
	                        <div class="row rs-vertical-middle">
	                        	<div class="col-md-6">
	                        	    <div class="event-img">
	                        	       <?php the_post_thumbnail('events_col_iamge'); ?>
                                        <a class="image-link" href="<?php the_permalink();?>">
                                            <i class="fa fa-link"></i>
                                        </a>
	                        	    </div>                        		
	                        	</div>
	                        	<div class="col-md-6">
	                    	        <div class="event-content">
		                    	        <div class="event-meta">
		                    	        	<?php if(!empty($ev_start_date)): ?>
			                    	        	<div class="event-date">
			                    	        		<i class="fa fa-calendar"></i>
			                    	        		<span><?php echo esc_attr($ev_start_date);?></span>
			                    	        	</div>
			                    	        <?php endif; ?>
			                    	        <?php if(!empty($ev_start_time)): ?>
		                    	        		<div class="event-time">
		                    	        			<i class="fa fa-clock-o"></i>
		                    	        			<span><?php echo esc_attr($ev_start_time);?><?php echo '-' ?><?php echo esc_attr($ev_end_time);?></span>
		                    	        		</div>
		                    	        	<?php endif; ?>
	                    	        	</div>

	                    	        	<h3 class="event-title">
	                    	        		<a href="<?php the_permalink();?>"><?php the_title();?></a>
	                    	        	</h3>

	                    	        	<?php if(!empty($ev_location)): ?>
	                    	        		<div class="event-location">
	                    	        			<i class="fa fa-map-marker"></i>
	                    	        			<span><?php echo esc_attr($ev_location);?></span>
	                    	        		</div>
                    	        		<?php endif; ?>

	                    	        	<div class="event-desc">
	                    	        		<?php the_excerpt();?>
	                    	        	</div>

	                    	        	<div class="event-btn">
	                    	        		<?php if(!empty($eshkool_option['event_btn'])):				 								
							            		?>
							            		<a href="<?php the_permalink();?>"><?php echo esc_attr($eshkool_option['event_btn']);?></a>
							            	<?php endif; ?>
	                    	        	</div>
	                    	        </div>                    		
	                        	</div>
	                        </div>                    		
                    	</div>
                </div>
			<?php
				endwhile; 	
				wp_reset_query();

				$paginate = paginate_links( array(
			'total' => $best_wp->max_num_pages
				));
				if( $paginate ){
					echo '<div class="pagination-area"><div class="nav-links">'.$paginate.'</div></div>';
				}
			?>
		</div>
	</div>
</div>
<?php
get_footer();
