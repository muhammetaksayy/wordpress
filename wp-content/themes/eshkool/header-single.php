<?php

/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php global $eshkool_option; ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
 <!--Preloader start here-->
   <?php get_template_part( 'inc/header/preloader' ); ?>
 <!--Preloader area end here-->
 <?php
    if(is_page()):
       $page_bg = get_post_meta( $post->ID, 'page_bg', true ); 
       $page_bg_back = ( $page_bg == 'Dark' ) ? 'dark' : '';
       else:
       $page_bg_back = '';
    endif;
    ?>
  <div id="page" class="site <?php echo esc_attr($page_bg_back);?>">
  <?php
   $post_meta_header = get_post_meta(get_the_ID(), 'header_select', true);   
   if($post_meta_header!=''){   
     
      if($post_meta_header == 'header6'){    
         get_template_part('inc/header/header-single');      
      }
      if($post_meta_header == 'header7'){    
        get_template_part('inc/header/header-single-kids');   
      }
   } else{
      get_template_part('inc/header/header-single');  
   }

  ?>
 
  <!-- End Header Menu End -->
  <div class="main-contain">
