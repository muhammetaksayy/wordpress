<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */

get_header();
global $eshkool_option;

get_template_part('inc/page-header/breadcrumbs-shop');
// Layout class
if ( $eshkool_option['shop-layout'] == 'full' ) {
	$eshkool_layout_class = 'col-sm-12 col-xs-12';
}
else{
	$eshkool_layout_class = 'col-sm-12 col-md-12 col-xs-12';
}
?>
<div class="container">
	<div id="content" class="site-content">		
		<div class="row">
			<?php
				if ( $eshkool_option['shop-layout'] == 'left-col'  ) {
					get_sidebar();
				}
			?>    			
		    <div class="<?php echo esc_attr($eshkool_layout_class);?>">
			    <?php					
					woocommerce_content();						
   				 ?>
		    </div>
			<?php
				if ( $eshkool_option['shop-layout'] == 'right-col'  ) {
					get_sidebar();
				}
			?> 
		</div>
	</div>
</div>
<?php
get_footer();