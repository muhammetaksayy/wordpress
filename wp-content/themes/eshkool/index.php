<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */
get_header(); ?>
<?php 
global $eshkool_option;
get_template_part( 'inc/page-header/breadcrumbs-blog' ); ?>
<div class="rs-blog">
    <div class="container">
        <div id="content">
          <?php
            //checking blog layout form option  
            $col         ='';
            $blog_layout =''; 
            $column      =''; 
            $layout      =''; 
            $blog_grid   ='';
            if(!empty($eshkool_option['blog-layout'])|| !is_active_sidebar( 'sidebar-1' ))
              {
                $blog_layout = ($eshkool_option['blog-layout']);
                $blog_grid = !empty($eshkool_option['blog-grid']) ? $eshkool_option['blog-grid'] : '' ;
                $blog_grid = !empty($blog_grid) ? $blog_grid : '12'; 
                            
                if($blog_layout == 'full' || !is_active_sidebar( 'sidebar-1' )){
                    $layout ='full-layout';
                    $col    = '-full';
                    $column = 'sidebar-none';  
                }                  
                elseif($blog_layout == '2left'){
                    $layout = 'full-layout-left';  
                }            
                elseif($blog_layout == '2right'){
                    $layout = 'full-layout-right'; 
                } 
                else{
                  $col = '';
                  $blog_layout = ''; 
                }
              }
              else{
                $col         ='';
                $blog_layout =''; 
                $layout      ='';
                $blog_grid   = 12;
              }
            ?>                
            <div class="row right-<?php echo esc_attr($layout); ?>">
                <div class="col-md-8<?php echo esc_attr($col); ?> <?php echo esc_attr($layout); ?>">               
                    <div class="row">                
                        <?php
                        if ( have_posts() ) :           
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();      
                        ?>  
                        <article <?php post_class(); ?>>              
                            <div class="col-sm-12 col-xs-12 blog-main-item">
                                <div class="blog-item">
                                  <?php if ( has_post_thumbnail() ) {?>
                                    <div class="blog-img">
                                        <?php
                                            the_post_thumbnail();
                                        ?>
                                        <a href="<?php the_permalink();?>" class="blog-link"><i class="fa fa-link"></i></a>  
                                    </div><!-- .blog-img -->
                                  <?php } ?>

                                    <div class="blog-full-area">
                                        <div class="full-blog-content">
                                            <h3 class="blog-title">
                                                <a href="<?php the_permalink();?>">
                                                      <?php the_title();?>
                                                </a>
                                            </h3> 
                                            <?php if($eshkool_option['blog-author-post'] == 'show'){ ?>
                                            <div class="blog-meta">                            
                                                <div class="blog-date">                                
                                                    <ul class="btm-cate">

                                                    <?php if(get_the_date()){?>
                                                        <li>
                                                            <i class="fa fa-calendar-o"></i>
                                                            <?php $post_date = get_the_date(); echo esc_attr($post_date);?>
                                                        </li>
                                                    <?php }?> 


                                                    <?php if(get_the_author()){?>
                                                        <li>
                                                            <i class="fa fa-user-o"></i>
                                                            <?php the_author();?>
                                                        </li>
                                                    <?php }?>

                                                      <?php if(get_the_category()){?>
                                                        <li class="category-name"><i class="fa fa-folder-o"></i>
                                                              <?php the_category(', '); 
                                                            ?>
                                                        </li>
                                                      <?php }?>

                                                      <?php if(has_tag()): ?>
                                                        <li>
                                                            <?php
                                                           //tag add
                                                              $seperator = ', '; // blank instead of comma
                                                              $after = '';
                                                              $before = '';
                                                              echo '<div class="tag-line">';
                                                              the_tags( $before, $seperator, $after );
                                                              echo '</div>';
                                                              ?>
                                                        </li>
                                                        <?php endif; ?>                   
                                                    </ul>
                                                </div>                                                     
                                            </div> 
                                        <?php } ?>                       
                                        <div class="blog-desc">                           
                                            <?php the_excerpt(); ?> 
                                            <div class="clearfix"></div>       
                                        </div>         
                                        <?php if( !empty( $eshkool_option['blog-readmore'])): ?>
                                        <div class="blog-button">
                                            <a href="<?php the_permalink();?>" class="readmore"><?php echo esc_attr($eshkool_option['blog_button_text']);?></a>
                                        </div>
                                        <?php endif; ?>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <?php  
                        endwhile;   
                      ?>
                    </div>            
                    <div class="pagination-area">
                      <?php
                          the_posts_pagination();
                        ?>
                    </div>                  
                  <?php
                  else :
                  get_template_part( 'template-parts/content', 'none' );
                  endif; ?> 
                </div>
                <?php if( $layout != 'full-layout' ):     
                     get_sidebar();    
                   endif;
                ?>
            </div>  
        </div>
    </div>
</div>
<?php
get_footer();