<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "eshkool_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'Eshkool/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Eshkool Options', 'eshkool' ),
        'page_title'           => esc_html__( 'Eshkool Options', 'eshkool' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        'forced_dev_mode_off' => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        'compiler' => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        'force_output' => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( wp_kses_post( '<p>Eshkool Theme</p>', 'eshkool' ), $v );
    } else {
        $args['intro_text'] = sprintf(wp_kses_post( '<p>Eshkool Theme</p>', 'eshkool' ));
    }

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTSeshkool
     */


    /*

    As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
     
   // -> START General Settings
   Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'General Sections', 'eshkool' ),
        'id'               => 'basic-checkbox',
        'customizer_width' => '450px',
        'fields'           => array(

            array(
                'id'       => 'logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Upload Default Logo', 'eshkool' ),
                'subtitle' => esc_html__( 'Upload your logo', 'eshkool' ),
                'url'=> true
                
            ),

            array(
                'id'       => 'logo_light',
                'type'     => 'media',
                'title'    => esc_html__( 'Upload Your Light', 'eshkool' ),
                'subtitle' => esc_html__( 'Upload your light logo', 'eshkool' ),
                'url'=> true
                
            ),

            array(
                'id'       => 'rswplogo_sticky',
                'type'     => 'media',
                'title'    => esc_html__( 'Upload Your Sticky Logo', 'eshkool' ),
                'subtitle' => esc_html__( 'Upload your sticky logo', 'eshkool' ),
                'url'=> true                
            ),


            
            array(
            'id'       => 'rs_favicon',
            'type'     => 'media',
            'title'    => esc_html__( 'Upload Favicon', 'eshkool' ),
            'subtitle' => esc_html__( 'Upload your faviocn here', 'eshkool' ),
            'url'=> true            
            ),
            
            array(
                'id'       => 'off_sticky',
                'type'     => 'switch', 
                'title'    => esc_html__('Sticky Menu', 'eshkool'),
                'subtitle' => esc_html__('You can show or hide sticky menu here', 'eshkool'),
                'default'  => false,
            ),

            array(
                'id'       => 'off_search',
                'type'     => 'switch', 
                'title'    => esc_html__('Show Searh Icon at Menu Area', 'eshkool'),
                'subtitle' => esc_html__('You can show or hide search here', 'eshkool'),
                'default'  => false,
            ),
            
            array(
                'id'       => 'off_canvas',
                'type'     => 'switch', 
                'title'    => esc_html__('Show off Canvas', 'eshkool'),
                'subtitle' => esc_html__('You can show or hide off canvas here', 'eshkool'),
                'default'  => false,
            ),
            
           
                
            array(
                'id'       => 'show_top_bottom',
                'type'     => 'switch', 
                'title'    => esc_html__('Go to Top', 'eshkool'),
                'subtitle' => esc_html__('You can show or hide here', 'eshkool'),
                'default'  => false,
            ),  


            
        )
    ) );
    
    
    // -> START Header Section
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Header', 'eshkool' ),
        'id'               => 'header',
        'customizer_width' => '450px',
        'icon' => 'el el-certificate',       
         
        'fields'           => array(
        array(
            'id'     => 'notice_critical',
            'type'   => 'info',
            'notice' => true,
            'style'  => 'success',
            'title'  => esc_html__('Header Top Area', 'eshkool'),
            'desc'   => esc_html__('Header top area here', 'eshkool')
        ),
        
        array(
            'id'       => 'show-top',
            'type'     => 'switch', 
            'title'    => esc_html__('Show Top Bar', 'eshkool'),
            'subtitle' => esc_html__('You can select top bar show or hide', 'eshkool'),
            'default'  => false,
        ), 

         array(
            'id'       => 'welcome-text',                               
            'title'    => esc_html__( 'Top Bar Welcome Text', 'eshkool' ),
            'subtitle' => esc_html__( 'Top Bar Welcome Text Add Here', 'eshkool' ),
            'type'     => 'text',
                    
            ),

         
      
        array(
                'id'       => 'show-social',
                'type'     => 'switch', 
                'title'    => esc_html__('Show Social Icons at Header', 'eshkool'),
                'subtitle' => esc_html__('You can select Social Icons show or hide', 'eshkool'),
                'default'  => true,
            ),  
                    
          array(
            'id'     => 'notice_critical2',
            'type'   => 'info',
            'notice' => true,
            'style'  => 'success',
            'title'  => esc_html__('Header Area', 'eshkool'),
            'desc'   => esc_html__('Header area here', 'eshkool')
        ),


          array(
                    'id'       => 'phone-pretext',                               
                    'title'    => esc_html__( 'Phone Number Pre Text', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Phone Number Pre Text', 'eshkool' ),
                    'type'     => 'text',
                    
            ),
        
         array(
                    'id'       => 'phone',                               
                    'title'    => esc_html__( 'Phone Number', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Phone Number', 'eshkool' ),
                    'type'     => 'text',
                    
            ),

           array(
                    'id'       => 'email-pretext',                               
                    'title'    => esc_html__( 'E-mail Pre Text', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter E-mail Pre Text', 'eshkool' ),
                    'type'     => 'text',
                    
            ),
       
        array(
                    'id'       => 'top-email',                               
                    'title'    => esc_html__( 'Email Address', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Email Address', 'eshkool' ),
                    'type'     => 'text',
                    'validate' => 'email',
                    'msg'      => 'Email Address Not Valid',
                    
            ),  

         array(
                    'id'       => 'location-pretext',                               
                    'title'    => esc_html__( 'Location Title', 'eshkool' ),
                    'subtitle' => esc_html__( 'Pre Text', 'eshkool' ),
                    'type'     => 'text',
                    
            ),
       
        array(
                    'id'       => 'top-location',                               
                    'title'    => esc_html__( 'Add Location', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Address Here', 'eshkool' ),
                    'type'     => 'text',                   
                    
            ),  
            
            array(
                    'id'       => 'quote',                               
                    'title'    => esc_html__( 'Quote Button Text', 'eshkool' ),                  
                    'type'     => 'text',
                    
            ),  
            
            array(
                'id'       => 'quote_link',                               
                'title'    => esc_html__( 'Quote Button Link', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Quote Button Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),  

            array(
                'id'       => 'login_link',                               
                'title'    => esc_html__( 'Login Button Link', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Login Button Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),

            array(
                'id'       => 'login_text',                               
                'title'    => esc_html__( 'Login Button Text', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Login Text Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),       
        )
    ) 
);  
   

Redux::setSection( $opt_name, array(
'title'            => esc_html__( 'Header Layout', 'eshkool' ),
'id'               => 'header-style',
'customizer_width' => '450px',
'subsection' =>'true',      
'fields'           => array(    
                    
                    array(
                        'id'       => 'header_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Header Layout', 'eshkool'), 
                        'subtitle' => esc_html__('Select header layout. Choose between 1, 2 ,3 or 4 layout.', 'eshkool'),
                        'options'  => array(
                            'style1'      => array(
                                'alt'   => 'Header Style 1', 
                                'img'   => get_template_directory_uri().'/libs/img/style_1.png'
                                
                            ),
                            'style2'      => array(
                                'alt'   => 'Header Style 2', 
                                'img'   => get_template_directory_uri().'/libs/img/style_2.png'
                            ),
                            'style3'      => array(
                                'alt'   => 'Header Style 3', 
                                'img'  => get_template_directory_uri().'/libs/img/style_3.png'
                            ),
                            'style4'      => array(
                                'alt'   => 'Header Style 4', 
                                'img'   => get_template_directory_uri().'/libs/img/style_4.png'
                            ),
                            'style5'      => array(
                                'alt'   => 'Header Style 5', 
                                'img'   => get_template_directory_uri().'/libs/img/style_5.png'
                            ),

                            'style6'      => array(
                                'alt'   => 'Header Style 6', 
                                'img'   => get_template_directory_uri().'/libs/img/style_1.png'
                                
                            ),
                            
                        ),
                        'default' => 'style1'
                    ),                           
                    
            )
        ) 

);

                           

    // -> START Style Section
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Style', 'eshkool' ),
        'id'               => 'stle',
        'customizer_width' => '450px',
        'icon' => 'el el-brush',
        ));
    
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Global Style', 'eshkool' ),
        'desc'   => esc_html__( 'Style your theme', 'eshkool' ),        
        'subsection' =>'true',  
        'fields' => array( 
                        
                        array(
                            'id'        => 'body_bg_color',
                            'type'      => 'color',                           
                            'title'     => esc_html__('Body Backgroud Color','eshkool'),
                            'subtitle'  => esc_html__('Pick body background color', 'eshkool'),
                            'default'   => '#fff',
                            'validate'  => 'color',                        
                        ), 
                        
                        array(
                            'id'        => 'body_text_color',
                            'type'      => 'color',            
                            'title'     => esc_html__('Text Color','eshkool'),
                            'subtitle'  => esc_html__('Pick text color', 'eshkool'),
                            'default'   => '#505050',
                            'validate'  => 'color',                        
                        ),     
        
                        array(
                        'id'        => 'primary_color',
                        'type'      => 'color', 
                        'title'     => esc_html__('Primary Color','eshkool'),
                        'subtitle'  => esc_html__('Select Primary Color.', 'eshkool'),
                        'default'   => '#ff3115',
                        'validate'  => 'color',                        
                        ), 

                        array(
                        'id'        => 'secondary_color',
                        'type'      => 'color', 
                        'title'     => esc_html__('Secondary Color','eshkool'),
                        'subtitle'  => esc_html__('Select Secondary Color.', 'eshkool'),
                        'default'   => '#e41f05',
                        'validate'  => 'color',                        
                        ),

                        array(
                            'id'        => 'link_text_color',
                            'type'      => 'color',                       
                            'title'     => esc_html__('Link Color','eshkool'),
                            'subtitle'  => esc_html__('Pick Link color', 'eshkool'),
                            'default'   => '#ff3115',
                            'validate'  => 'color',                        
                        ),
                        
                        array(
                            'id'        => 'link_hover_text_color',
                            'type'      => 'color',                 
                            'title'     => esc_html__('Link Hover Color','eshkool'),
                            'subtitle'  => esc_html__('Pick link hover color', 'eshkool'),
                            'default'   => '#e41f05',
                            'validate'  => 'color',                        
                        ),    
                            
                        array(
                            'id'        => 'footer_bg_color',
                            'type'      => 'color',
                            'title'     => esc_html__('Footer Bg Color','eshkool'),
                            'subtitle'  => esc_html__('Pick color.', 'eshkool'),
                            'default'   => '#252525',
                            'validate'  => 'color',                        
                        ),  
                       
                 ) 
            ) 
    ); 

      
    
//Menu settings
Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Main Menu', 'eshkool' ),
    'desc'   => esc_html__( 'Main Menu Style Here', 'eshkool' ),        
    'subsection' =>'true',  
    'fields' => array( 
                    
                    array(
                        'id'        => 'menu_text_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),    
                        'default'   => '#505050',                        
                        'validate'  => 'color',                        
                    ), 

                    array(
                        'id'        => 'transparent_menu_text_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Transparent Main Menu Text Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),    
                        'default'   => '#fff',                        
                        'validate'  => 'color',                        
                    ), 
                    
                    array(
                        'id'        => 'menu_text_hover_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Hover Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),           
                        'default'   => '#ff3115',                 
                        'validate'  => 'color',                        
                    ), 
                    array(
                        'id'        => 'menu_text_active_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Active Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),
                        'default'   => '#ff3115',
                        'validate'  => 'color',                        
                    ), 
                    
                    array(
                        'id'        => 'drop_down_bg_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Dropdown Menu Background Color','eshkool'),
                        'subtitle'  => esc_html__('Pick bg color', 'eshkool'),
                        'default'   => '#212121',
                        'validate'  => 'color',                        
                    ), 
                        
                    
                    array(
                        'id'        => 'drop_text_color',
                        'type'      => 'color',                     
                        'title'     => esc_html__('Dropdown Menu Text Color','eshkool'),
                        'subtitle'  => esc_html__('Pick text color', 'eshkool'),
                        'default'   => '#bbbbbb',
                        'validate'  => 'color',                        
                    ), 
                    
                    array(
                        'id'        => 'drop_text_hover_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Dropdown Menu Hover Text Color','eshkool'),
                        'subtitle'  => esc_html__('Pick text color', 'eshkool'),
                        'default'   => '#ff3115',
                        'validate'  => 'color',                        
                    ),     
                    
                    
                    array(
                        'id'        => 'drop_text_hoverbg_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Dropdown Menu item Hover Background Color','eshkool'),
                        'subtitle'  => esc_html__('Pick text color', 'eshkool'),
                        'default'   => '',
                        'validate'  => 'color',                        
                    ), 
                    
                    array(
                        'id'        => 'drop_text_border_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Dropdown Menu item Seperate Border Color','eshkool'),
                        'subtitle'  => esc_html__('Pick a color', 'eshkool'),             
                        'default'   => '#fff',               
                        'validate'  => 'color',                        
                    ),              
                    
                    
            )
        )
    );


//Menu settings
Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Mobile Menu', 'eshkool' ),
    'desc'   => esc_html__( 'Mobile Menu Style Here', 'eshkool' ),        
    'subsection' =>'true',  
    'fields' => array( 


                    array(
                        'id'        => 'mobile_menu_icon_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Menu Burger Icon Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),           
                        'default'   => '#ff3115',                 
                        'validate'  => 'color',                        
                    ),

                    array(
                        'id'        => 'mobile_menu_container_bg',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Menu Background Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),           
                        'default'   => '#212121',                 
                        'validate'  => 'color',                        
                    ),
                    
                    
                    array(
                        'id'        => 'mobile_menu_text_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),    
                        'default'   => '#ffffff',                        
                        'validate'  => 'color',                        
                    ), 
                    
                    
                    array(
                        'id'        => 'mobile_menu_text_hover_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Hover Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),           
                        'default'   => '#ff3115',                 
                        'validate'  => 'color',                        
                    ), 
                    array(
                        'id'        => 'mobile_menu_text_active_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Main Menu Text Active Color','eshkool'),
                        'subtitle'  => esc_html__('Pick color', 'eshkool'),
                        'default'   => '#ff3115',
                        'validate'  => 'color',                        
                    ), 
                  
                 
                    array(
                        'id'        => 'mobile_drop_text_border_color',
                        'type'      => 'color',                       
                        'title'     => esc_html__('Dropdown Menu item Seperate Border Color','eshkool'),
                        'subtitle'  => esc_html__('Pick a color', 'eshkool'),             
                        'default'   => '#fff',               
                        'validate'  => 'color',                        
                    ),              
                    
                    
            )
        )
    );
    
 // -> START Kids Header Section

  //Preloader settings
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Preloader Style', 'eshkool' ),
        'desc'   => esc_html__( 'Preloader Style Here', 'eshkool' ),        
        
        'fields' => array(     

                        array(
                            'id'       => 'show_preloader',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Preloader', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide preloader', 'eshkool'),
                            'default'  => false,
                        ),                    

                        array(
                            'id'        => 'preloader_bg_color',
                            'type'      => 'color',                       
                            'title'     => esc_html__('Preloader Background Color','eshkool'),
                            'subtitle'  => esc_html__('Pick color', 'eshkool'),    
                            'default'   => '#ff3115',                        
                            'validate'  => 'color',                        
                        ), 
                        
                        array(
                            'id'        => 'preloader_text_color',
                            'type'      => 'color',                       
                            'title'     => esc_html__('Preloader Color','eshkool'),
                            'subtitle'  => esc_html__('Pick color if you use default preloader color', 'eshkool'),    
                            'default'   => '#e41f05',                        
                            'validate'  => 'color',                        
                        ), 

                        array(
                            'id'    => 'preloader_img', 
                            'url'   => true,     
                            'title' => esc_html__( 'Preloader Image', 'eshkool' ),                 
                            'type'  => 'media',                                  
                        ),       
                    )
                )
            );    
//End Preloader settings  


Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Kids Header', 'eshkool' ),
        'id'               => 'kids-header',
        'customizer_width' => '450px',
        'icon' => 'el el-certificate',       
         
        'fields'           => array(
            array(
                'id'     => 'kids-notice-1',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'success',
                'title'  => esc_html__('Topbar Left Area', 'eshkool'),
            ),
            
            array(
                'id'       => 'kids-show-top',
                'type'     => 'switch', 
                'title'    => esc_html__('Show Top Bar', 'eshkool'),
                'subtitle' => esc_html__('You can select top bar show or hide', 'eshkool'),
                'default'  => false,
            ), 
             

            array(
                'id'       => 'kids-welcome-text',                               
                'title'    => esc_html__( 'Top Bar Welcome Text', 'eshkool' ),
                'subtitle' => esc_html__( 'Top Bar Welcome Text Add Here', 'eshkool' ),
                'type'     => 'text',
                        
            ),
            
            
             array(
                'id'       => 'kids-phone',                               
                'title'    => esc_html__( 'Phone Number', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Phone Number', 'eshkool' ),
                'type'     => 'text',
                        
            ),

            array(
                'id'       => 'kids-top-email',                               
                'title'    => esc_html__( 'Email Address', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Email Address', 'eshkool' ),
                'type'     => 'text',
                'validate' => 'email',
                'msg'      => 'Email Address Not Valid',
                    
            ),  

           
            array(
                'id'       => 'kids-address',                               
                'title'    => esc_html__( 'Address', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Address Here', 'eshkool' ),
                'type'     => 'text',                   
                
            ),  
             
        
            array(
                'id'     => 'kids-notice-2',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'success',
                'title'  => esc_html__('Topbar Right Area', 'eshkool'),
             ),

            array(
                'id'       => 'kids-show-social',
                'type'     => 'switch', 
                'title'    => esc_html__('Show Social Icons at Header', 'eshkool'),
                'subtitle' => esc_html__('You can select Social Icons show or hide', 'eshkool'),
                'default'  => true,
            ),  
                   

            
            array(
                    'id'       => 'kids-quote',                               
                    'title'    => esc_html__( 'Quote Button Text', 'eshkool' ),                  
                    'type'     => 'text',
                    
            ),  
            
            array(
                'id'       => 'kids-quote_link',                               
                'title'    => esc_html__( 'Quote Button Link', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Quote Button Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),  

            array(
                'id'       => 'kids-login_link',                               
                'title'    => esc_html__( 'Login Button Link', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Login Button Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),

            array(
                'id'       => 'kids-login_text',                               
                'title'    => esc_html__( 'Login Button Text', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Login Text Link Here', 'eshkool' ),
                'type'     => 'text',   
            ), 

            array(
                'id'     => 'kids-notice-3',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'success',
                'title'  => esc_html__('Topbar Style Settings', 'eshkool'),
            ), 

            array(
            'id'        => 'kids-topbar-bg',
            'type'      => 'color', 
            'title'     => esc_html__('Background Color','eshkool'),
            'subtitle'  => esc_html__('Select Background Color.', 'eshkool'),
            'default'   => '#00bcd4',
            'validate'  => 'color',                        
            ),

            array(
                'id'        => 'kids-topbar_text_color',
                'type'      => 'color',                       
                'title'     => esc_html__('Text Color','eshkool'),
                'subtitle'  => esc_html__('Pick Text color', 'eshkool'),
                'default'   => '#ffffff',
                'validate'  => 'color',                        
            ),

             array(
                'id'        => 'kids-topbar_link_color',
                'type'      => 'color',                       
                'title'     => esc_html__('Link Color','eshkool'),
                'subtitle'  => esc_html__('Pick Link color', 'eshkool'),
                'default'   => '#ffffff',
                'validate'  => 'color',                        
            ),
            
            
            array(
                'id'        => 'kids-topbar_link_hover_color',
                'type'      => 'color',                 
                'title'     => esc_html__('Link Hover Color','eshkool'),
                'subtitle'  => esc_html__('Pick link hover color', 'eshkool'),
                'default'   => '#e41f05',
                'validate'  => 'color',                        
            ),    
            array(
                'id'        => 'kids-topbar_icon_color',
                'type'      => 'color',                       
                'title'     => esc_html__('Icon Color','eshkool'),
                'subtitle'  => esc_html__('Pick Icon color', 'eshkool'),
                'default'   => '#ffffff',
                'validate'  => 'color',                        
            ),
            
            
            array(
                'id'        => 'kids-topbar_icon_hover_color',
                'type'      => 'color',                 
                'title'     => esc_html__('Icon Hover Color','eshkool'),
                'subtitle'  => esc_html__('Pick Icon hover color', 'eshkool'),
                'default'   => '#e41f05',
                'validate'  => 'color',                        
            ),    

            array(
                'id'     => 'kids-notice-4',
                'type'   => 'info',
                'notice' => true,
                'style'  => 'success',
                'title'  => esc_html__('Header Style Settings', 'eshkool'),
            ), 

            array(
                'id'        => 'kids-header-bg',
                'type'      => 'color', 
                'title'     => esc_html__('Background Color','eshkool'),
                'subtitle'  => esc_html__('Select Background Color.', 'eshkool'),
                'default'   => '#ffffff',
                'validate'  => 'color',                        
            ),

            array(
                'id'        => 'kids-header_link_color',
                'type'      => 'color',                       
                'title'     => esc_html__('Link Color','eshkool'),
                'subtitle'  => esc_html__('Pick Link color', 'eshkool'),
                'default'   => '#00bcd4',
                'validate'  => 'color',                        
            ),
                       
            array(
                'id'        => 'kids-header_link_hover_color',
                'type'      => 'color',                 
                'title'     => esc_html__('Link Hover Color','eshkool'),
                'subtitle'  => esc_html__('Pick link hover color', 'eshkool'),
                'default'   => '#e41f05',
                'validate'  => 'color',                        
            ), 

            array(
                'id'        => 'kids-dropdown-bg',
                'type'      => 'color', 
                'title'     => esc_html__('Dropdown Background Color','eshkool'),
                'subtitle'  => esc_html__('Pick Dropdown Background Color.', 'eshkool'),
                'default'   => '#00bcd4',
                'validate'  => 'color',                        
            ), 

            array(
                'id'        => 'kids-dropdown_link_color',
                'type'      => 'color',                       
                'title'     => esc_html__('Link Color','eshkool'),
                'subtitle'  => esc_html__('Pick Dropdown Link color', 'eshkool'),
                'default'   => '#ffffff',
                'validate'  => 'color',                        
            ),
                       
            array(
                'id'        => 'kids-dropdown_link_hover_color',
                'type'      => 'color',                 
                'title'     => esc_html__('Link Hover Color','eshkool'),
                'subtitle'  => esc_html__('Pick Dropdown link hover color', 'eshkool'),
                'default'   => '#e41f05',
                'validate'  => 'color',                        
            ),   

        )
    ) 
);  
           
    
    //-> START Typography
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Typography', 'eshkool' ),
        'id'     => 'typography',
        'desc'   => esc_html__( 'You can specify your body and heading font here','eshkool'),
        'icon'   => 'el el-font',
        'fields' => array(
            array(
                'id'       => 'opt-typography-body',
                'type'     => 'typography',
                'title'    => esc_html__( 'Body Font', 'eshkool' ),
                'subtitle' => esc_html__( 'Specify the body font properties.', 'eshkool' ),
                'google'   => true, 
                'font-style' =>false,           
                'default'  => array(                    
                    'font-size'   => '14px',
                    'font-family' => 'Roboto',
                    'font-weight' => '400',
                ),
            ),
             array(
                'id'       => 'opt-typography-menu',
                'type'     => 'typography',
                'title'    => esc_html__( 'Navigation Font', 'eshkool' ),
                'subtitle' => esc_html__( 'Specify the menu font properties.', 'eshkool' ),
                'google'   => true,
                'font-backup' => true,                
                'all_styles'  => true,              
                'default'  => array(
                    'color'       => '#505050',                    
                    'font-family' => 'Roboto',
                    'google'      => true,
                    'font-size'   => '14px',                    
                    'font-weight' => 'Normal',
                    
                ),
            ),
            array(
                'id'          => 'opt-typography-h1',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H1', 'eshkool' ),
                'font-backup' => true,                
                'all_styles'  => true,
                'units'       => 'px',
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '36px',
                    
                    ),
                ),
            array(
                'id'          => 'opt-typography-h2',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H2', 'eshkool' ),
                'font-backup' => true,                
                'all_styles'  => true,                 
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '30px',
                    
                ),
                ),
            array(
                'id'          => 'opt-typography-h3',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H3', 'eshkool' ),             
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '24px',
                    
                    ),
                ),
            array(
                'id'          => 'opt-typography-h4',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H4', 'eshkool' ),
                //'compiler'      => true,  // Use if you want to hook in your own CSS compiler
                //'google'      => false,
                // Disable google fonts. Won't work if you haven't defined your google api key
                'font-backup' => false,                
                'all_styles'  => true,               
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '20px',
                    'line-height' => '30px'
                    ),
                ),
            array(
                'id'          => 'opt-typography-h5',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H5', 'eshkool' ),
                //'compiler'      => true,  // Use if you want to hook in your own CSS compiler
                //'google'      => false,
                // Disable google fonts. Won't work if you haven't defined your google api key
                'font-backup' => false,                
                'all_styles'  => true,                
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '18px',
                    'line-height' => '26px'
                    ),
                ),
            array(
                'id'          => 'opt-typography-6',
                'type'        => 'typography',
                'title'       => esc_html__( 'Heading H6', 'eshkool' ),
             
                'font-backup' => false,                
                'all_styles'  => true,                
                'units'       => 'px',
                // Defaults to px
                'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'eshkool' ),
                'default'     => array(
                    'color'       => '#212121',
                    'font-style'  => '700',
                    'font-family' => 'Poppins',
                    'google'      => true,
                    'font-size'   => '16x',
                    'line-height' => '24px'
                ),
                ),
                
                )
            )
                    
   
    );

    /*Blog Sections*/
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Blog', 'eshkool' ),
        'id'               => 'blog',
        'customizer_width' => '450px',
        'icon' => 'el el-comment',
        )
    );
        
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Blog Settings', 'eshkool' ),
        'id'               => 'blog-settings',
        'subsection'       => true,
        'customizer_width' => '450px',      
        'fields'           => array(
        
                            array(
                                'id'       => 'blog_banner_main', 
                                'url'      => true,     
                                'title'    => esc_html__( 'Blog Page Banner', 'eshkool' ),                 
                                'type'     => 'media',                                  
                            ),  
                            
                            array(
                                'id'       => 'blog_title',                               
                                'title'    => esc_html__( 'Blog  Title', 'eshkool' ),
                                'subtitle' => esc_html__( 'Enter Blog  Title Here', 'eshkool' ),
                                'type'     => 'text',                                   
                            ),
                            
                            array(
                                'id'       => 'blog-layout',
                                'type'     => 'image_select',
                                'title'    => esc_html__('Select Blog Layout', 'eshkool'), 
                                'subtitle' => esc_html__('Select your blog layout', 'eshkool'),
                                'options'  => array(
                                    'full'      => array(
                                        'alt'   => 'Blog Style 1', 
                                        'img'   => get_template_directory_uri().'/libs/img/1c.png'                                      
                                    ),
                                    '2right'      => array(
                                        'alt'   => 'Blog Style 2', 
                                        'img'   => get_template_directory_uri().'/libs/img/2cr.png'
                                    ),
                                    '2left'      => array(
                                        'alt'   => 'Blog Style 3', 
                                        'img'  => get_template_directory_uri().'/libs/img/2cl.png'
                                    ),                                  
                                ),
                                'default' => '2right'
                            ),                      
                        
                            
                                    
                            array(
                                'id'       => 'blog-author-post',
                                'type'     => 'select',
                                'title'    => esc_html__('Show Author Info', 'eshkool'),                   
                                'desc'     => esc_html__('Select author info show or hide', 'eshkool'),
                                 //Must provide key => value pairs for select options
                                'options'  => array(                                            
                                        'show' => 'Show',
                                        'hide' => 'Hide'
                                        ),
                                    'default'  => 'show',
                                
                            ), 
            
                            array(
                                'id'       => 'blog-readmore',
                                'type'     => 'switch',
                                'title'    => esc_html__('Show ReadMore', 'eshkool'),                   
                                'desc'     => esc_html__('You can show/hide readmore at blog page', 'eshkool'),                                
                                'default'  => true,
                            ),

                            array(
                                'id'       => 'blog_button_text',                               
                                'title'    => esc_html__( 'Blog Button Text', 'eshkool' ),
                                'subtitle' => esc_html__( 'Enter Blog Button Text Here', 'eshkool' ),
                                'type'     => 'text',  
                                 'default' => 'Read More', 
                                 'required' => array(
                                    array(
                                        'blog-readmore',
                                        'equals',
                                        true,
                                    ),
                                ),                                
                            ),
                            
                        )
                    ) 
    
            );
    
    
    /*Single Post Sections*/
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Single Post', 'eshkool' ),
        'id'               => 'spost',
        'subsection'       => true,
        'customizer_width' => '450px',      
        'fields'           => array(                            
        
                            array(
                                    'id'       => 'blog_banner', 
                                    'url'      => true,     
                                    'title'    => esc_html__( 'Blog Single page banner', 'eshkool' ),                  
                                    'type'     => 'media',
                                    
                            ),  
                           
                            array(
                                    'id'       => 'blog-comments',
                                    'type'     => 'select',
                                    'title'    => esc_html__('Show Comment', 'eshkool'),                   
                                    'desc'     => esc_html__('Select comments show or hide', 'eshkool'),
                                     //Must provide key => value pairs for select options
                                    'options'  => array(                                            
                                            'show' => 'Show',
                                            'hide' => 'Hide'
                                            ),
                                        'default'  => 'show',
                                        
                            ),  
                            
                            array(
                                    'id'       => 'blog-author',
                                    'type'     => 'select',
                                    'title'    => esc_html__('Show Blog Meta Info', 'eshkool'),                   
                                    'desc'     => esc_html__('Select author info show or hide', 'eshkool'),
                                     //Must provide key => value pairs for select options
                                    'options'  => array(                                            
                                            'show' => 'Show',
                                            'hide' => 'Hide'
                                            ),
                                        'default'  => 'show',
                                        
                            ),  
                          
                        )
                ) 
    
    
    );

    
    /*Portfolio Sections*/
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Portfolio Section', 'eshkool' ),
        'id'               => 'portfolio',
        'customizer_width' => '450px',
        'icon' => 'el el-camera',
        'fields'           => array(
        
            array(
                    'id'       => 'portfolio_single_image', 
                    'url'      => true,     
                    'title'    => esc_html__( 'Portfolio Single page banner image', 'eshkool' ),                   
                    'type'     => 'media',
                    
            ),  
            
            array(
                    'id'       => 'portfolio_details_title',                               
                    'title'    => esc_html__( 'Portfolio Details Title', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Portfolio Details Title Here', 'eshkool' ),
                    'type'     => 'text',
                    
                ),      
             )
         ) 
    );

         /*Team Sections*/
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Team Section', 'eshkool' ),
        'id'               => 'team',
        'customizer_width' => '450px',
        'icon' => 'el el-user',
        'fields'           => array(
        
            array(
                    'id'       => 'team_single_image', 
                    'url'      => true,     
                    'title'    => esc_html__( 'Team Single page banner image', 'eshkool' ),                    
                    'type'     => 'media',
                    
            ),  
            
            array(
                    'id'       => 'team_slug',                               
                    'title'    => esc_html__( 'Team Slug', 'eshkool' ),
                    'subtitle' => esc_html__( 'Enter Team Slug Here', 'eshkool' ),
                    'type'     => 'text',
                    'default'  => 'teams',
                    
                ),      
             )
         ) 
    );

    /*Portfolio Sections*/
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Event Section', 'eshkool' ),
        'id'               => 'event',
        'customizer_width' => '450px',
        'icon' => 'el el-camera',
        'fields'           => array(
        
            array(
                    'id'       => 'event_single_image', 
                    'url'      => true,     
                    'title'    => esc_html__( 'Event Single page banner image', 'eshkool' ),                   
                    'type'     => 'media',
                    
            ),  

            array(
                    'id'       => 'event_info',                               
                    'title'    => __( 'Event Info Title', 'eshkool' ),
                    'subtitle' => __( 'Event information title here', 'eshkool' ),
                    'type'     => 'text',
                    'default'  => 'Event Details', 
                    
                ),

              array(
                    'id'       => 'event_count',                               
                    'title'    => __( 'Event Show Per Page', 'eshkool' ),                   
                    'type'     => 'text',
                    'default'  => '6',                   
                ),                

            array(
                'id'       => 'order_event',                               
                'title'    => __( 'Event Order', 'eshkool' ),
                'type'     => 'select',
                'options'  => array(                                            
                        'ASC' => 'ASC',
                        'DESC' => 'DESC'
                    ),
                'default'  => 'DESC',                   
                
            ),


            array(
                    'id'       => 'event_btn',                               
                    'title'    => __( 'Book Now Button', 'eshkool' ),
                    'subtitle' => __( 'Enter book now button text', 'eshkool' ),
                    'type'     => 'text',
                    'default'  => 'Book Now', 
                    
                ), 
            array(
                'id'       => 'btn_link',                               
                'title'    => esc_html__( 'Book Button Link', 'eshkool' ),
                'subtitle' => esc_html__( 'Enter Book Button Link Here', 'eshkool' ),
                'type'     => 'text',   
            ),  
            
            array(
                'id'       => 'event_slug',                               
                'title'    => __( 'Event Slug', 'eshkool' ),
                'subtitle' => __( 'Enter Event Slug Here', 'eshkool' ),
                'type'     => 'text',
                'default'  => 'event',                    
                ),
            array(
                'id'       => 'date_style',
                'type'     => 'select',
                'title'    => esc_html__('Select Event Date Format', 'eshkool'),                  
                'desc'     => esc_html__('Choose event date format', 'eshkool'),
                'options'  => array(                                            
                        'style1' => 'mm / dd / yyyy',
                        'style2' => 'dd / mm / yyyy'
                        ),
                    'default'  => 'style1',    
                ),
            array(
                'id'       => 'time_style',
                'type'     => 'select',
                'title'    => esc_html__('Select Event Time Format', 'eshkool'),                  
                'desc'     => esc_html__('Choose event time format', 'eshkool'),
                'options'  => array(                                            
                        'style1' => '12 Hours clock',
                        'style2' => '24 Hours Clock'
                        ),
                    'default'  => 'style1',    
                ), 
            )
         ) 
    );


     Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Course', 'eshkool' ),    
    'icon'   => 'el el-file-edit',    
        ) 
    ); 

    Redux::setSection( $opt_name, array(
                'title'            => esc_html__( 'Course', 'eshkool' ),
                'id'               => 'course_layout',
                'customizer_width' => '450px',
                'subsection' =>'true',      
                'fields'           => array(                      
                   
                         array(
                            'id'       => 'course_banner', 
                            'url'      => true,     
                            'title'    => esc_html__( 'Course Page Banner', 'eshkool' ),                    
                            'type'     => 'media',
                    
                        ), 

                        array(
                                'id'       => 'course-layout',
                                'type'     => 'image_select',
                                'title'    => esc_html__('Select Course Page Layout', 'eshkool'), 
                                
                                'options'  => array(
                                    'full'      => array(
                                        'alt'   => 'Shop Style 1', 
                                        'img'   => get_template_directory_uri().'/libs/img/1c.png'                                      
                                    ),
                                    'right-col'      => array(
                                        'alt'   => 'Shop Style 2', 
                                        'img'   => get_template_directory_uri().'/libs/img/2cr.png'
                                    ),
                                    'left-col'      => array(
                                        'alt'   => 'Shop Style 3', 
                                        'img'  => get_template_directory_uri().'/libs/img/2cl.png'
                                    ),                                  
                                ),
                                'default' => 'full'
                        ),
                        array(
                            'id'       => 'off_course_cat',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Category', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course category here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_author',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Author', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course author here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_price',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Price', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course price here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_title',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Title', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course title here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_shortdes',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Short Description', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course short description here', 'eshkool'),
                            'default'  => true,
                        ),


                        array(
                            'id'       => 'off_course_enroll_count',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Enroll Count', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course enroll count here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_review',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Review', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course review here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_time',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Time', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course time here', 'eshkool'),
                            'default'  => true,
                        ),

                        array(
                            'id'       => 'off_course_apply',
                            'type'     => 'switch', 
                            'title'    => esc_html__('Show Course Button', 'eshkool'),
                            'subtitle' => esc_html__('You can show or hide course button here', 'eshkool'),
                            'default'  => true,
                        ),



                            
                        )
                     ) 

                );




    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Social Icons', 'eshkool' ),
        'desc'   => esc_html__( 'Add your social icon here', 'eshkool' ),
        'icon'   => 'el el-share',
         'submenu' => true, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
        'fields' => array(
                    array(
                        'id'       => 'facebook',                               
                        'title'    => esc_html__( 'Facebook Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Facebook Link', 'eshkool' ),
                        'type'     => 'text',                     
                    ),
                        
                     array(
                        'id'       => 'twitter',                               
                        'title'    => esc_html__( 'Twitter Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Twitter Link', 'eshkool' ),
                        'type'     => 'text'
                    ),
                    
                        array(
                        'id'       => 'rss',                               
                        'title'    => esc_html__( 'Rss Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Rss Link', 'eshkool' ),
                        'type'     => 'text'
                    ),
                    
                     array(
                        'id'       => 'pinterest',                               
                        'title'    => esc_html__( 'Pinterest Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Pinterest Link', 'eshkool' ),
                        'type'     => 'text'
                    ),
                     array(
                        'id'       => 'linkedin',                               
                        'title'    => esc_html__( 'Linkedin Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Linkedin Link', 'eshkool' ),
                        'type'     => 'text',
                        
                    ),
                     array(
                        'id'       => 'google',                               
                        'title'    => esc_html__( 'Google Plus Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Google Plus  Link', 'eshkool' ),
                        'type'     => 'text',                       
                    ),

                    array(
                        'id'       => 'instagram',                               
                        'title'    => esc_html__( 'Instagram Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Instagram Link', 'eshkool' ),
                        'type'     => 'text',                       
                    ),

                     array(
                        'id'       => 'youtube',                               
                        'title'    => esc_html__( 'Youtube Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Youtube Link', 'eshkool' ),
                        'type'     => 'text',                       
                    ),

                    array(
                        'id'       => 'tumblr',                               
                        'title'    => esc_html__( 'Tumblr Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Tumblr Link', 'eshkool' ),
                        'type'     => 'text',                       
                    ),

                    array(
                        'id'       => 'vimeo',                               
                        'title'    => esc_html__( 'Vimeo Link', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter Vimeo Link', 'eshkool' ),
                        'type'     => 'text',                       
                    ),         
            ) 
        ) 
    );
    
    
   
    Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Footer Option', 'eshkool' ),
    'desc'   => esc_html__( 'Footer style here', 'eshkool' ),
    'icon'   => 'el el-th-large',
    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
    'fields' => array(
                
                array(
                        'id'       => 'footer_bg_image', 
                        'url'      => true,     
                        'title'    => esc_html__( 'Footer Background Image', 'eshkool' ),                 
                        'type'     => 'media',                                  
                ),

                array(
                    'id'       => 'footer_logo',
                    'type'     => 'media',
                    'title'    => esc_html__( 'Footer Logo', 'eshkool' ),
                    'subtitle' => esc_html__( 'Upload your footer logo', 'eshkool' ),                  
                ),

                array(
                    'id'       => 'footer_logo_dark',
                    'type'     => 'media',
                    'title'    => esc_html__( 'Footer Logo Dark', 'eshkool' ),
                    'subtitle' => esc_html__( 'Upload your dark footer logo', 'eshkool' ),                  
                ),
                
                array(
                        'id'       => 'show-social2',
                        'type'     => 'switch', 
                        'title'    => esc_html__('Show Social Icons at Footer', 'eshkool'),
                        'subtitle' => esc_html__('You can select Social Icons show or hide', 'eshkool'),
                        'default'  => true,
                    ),                      
                       
                
                array(
                    'id'       => 'copyright',
                    'type'     => 'textarea',
                    'title'    => esc_html__( 'Footer CopyRight', 'eshkool' ),
                    'subtitle' => esc_html__( 'Change your footer copyright text ?', 'eshkool' ),
                    'default'  => '&copy; 2018 All Rights Reserved',
                ),  

                 array(
                        'id'       => 'copyright_bg',
                        'type'     => 'color',
                        'title'    => esc_html__( 'Copyright Background', 'eshkool' ),
                        'subtitle' => esc_html__( 'Copyright Background Color', 'eshkool' ),                  
                    ),
                           

              
            ) 
        ) 
    ); 

    Redux::setSection( $opt_name, array(
                'title'            => esc_html__( 'Footer Layout', 'eshkool' ),
                'id'               => 'footer_layout',
                'customizer_width' => '450px',
                'subsection' =>'true',      
                'fields'           => array(    
                    
                    array(
                        'id'       => 'footer_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Header Layout', 'eshkool'), 
                        'subtitle' => esc_html__('Select header layout. Choose between 1, 2 ,3 or 4 layout.', 'eshkool'),
                        'options'  => array(
                            'style1'      => array(
                                'alt'   => 'Footer Style 1', 
                                'img'   => get_template_directory_uri().'/libs/img/style1.png'
                                
                            ),
                            'style2'      => array(
                                'alt'   => 'Footer Style 2', 
                                'img'   => get_template_directory_uri().'/libs/img/style2.png'
                            ),
                            'style3'      => array(
                                'alt'   => 'Footer Style 3', 
                                'img'  => get_template_directory_uri().'/libs/img/style3.png'
                            ),
                            'style4'      => array(
                                'alt'   => 'Footer Style 4', 
                                'img'   => get_template_directory_uri().'/libs/img/style4.png'
                            ),
                            'style5'      => array(
                                'alt'   => 'Footer Style 5', 
                                'img'   => get_template_directory_uri().'/libs/img/style5.png'
                            ),
                            
                        ),
                        'default' => 'style1'
                    ),                           
                    
            )
        ) 

);



    Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Woocommerce', 'eshkool' ),    
    'icon'   => 'el el-shopping-cart',    
        ) 
    ); 

    Redux::setSection( $opt_name, array(
                'title'            => esc_html__( 'Shop', 'eshkool' ),
                'id'               => 'shop_layout',
                'customizer_width' => '450px',
                'subsection' =>'true',      
                'fields'           => array(                      
                   
                         array(
                            'id'       => 'shop_banner', 
                            'url'      => true,     
                            'title'    => esc_html__( 'Shop page banner', 'eshkool' ),                    
                            'type'     => 'media',
                    
                        ), 

                        array(
                                'id'       => 'shop-layout',
                                'type'     => 'image_select',
                                'title'    => esc_html__('Select Shop Layout', 'eshkool'), 
                                'subtitle' => esc_html__('Select your shop layout', 'eshkool'),
                                'options'  => array(
                                    'full'      => array(
                                        'alt'   => 'Shop Style 1', 
                                        'img'   => get_template_directory_uri().'/libs/img/1c.png'                                      
                                    ),
                                    'right-col'      => array(
                                        'alt'   => 'Shop Style 2', 
                                        'img'   => get_template_directory_uri().'/libs/img/2cr.png'
                                    ),
                                    'left-col'      => array(
                                        'alt'   => 'Shop Style 3', 
                                        'img'  => get_template_directory_uri().'/libs/img/2cl.png'
                                    ),                                  
                                ),
                                'default' => 'full'
                            ),

                            array(
                                'id'       => 'wc_num_product',
                                'type'     => 'text',
                                'title'    => esc_html__( 'Number of Products Per Page', 'eshkool' ),
                                'default'  => '9',
                            ),

                            array(
                                'id'       => 'wc_num_product_per_row',
                                'type'     => 'text',
                                'title'    => esc_html__( 'Number of Products Per Row', 'eshkool' ),
                                'default'  => '3',
                            ),

                            array(
                                'id'       => 'wc_wishlist_icon',
                                'type'     => 'switch',
                                'title'    => esc_html__( 'Show Wishlist Icon', 'eshkool' ),
                                'on'       => esc_html__( 'Enabled', 'eshkool' ),
                                'off'      => esc_html__( 'Disabled', 'eshkool' ),
                                'default'  => true,
                            ),
                            array(
                                'id'       => 'wc_quickview_icon',
                                'type'     => 'switch',
                                'title'    => esc_html__( 'Product Quickview Icon', 'eshkool' ),
                                'on'       => esc_html__( 'Enabled', 'eshkool' ),
                                'off'      => esc_html__( 'Disabled', 'eshkool' ),
                                'default'  => true,
                            ),
                                                        
                    
                        )
                     ) 

                );

    
    
    Redux::setSection( $opt_name, array(
    'title'  => esc_html__( '404 Error Page', 'eshkool' ),
    'desc'   => esc_html__( '404 details  here', 'eshkool' ),
    'icon'   => 'el el-error-alt',
    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
    'fields' => array(

                array(
                        'id'       => 'title_404',
                        'type'     => 'text',
                        'title'    => esc_html__( 'Title', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter title for 404 page', 'eshkool' ), 
                        'default'  => '404',                
                    ),  
                
                array(
                        'id'       => 'text_404',
                        'type'     => 'text',
                        'title'    => esc_html__( 'Text', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter text for 404 page', 'eshkool' ),  
                        'default'  => 'Page Not Found',             
                    ),                      
                       
                
                array(
                        'id'       => 'back_home',
                        'type'     => 'text',
                        'title'    => esc_html__( 'Back to Home Button Label', 'eshkool' ),
                        'subtitle' => esc_html__( 'Enter label for "Back to Home" button', 'eshkool' ),
                        'default'  => 'Back to Homepage',   
                                    
                    ),                
            
                                  
            ) 
        ) 
    ); 
    


    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );
    
    //add_filter('redux/options/' . $this->args['opt_name'] . '/compiler', array( $this, 'compiler_action' ), 10, 3);

    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri()() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'eshkool' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'eshkool' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_action( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

