<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function eshkool_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'eshkool_body_classes' );
/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function eshkool_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}

add_action( 'wp_head', 'eshkool_pingback_header' );

/*
Register Fonts theme google font
*/
function eshkool_studio_fonts_url() {
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'eshkool' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Poppins|Roboto
:300,400,400i,500,600,700' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}
/*
Enqueue scripts and styles.
*/
function eshkool_studio_scripts() {
    wp_enqueue_style( 'studio-fonts', eshkool_studio_fonts_url(), array(), '1.0.0' );
}
add_action( 'wp_enqueue_scripts', 'eshkool_studio_scripts' );


//Favicon Icon
function eshkool_site_icon() {
 if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) {     
  	global $eshkool_option;
  	 
  	if(!empty($eshkool_option['rs_favicon']['url']))
  	{?>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url(($eshkool_option['rs_favicon']['url'])); ?>">	

 	<?php 
 		}
	}
}
add_filter('wp_head', 'eshkool_site_icon');



/* extra field at for user profile */

add_action( 'show_user_profile', 'eshkool_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'eshkool_show_extra_profile_fields' );

function eshkool_show_extra_profile_fields( $user ) { ?>
  <h3><?php  esc_html_e('Extra profile information', 'eshkool');?></h3>
  <table class="form-table">
    <tr>
      <th><label for="designation"><?php esc_html_e('Designation', 'eshkool');?></label></th>
      <td>
        <input type="text" name="designation" id="designation" value="<?php echo esc_attr( get_the_author_meta( 'designation', $user->ID ) ); ?>" class="regular-text" /><br />       
      </td>
    </tr>
    <tr>
      <th><label for="facebook"><?php esc_html_e('Facebook', 'eshkool'); ?></label></th>
      <td>
        <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />       
      </td>
    </tr>
    <tr>
      <th><label for="twitter"><?php esc_html_e('Twitter', 'eshkool');?></label></th>
      <td>
        <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
       
      </td>
    </tr>
    <tr>
      <th><label for="linkedin "><?php esc_html_e('Linkedin', 'eshkool');?></label></th>
      <td>
        <input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />       
      </td>
    </tr>
    <tr>
      <th><label for="google"><?php esc_html_e('Google Plus', 'eshkool');?></label></th>

      <td>
        <input type="text" name="google" id="google" value="<?php echo esc_attr( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text" /><br />       
      </td>
    </tr>
    <tr>
      <th><label for="youtube"><?php esc_html_e('Youtube', 'eshkool');?></label></th>
      <td>
        <input type="text" name="youtube" id="youtube" value="<?php echo esc_attr( get_the_author_meta( 'youtube', $user->ID ) ); ?>" class="regular-text" /><br />       
      </td>
    </tr>
  </table>
<?php }
/* update user profile field */
add_action( 'personal_options_update', 'eshkool_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'eshkool_save_extra_profile_fields' );

function eshkool_save_extra_profile_fields( $user_id ) {

  if ( !current_user_can( 'edit_user', $user_id ) )
    return false;
  update_user_meta( $user_id, 'designation', $_POST['designation'] );
  update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
  update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
  update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
  update_user_meta( $user_id, 'google', $_POST['google'] );
  update_user_meta( $user_id, 'youtube', $_POST['youtube'] );

}


/* Excerpt Define for Search */
function eshkool_wpex_get_excerpt( $args = array() ) {

  // Defaults
  $defaults = array(
    'post'            => '',
    'length'          => 40,
    'readmore'        => false,
    'readmore_text'   => esc_html__( 'read more', 'eshkool' ),
    'readmore_after'  => '',
    'custom_excerpts' => true,
    'disable_more'    => false,
  );

  // Apply filters
  $defaults = apply_filters( 'eshkool_wpex_get_excerpt_defaults', $defaults );

  // Parse args
  $args = wp_parse_args( $args, $defaults );

  // Apply filters to args
  $args = apply_filters( 'eshkool_wpex_get_excerpt_args', $defaults );

  // Extract
  extract( $args );

  // Get global post data
  if ( ! $post ) {
    global $post;
  }

  // Get post ID
  $post_id = $post->ID;

  // Check for custom excerpt
  if ( $custom_excerpts && has_excerpt( $post_id ) ) {
    $output = $post->post_excerpt;
  }

  // No custom excerpt...so lets generate one
  else {

    // Readmore link
    $readmore_link = '<a href="' . get_permalink( $post_id ) . '" class="readmore">' . $readmore_text . $readmore_after . '</a>';

    // Check for more tag and return content if it exists
    if ( ! $disable_more && strpos( $post->post_content, '<!--more-->' ) ) {
      $output = apply_filters( 'the_content', get_the_content( $readmore_text . $readmore_after ) );
    }

    // No more tag defined so generate excerpt using wp_trim_words
    else {

      // Generate excerpt
      $output = wp_trim_words( strip_shortcodes( $post->post_content ), $length );

      // Add readmore to excerpt if enabled
      if ( $readmore ) {

        $output .= apply_filters( 'eshkool_wpex_readmore_link', $readmore_link );

      }

    }

  }

  // Apply filters and echo
  return apply_filters( 'eshkool_wpex_get_excerpt', $output );

}

/* Excerpt Define for Blog */
function eshkool_blog_get_excerpt( $args = array() ) {

  // Defaults
  $defaults = array(
    'post'            => '',
    'length'          => 16,
    'readmore'        => false,
    'readmore_text'   => esc_html__( 'read more', 'eshkool' ),
    'readmore_after'  => '',
    'custom_excerpts' => true,
    'disable_more'    => false,
  );

  // Apply filters
  $defaults = apply_filters( 'eshkool_blog_get_excerpt_defaults', $defaults );

  // Parse args
  $args = wp_parse_args( $args, $defaults );

  // Apply filters to args
  $args = apply_filters( 'eshkool_blog_get_excerpt_args', $defaults );

  // Extract
  extract( $args );

  // Get global post data
  if ( ! $post ) {
    global $post;
  }

  // Get post ID
  $post_id = $post->ID;

  // Check for custom excerpt
  if ( $custom_excerpts && has_excerpt( $post_id ) ) {
    $output = $post->post_excerpt;
  }

  // No custom excerpt...so lets generate one
  else {

    // Readmore link
    $readmore_link = '<a href="' . get_permalink( $post_id ) . '" class="readmore">' . $readmore_text . $readmore_after . '</a>';

    // Check for more tag and return content if it exists
    if ( ! $disable_more && strpos( $post->post_content, '<!--more-->' ) ) {
      $output = apply_filters( 'the_content', get_the_content( $readmore_text . $readmore_after ) );
    }

    // No more tag defined so generate excerpt using wp_trim_words
    else {

      // Generate excerpt
      $output = wp_trim_words( strip_shortcodes( $post->post_content ), $length );

      // Add readmore to excerpt if enabled
      if ( $readmore ) {

        $output .= apply_filters( 'eshkool_wpex_readmore_link', $readmore_link );

      }

    }

  }

  // Apply filters and echo
  return apply_filters( 'eshkool_blog_get_excerpt', $output );
}

//demo content file include here
function eshkool_import_files() {
  return array(
    array(
      'import_file_name'           => 'Eshkool Demo Import',
      'categories'                 => array( 'Category 1' ),
      'import_file_url'            => trailingslashit( get_template_directory_uri() ) . 'ocdi/eshkool.wordpress.xml',
      'import_widget_file_url'     => trailingslashit( get_template_directory_uri() ) . 'ocdi/eshkool-widgets.wie',      
      'import_redux'               => array(
	        array(
	          'file_url'    => trailingslashit( get_template_directory_uri() ) . 'ocdi/redux_options_eshkool.json',
	          'option_name' => 'eshkool_option',
	        ),
     	),
      
      'import_notice'              => esc_html__( 'Caution: For importing demo data please click on "Import Demo Data" button. During demo data installation please do not refresh the page. For more info plesae read the documentation carefully.', 'eshkool' ),
      
    ),
    
  );
}

add_filter( 'pt-ocdi/import_files', 'eshkool_import_files' );
function eshkool_after_import_setup() {
  // Assign menus to their locations.
  $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
  $single_menu = get_term_by( 'name', 'One Page Menu', 'nav_menu' );
  

  set_theme_mod( 'nav_menu_locations', array(
      'menu-1' => $main_menu->term_id,
      'menu-2' => $single_menu->term_id,
        
    )
  );

  // Assign front page and posts page (blog page).
  $front_page_id = get_page_by_title( 'Home' );
  $blog_page_id  = get_page_by_title( 'Blog' );
  update_option( 'show_on_front', 'page' );
  update_option( 'page_on_front', $front_page_id->ID );
  update_option( 'page_for_posts', $blog_page_id->ID ); 

      //Import Revolution Slider
    if ( class_exists( 'RevSlider' ) ) {
        $slider_array = array(
            get_template_directory()."/ocdi/sliders/classic.zip",
            get_template_directory()."/ocdi/sliders/slider1.zip",
            get_template_directory()."/ocdi/sliders/slider-2.zip",
            get_template_directory()."/ocdi/sliders/slider-3.zip",
            get_template_directory()."/ocdi/sliders/slider-4.zip",
            get_template_directory()."/ocdi/sliders/slider-5.zip",
            get_template_directory()."/ocdi/sliders/slider6.zip",
            get_template_directory()."/ocdi/sliders/slider7.zip",
            get_template_directory()."/ocdi/sliders/kindergarten.zip",
            get_template_directory()."/ocdi/sliders/university.zip",
            get_template_directory()."/ocdi/sliders/Instructor.zip",      
            get_template_directory()."/ocdi/sliders/home-13.zip"      
        );

        $slider = new RevSlider();

        foreach($slider_array as $filepath){
           $slider->importSliderFromPost(true,true,$filepath);  
        }
    }  
}
add_action( 'pt-ocdi/after_import', 'eshkool_after_import_setup' );