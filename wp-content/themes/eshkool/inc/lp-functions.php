<?php
/**
 * Custom functions for LearnPress 3.x
 *
 */
if ( ! function_exists( 'eshkool_remove_learnpress_hooks' ) ) {
    function eshkool_remove_learnpress_hooks() {
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_begin_meta', 10 );
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_price', 20 );
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_instructor', 25 );
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_end_meta', 30 );
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_course_loop_item_buttons', 35 );
        remove_action( 'learn-press/after-courses-loop-item', 'learn_press_course_loop_item_user_progress', 40 );
        remove_action( 'learn-press/before-main-content', 'learn_press_breadcrumb', 10 );
        remove_action( 'learn-press/before-main-content', 'learn_press_search_form', 15 );
        remove_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_start_wrapper', 5 );
        remove_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_end_wrapper', 15 );
        remove_action( 'learn-press/content-landing-summary', 'learn_press_course_price', 25 );
        remove_action( 'learn-press/content-landing-summary', 'learn_press_course_buttons', 30 );      
    }
}
add_action( 'after_setup_theme', 'eshkool_remove_learnpress_hooks', 15 );

add_action( 'wp_enqueue_scripts', 'eshkool_load_dashicons_front_end' );
function eshkool_load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}

//single course aautor info
function eshkool_learn_press_course_instructor(){
    $course_id      = get_the_ID();    
    $rstheme_course = LP()->global['course'];
    $course_author = get_post_field( 'post_author', $course_id );
    $designation   = get_the_author_meta( 'designation' );
    ?>
        <div class="author">
            <div class="image">
                <a class="rs-author" href="<?php echo esc_url( learn_press_user_profile_link( $course_author ) );?>"><?php echo get_avatar( $course_author, 60 ); ?>
                </a>
            </div> 
            <div class="author-name"> <a class="rs-author" href="<?php echo esc_url( learn_press_user_profile_link( $course_author ) );?>"> <?php the_author(); ?></a>
                <p><?php echo esc_attr($designation); ?></p>
            </div>    
        </div>
    <?php 
}

add_action('eshkool_single_course_meta', 'eshkool_learn_press_course_instructor', 5);

//single course category info

function eshkool_learn_press_course_categories(){
    ?>
        <div class="categories">
            <?php learn_press_course_categories(); ?>
            <p><?php echo esc_html('Categorires', 'eshkool');?></p>
        </div>
    <?php
}
add_action('eshkool_single_course_meta', 'eshkool_learn_press_course_categories', 15);

/** Display course ratings
 */
if ( ! function_exists( 'eshkool_course_ratings' ) ) {
    function eshkool_course_ratings() {       
        if ( function_exists( 'learn_press_get_course_rate' ) ) {
            $course_id      = get_the_ID();    
            $rstheme_course = LP()->global['course'];
            $course_rate_res = learn_press_get_course_rate( $course_id, false );
            $course_rate     = $course_rate_res['rated'];
            $course_rate_total = $course_rate_res['total'];
            $course_rate_text = $course_rate_total > 1 ? esc_html__( 'Reviews', 'eshkool' ) : esc_html__( 'Review', 'eshkool' );
        } 


        if ( function_exists( 'learn_press_get_course_rate' ) ) : ?>  
        <div class="client-rating">
            <span class="course-rating-total"> <?php echo esc_html( $course_rate_total );?> </span> <?php echo esc_html( $course_rate_text );?>
            <?php learn_press_course_review_template( 'rating-stars.php', array( 'rated' => $course_rate ) );?> 
        </div>
        <?php endif;        
    }
}


add_action('eshkool_single_course_meta', 'eshkool_course_ratings', 25);
;

add_action( 'eshkool_single_course_payment', 'learn_press_course_buttons', 20 ); 

//Show single course information
if( !function_exists( 'eshkool_single_course_feature_info' )){
    function eshkool_single_course_feature_info(){ 

        $course    = LP()->global['course'];
        $course_id = get_the_ID();

        $course_skill_level = get_post_meta( $course_id, 'eshkool_course_skill_level', true );
        $course_language    = get_post_meta( $course_id, 'eshkool_course_language', true );
        $duration           = get_post_meta( $course_id, '_lp_duration', true );
        $duration_type      = get_post_meta( $course_id, '_lp_duration_select', true );
        $duration_total     = $duration.' '.$duration_type;

        ?>
        <div class="course-features-info">
        <h3 class="title"> <?php esc_html_e( 'Course Features', 'eshkool' ); ?></h3>
            <ul>
                <li class="lectures-feature">
                    <i class="fa fa-files-o"></i>
                    <span class="label"><?php esc_html_e( 'Lectures', 'eshkool' ); ?></span>
                    <span class="value"><?php $couser_get = $course->get_curriculum_items('lp_lesson') ? count( $course->get_curriculum_items('lp_lesson') ) : 0; 
                            
                            echo wp_kses_post($couser_get);
                    
                    ?></span>
                </li>
               
                <li class="quizzes-feature">
                    <i class="fa fa-puzzle-piece"></i>
                    <span class="label"><?php esc_html_e( 'Quizzes', 'eshkool' ); ?></span>
                    <span class="value"><?php $quiq_item = $course->get_curriculum_items('lp_quiz') ? count( $course->get_curriculum_items('lp_quiz') ) : 0; 
                        echo wp_kses_post($quiq_item);
                    ?></span>
                </li>
               
                    <li class="duration-feature">
                        <i class="fa fa-clock-o"></i>
                        <span class="label"><?php esc_html_e( 'Duration', 'eshkool' ); ?></span>
                        <span class="value"><?php echo esc_html($duration_total); ?></span>
                    </li>
              
                <?php if ( ! empty( $course_skill_level ) ): ?>
                    <li class="skill-feature">
                        <i class="fa fa-level-up"></i>
                        <span class="label"><?php esc_html_e( 'Skill level', 'eshkool' ); ?></span>
                        <span class="value"><?php echo esc_html( $course_skill_level ); ?></span>
                    </li>
                <?php endif; ?>
                <?php if ( ! empty( $course_language ) ): ?>
                    <li class="language-feature">
                        <i class="fa fa-language"></i>
                        <span class="label"><?php esc_html_e( 'Language', 'eshkool' ); ?></span>
                        <span class="value"><?php echo esc_html( $course_language ); ?></span>
                    </li>
                <?php endif; ?>
                <li class="students-feature">
                    <i class="fa fa-users"></i>
                    <span class="label"><?php esc_html_e( 'Students', 'eshkool' ); ?></span>
                    <?php $user_count = $course->get_users_enrolled() ? $course->get_users_enrolled() : 0; ?>
                    <span class="value"><?php echo esc_html( $user_count ); ?></span>
                </li>
               
                <li class="assessments-feature">
                    <i class="fa fa-check-square-o"></i>
                    <span class="label"><?php esc_html_e( 'Assessments', 'eshkool' ); ?></span>
                    <span class="value"><?php echo ( get_post_meta( $course_id, '_lp_course_result', true ) == 'evaluate_lesson' ) ? esc_html__( 'Yes', 'eshkool' ) : esc_html__( 'Self', 'eshkool' ); ?></span>
                </li>
            </ul>
        </div>
        <?php
    }
}

add_action( 'eshkool_single_course_feature', 'eshkool_single_course_feature_info' );


//Custom metabox for course

//test for admin metabox for learnpress
/**
 * Add some meta data for a course
 *
 * @param $meta_box
 */
if ( ! function_exists( 'eshkool_add_course_meta' ) ) {
    function eshkool_add_course_meta( $meta_box ) {
        $fields             = $meta_box['fields'];
        
        $fields[]           = array(
            'name' => esc_html__( 'Skill Levels', 'eshkool' ),
            'id'   => 'eshkool_course_skill_level',
            'type' => 'text',
            'desc' => esc_html__( 'A possible level with this course', 'eshkool' ),
            'std'  => esc_html__( 'All levels', 'eshkool' )
        );
        $fields[]           = array(
            'name' => esc_html__( 'Languages', 'eshkool' ),
            'id'   => 'eshkool_course_language',
            'type' => 'text',
            'desc' => esc_html__( 'Language\'s used for studying', 'eshkool' ),
            'std'  => esc_html__( 'English', 'eshkool' )
        );       
         $fields[]           = array(
            'name' => esc_html__( 'Course Color', 'eshkool' ),
            'id'   => 'eshkool_course_color',
            'type' => 'text',
            'desc' => esc_html__( 'Colors\'s used for each course(ex: #ff0000)', 'eshkool' ),
            
        );       
        $meta_box['fields'] = $fields;

        return $meta_box;
    }

}

add_filter( 'learn_press_course_settings_meta_box_args', 'eshkool_add_course_meta' );

/**
 * Display related courses
 */
if ( ! function_exists( 'eshkool_related_courses' ) ) {
    function eshkool_related_courses() {
        $related_courses = eshkool_get_related_courses( 5 );
        $theme_options_data = get_theme_mods();
        $style_content = isset($theme_options_data['eshkool_layout_content_page']) ? $theme_options_data['eshkool_layout_content_page'] : 'normal';

        if ( $related_courses ) {
            $layout_grid = get_theme_mod('eshkool_learnpress_cate_layout_grid', '');           
            ?>
            <div class="related-courses rs-courses-3">
                <div class="eshkool-ralated-course">               
                    <h2 class="related-title">
                        <?php esc_html_e( 'Related Course', 'eshkool' ); ?>
                    </h2>
                </div>    
                <div class="rs-carousel owl-carousel">                        
                    <?php foreach ( $related_courses as $course_item ) : ?>
                        <?php
                        $course = learn_press_get_course( $course_item->ID );
                        $is_required = $course->is_required_enroll();
                    ?>
                   
                        <div class="course-item">
                            <div class="course-img">
                                <a class="thumb" href="<?php echo get_the_permalink( $course_item->ID ); ?>">
                                    <?php
                                         $post_img_url = get_the_post_thumbnail($course_item->ID);
                                          echo wp_kses_post($post_img_url);
                                    ?>
                                    
                                </a>                                   
                            </div>
                            <div class="course-body">
                                <div class="course-author">
                                    <div class="course_author_img">
                                        <a href="<?php echo esc_url( learn_press_user_profile_link( $course_item->post_author ) ); ?>">
                                            <?php echo get_avatar( $course_item->post_author, 40 ); ?>
                                        </a>
                                    </div>
                                    <div class="author-contain">
                                        <div class="course-price">                                            
                                            <?php if ( $price = $course->get_price_html() ) {
                                                $origin_price = $course->get_origin_price_html();
                                                $sale_price   = $course->get_sale_price();
                                                $sale_price   = isset( $sale_price ) ? $sale_price : '';
                                                $class        = '';
                                                if ( $course->is_free() || ! $is_required ) {
                                                    $class .= ' free-course';
                                                    $price = esc_html__( 'Free', 'eshkool' );
                                                }
                                                ?>
                                                <div class="course-price">
                                                    <div class="value<?php echo esc_attr($class); ?>">
                                                        <?php
                                                        if ( $sale_price ) {
                                                            echo '<span class="course-origin-price">' . $origin_price . '</span>';
                                                        }
                                                        ?>
                                                        <?php echo esc_html($price); ?>
                                                    </div>                                                
                                                </div>
                                                <?php
                                            }?>                                     
                                        </div>
                                    </div>
                                </div>
                                <h2 class="course-title">
                                    <a rel="bookmark"
                                       href="<?php echo get_the_permalink( $course_item->ID ); ?>"><?php echo esc_html( $course_item->post_title ); ?></a>
                                </h2> <!-- .entry-header -->
                                <div class="course-meta">
                                    <?php
                                    $count_student = $course->get_users_enrolled() ? $course->get_users_enrolled() : 0;
                                    ?>
                                    <div class="course-students">                                          
                                        <?php do_action( 'learn_press_begin_course_students' ); ?>

                                        <div class="value"><i class="fa fa-group"></i>
                                            <?php echo esc_html( $count_student ); ?>
                                        </div>
                                        <?php do_action( 'learn_press_end_course_students' ); ?>

                                    </div>
                                    <?php eshkool_course_ratings_count( $course_item->ID ); ?>  
                                </div>
                            </div>
                        </div>
                      
                    <?php endforeach; ?>                        
                </div>                
            </div>
            <?php
        }
    }
}

if ( ! function_exists( 'eshkool_get_related_courses' ) ) {
    function eshkool_get_related_courses( $limit ) {       
        $course_id = get_the_ID();
        $tag_ids = array();
        $tags    = get_the_terms( $course_id, 'course_tag' );

        if ( $tags ) {
            foreach ( $tags as $individual_tag ) {
                $tag_ids[] = $individual_tag->slug;
            }
        }

        $args = array(
            'posts_per_page'      => -1,
            'paged'               => 1,
            'ignore_sticky_posts' => 1,
            'post__not_in'        => array( $course_id ),
            'post_type'           => 'lp_course'
        );

        if ( $tag_ids ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'course_tag',
                    'field'    => 'slug',
                    'terms'    => $tag_ids
                )
            );
        }
        $related = array();
        if ( $posts = new WP_Query( $args ) ) {
            global $post;
            while ( $posts->have_posts() ) {
                $posts->the_post();
                $related[] = $post;
            }
        }
        wp_reset_query();

        return $related;
    }
}


/**
 * Display ratings count
 */

if ( ! function_exists( 'eshkool_course_ratings_count' ) ) {
    function eshkool_course_ratings_count( $course_id = null ) {
        
        if ( ! $course_id ) {
            $course_id = get_the_ID();
        }
        $ratings = learn_press_get_course_rate_total( $course_id ) ? learn_press_get_course_rate_total( $course_id ) : 0;
        echo '<div class="course-comments-count">';
        echo '<div class="value"><i class="fa fa-comment"></i>';
        echo esc_html( $ratings );
        echo '</div>';
        echo '</div>';
    }
}?>