<?php
/**
 /**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eshkool_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'eshkool' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'eshkool' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	if ( class_exists( 'WooCommerce' ) ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar Shop', 'eshkool' ),
				'id'            => 'sidebar_shop',
				'description'   => esc_html__( 'Sidebar Shop', 'eshkool' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
		}

		if ( class_exists( 'LearnPress' ) ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar Courses', 'eshkool' ),
				'id'            => 'sidebar_courses',
				'description'   => esc_html__( 'Sidebar Courses', 'eshkool' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
		}


 	register_sidebar( array(
			'name' => esc_html__( 'Footer Top One Widget Area', 'eshkool' ),
			'id' => 'footer_top_1',
			'description' => esc_html__( 'Footer Top One Widget Here', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-top-title">',
			'after_title' => '</h3>'
	) ); 		 				

	register_sidebar( array(
			'name' => esc_html__( 'Footer Top Two Widget Area', 'eshkool' ),
			'id' => 'footer_top_2',
			'description' => esc_html__( 'Footer Top Two Widget Here', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-top-title">',
			'after_title' => '</h3>'
	) ); 
	register_sidebar( array(
			'name' => esc_html__( 'Footer Top Three Widget Area', 'eshkool' ),
			'id' => 'footer_top_3',
			'description' => esc_html__( 'Footer Top Three Widget Here', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-top-title">',
			'after_title' => '</h3>'
	) );


 	register_sidebar( array(
			'name' => esc_html__( 'Footer One Widget Area', 'eshkool' ),
			'id' => 'footer1',
			'description' => esc_html__( 'Add Footer Three Widget here', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-title">',
			'after_title' => '</h3>'
	) ); 		 				

	register_sidebar( array(
			'name' => esc_html__( 'Footer Two Widget Area', 'eshkool' ),
			'id' => 'footer2',
			'description' => esc_html__( 'Add text box widgets area', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-title">',
			'after_title' => '</h3>'
	) ); 
	register_sidebar( array(
			'name' => esc_html__( 'Footer Three Widget Area', 'eshkool' ),
			'id' => 'footer3',
			'description' => esc_html__( 'Add text box widgets area', 'eshkool' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="footer-title">',
			'after_title' => '</h3>'
	) ); 
	register_sidebar( array(
			'name' => esc_html__( 'Footer Four Widget Area', 'eshkool' ),
			'id' => 'footer4',
			'description' => esc_html__( 'Add text box widgets area', 'eshkool' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="footer-title">',
			'after_title' => '</h3>'
	) );
				
}
add_action( 'widgets_init', 'eshkool_widgets_init' );