<?php
/**
 * @author  rs-theme
 * @version 1.0
 */
/* All Functions for woocommerce
-----------------------------------------*/
/*-------------------------------------
#. Theme supports for WooCommerce
---------------------------------------*/

function eshkool_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
  }
add_action( 'after_setup_theme', 'eshkool_add_woocommerce_support' );


function eshkool_wc_shop_thumb_area(){
	get_template_part( 'template-parts/wo-templates/content', 'shop-thumb' );
}

/* Shop hide default page title */
function eshkool_wc_hide_page_title(){
	return false;
}

function eshkool_wc_loop_product_title(){
	echo '<h2 class="woocommerce-loop-product__title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2>';
}

if(!empty($eshkool_option['wc_num_product'])):
	function eshkool_wc_loop_shop_per_page(){
		global $eshkool_option;
		return $eshkool_option['wc_num_product'];
	}
add_action( 'loop_shop_per_page', 'eshkool_wc_loop_shop_per_page' );
endif;

// Change number or products per row 
if(!empty($eshkool_option['wc_num_product_per_row'])):
if (!function_exists('loop_columns')) {
	function eshkool_loop_columns() {
		global $eshkool_option;
		return $eshkool_option['wc_num_product_per_row']; 
	}
}
add_filter('loop_shop_columns', 'eshkool_loop_columns');
endif;

/*All hoocks for woocommerce*
-------------------------------------------*/


 
/* Breadcrumb */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

/* Shop loop */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
;
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );


add_action( 'woocommerce_before_shop_loop_item_title', 'eshkool_wc_shop_thumb_area', 11 );

add_filter( 'woocommerce_show_page_title', 'eshkool_wc_hide_page_title' );

add_action( 'woocommerce_shop_loop_item_title', 'eshkool_wc_loop_product_title', 10 );
?>