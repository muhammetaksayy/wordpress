<?php
/*
Header style 1
*/
global $eshkool_option;
$sticky      = $eshkool_option['off_sticky']; 
$sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
$has_quote   = $quote_right = '';
$header_width_meta = get_post_meta(get_the_ID(), 'header_width_custom', true);

if ($header_width_meta != ''){
  $header_width = ( $header_width_meta == 'full' ) ? 'container-fluid': 'container';
}else{
  $header_width = 'container';
}
?>
<header id="rs-header" class="header-transparent style1">
    <div class="header-inner<?php echo esc_attr($sticky_menu);?>">
        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['show-top'])){
                $top_bar = $eshkool_option['show-top'];
                if($top_bar == ""){?>
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-two');
                    ?>
                    </div>
                <?php
                }
                else{
                    if($top_bar =='1'){       
                        get_template_part('inc/header/top-head/top-head-two');
                    } 
                }
            }
        ?>
        <!-- Toolbar End -->
        
        <!-- Header Menu Start -->
        <div class="menu-area">
        <div class="<?php echo esc_attr($header_width);?>">
          <div class="row">
            <div class="col-sm-3 header-logo">
              <?php  get_template_part('inc/header/logo/logo'); ?>
            </div>

            <?php if(!empty($eshkool_option['quote'])){ 
                $has_quote = ' has-quote-text';
            }
            ?>
            <div class="col-sm-9 menu-responsive<?php echo esc_attr($has_quote); ?>">   
                <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                        $menu_right='nav-right-bar';
                    else:
                        $menu_right=''; 
                    endif;
                    get_template_part('inc/header/menu');
                    echo get_template_part('inc/header/search');
                    
                ?>
                <?php 
                    if(!empty($eshkool_option['quote'])):   
                      $quote_right = $eshkool_option['quote'];                     
                    endif;
                    $rs_quote_menu = ( $quote_right == 1 ) ? 'rs-quote-off' : '';

                    if(!empty($eshkool_option['quote'])) { ?>   
                      <div class="get-quote <?php echo esc_attr($rs_quote_menu);?>">
                      <?php if (!empty($eshkool_option['quote'])) { ?>
                      <a href="<?php echo esc_url($eshkool_option['quote_link']); ?>" class="quote-button"><?php  echo esc_html($eshkool_option['quote']); ?></a>                            
                      <?php }
                       ?> 
                      </div>
                  <?php } ?>
            </div>
          </div>
        </div>
         
        </div>
        <!-- Header Menu End -->
    </div>
     <!-- End Slider area  -->
   <?php 
  if(is_page())
  {
    get_template_part( 'inc/page-header/breadcrumbs' );
  }
  ?>
</header>
<?php  
  get_template_part('inc/header/slider/slider'); ?>
 
