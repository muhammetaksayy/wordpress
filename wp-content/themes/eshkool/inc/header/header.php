<?php
/*
Header Style
*/
global $eshkool_option;
if(is_page()){
	 //check individual header 
    $post_meta_header = get_post_meta(get_the_ID(), 'header_select', true);	 
	if($post_meta_header!=''){		 	
		if($post_meta_header == 'header2'){		 
			get_template_part('inc/header/header-style2');				
		}
		if($post_meta_header == 'header3'){		 
			get_template_part('inc/header/header-style3');		
		}
		if($post_meta_header == 'header4'){		 
			get_template_part('inc/header/header-style4');		
		}	 
		if($post_meta_header == 'header1'){		
			get_template_part('inc/header/header-style1');       
		}
		if($post_meta_header == 'header5'){		
			get_template_part('inc/header/header-style-kids');       
		}
		if($post_meta_header == 'header8'){		
			get_template_part('inc/header/header-style5');       
		}
		if($post_meta_header == 'header9'){		
			get_template_part('inc/header/header-style6');       
		}
		if($post_meta_header == 'header10'){		
			get_template_part('inc/header/header-style7');       
		}
	}
	else if(!empty($eshkool_option['header_layout'])){		 
		$header_style = $eshkool_option['header_layout'];	 			 
		if($header_style == 'style2'){		 
			get_template_part('inc/header/header-style2');		
		}
		if($header_style == 'style3'){		 
			get_template_part('inc/header/header-style3');		
		}
		if($header_style == 'style4'){		 
			get_template_part('inc/header/header-style4');		
		}	 
		if($header_style == 'style1'){		
			get_template_part('inc/header/header-style1');       
		}
		if($header_style == 'style5'){		
			get_template_part('inc/header/header-style-kids');       
		}
		if($header_style == 'style6'){		
			get_template_part('inc/header/header-style6');       
		}
		if($header_style == 'style7'){		
			get_template_part('inc/header/header-style7');       
		}	  
	}
		
	else{
		get_template_part('inc/header/header-style1'); 
	}
		
}
	elseif(!empty($eshkool_option['header_layout']))
	 	{	//checking header style form global settings
		 $header_style = $eshkool_option['header_layout'];	 
		 if($header_style == 'style1'){		
			get_template_part('inc/header/header-style1');       
		 }
		 if($header_style == 'style2'){		 
			get_template_part('inc/header/header-style2');		
		 }
		  if($header_style == 'style3'){		 
			get_template_part('inc/header/header-style3');		
		 }
		if($header_style == 'style4'){		 
			get_template_part('inc/header/header-style4');		
		}
		if($header_style == 'style6'){		 
			get_template_part('inc/header/header-style6');		
		}	
		if($header_style == 'style7'){		 
			get_template_part('inc/header/header-style7');		
		}		
		if($header_style == 'style5'){		
			get_template_part('inc/header/header-style-kids');       
 		} 
			 
	}
else{
	get_template_part('inc/header/header-style1');
}	 
?>