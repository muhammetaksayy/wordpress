<?php
global $eshkool_option;
if(is_page()){
    $post_meta_header = get_post_meta(get_the_ID(), 'select-logo', true); 
    if($post_meta_header == 'light'){ ?>
    <div class="logo-area">
        <?php
           if (!empty( $eshkool_option['logo_light']['url'] ) ) { ?>
        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $eshkool_option['logo_light']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
        <?php }	else{?>
          <h1 id="logo">
              <span class="site-name"><a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
              </span>
          </h1><!-- end of #logo -->
        <?php } 
        ?>
    </div>
  <?php } else {?>

    <div class="logo-area">
        <?php
           if (!empty( $eshkool_option['logo']['url'] ) ) { ?>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $eshkool_option['logo']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
        <?php } else{?>
          <h1 id="logo">
              <span class="site-name"><a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
              </span>
          </h1><!-- end of #logo -->
        <?php } 
        ?>
    </div>
<?php }

 if (!empty( $eshkool_option['rswplogo_sticky']['url'] ) ) { ?>
    <div class="logo-area sticky-logo">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $eshkool_option['rswplogo_sticky']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
       </div>
    <?php } 
    }
else{
  ?>
  <div class="logo-area">
    <?php
       if (!empty( $eshkool_option['logo']['url'] ) ) { ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $eshkool_option['logo']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
    <?php }	else{?>
      <h1 id="logo">
          <span class="site-name"><a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
          </span>
      </h1><!-- end of #logo -->
    <?php } 
    ?>
  </div>
  <?php

  if (!empty( $eshkool_option['rswplogo_sticky']['url'] ) ) { ?>
    <div class="logo-area sticky-logo">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url( $eshkool_option['rswplogo_sticky']['url']); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
       </div>
    <?php }
} ?>