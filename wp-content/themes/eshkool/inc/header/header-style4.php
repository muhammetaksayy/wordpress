<?php
    /*
        header style 4
    */
    global $eshkool_option;
    $sticky = $eshkool_option['off_sticky']; 
    $sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
?>

<header id="rs-header" class="header-style-4">

    <div class="header-inner<?php echo esc_attr($sticky_menu);?>">

        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['show-top'])){
                $top_bar = $eshkool_option['show-top'];
                if($top_bar == ""){?>
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-two');
                    ?>
                    </div>
                <?php
                }
                else{
                    if($top_bar =='1'){       
                        get_template_part('inc/header/top-head/top-head-two');
                    } 
                }
            }
        ?>
        <!-- Toolbar End -->
        
        <!-- Header Menu Start -->
        <div class="menu-area">
            <!-- Header Middle Start -->
            <div class="container main-menu-responsive">
                <div class="row toolbar-contact-style4">
                    <div class="col-md-4 col-sm-12">
                        <?php if(!empty($eshkool_option['top-email'])) { ?>
                        <div class="t-con-inner">
                            <i class="icon-basic-mail-open"></i>
                            <span class="contact-inf">
                                <span><?php if(!empty($eshkool_option['email-pretext'])): echo esc_html($eshkool_option['email-pretext']); endif;?> </span>
                                <a href="mailto:<?php echo esc_attr($eshkool_option['top-email'])?>"><?php echo esc_html($eshkool_option['top-email'])?></a> 
                            </span>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <?php get_template_part('inc/header/logo/logo'); ?>                        
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <?php if(!empty($eshkool_option['phone'])) { ?>
                        <div class="t-con-inner float-right">
                            <i class="icon-basic-tablet"></i>
                            <span class="contact-inf">
                              <span><?php if(!empty($eshkool_option['phone-pretext'])): echo esc_html($eshkool_option['phone-pretext']); endif;?> </span>
                              <a href="tel:+<?php echo esc_attr(str_replace(" ","",($eshkool_option['phone'])))?>"> <?php echo esc_html($eshkool_option['phone']); ?></a> 
                            </span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- Header Middle End -->

            <div class="fullwidth_menu menu-responsive">
                <div class="container">
                    <div class="row">
                         <div class="col-md-10">  
                             <?php 
                                get_template_part('inc/header/menu');
                              ?> 
                         </div> 
                         <div class="col-md-2"> 
                              <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                                    $menu_right = 'nav-right-bar';
                                  else:
                                    $menu_right=''; 
                                    endif;  
                              ?>
                              <div class="sidebarmenu-area text-right">
                                  <?php 
                                    //search box here
                                    get_template_part('inc/header/search');
                                    //off convas here
                                    get_template_part('inc/header/off-canvas');
                                  ?> 
                              </div>
                        </div>
                   </div>
                </div>  
            </div>

        </div>
    </div>
 <!-- Slider Start Here -->
    <?php  get_template_part('inc/header/slider');?>
  <?php 
  if(is_page())
  {
      get_template_part( 'inc/page-header/breadcrumbs' );
  }
  ?>
  <!-- Header Menu End --> 
</header>