<?php
/* Top Header part for grassy template
*/
global $eshkool_option;
if(!empty($eshkool_option['kids-show-top'])){ 
    if(is_page()){
        $rs_top_bar = get_post_meta(get_the_ID(), 'select-top', true);
        if($rs_top_bar == 'show' || $rs_top_bar == ''){ ?> 
            <div class="kids-toolbar-area toolbar-area">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-sm-7 col-xs-12">
                    <div class="toolbar-contact">
                      <ul class="rs-contact-info">

                      <?php if(!empty($eshkool_option['kids-welcome-text'])) { ?>
                        <li> <?php echo esc_html($eshkool_option['kids-welcome-text']); ?> </li>
                      <?php } ?> 
                      <?php if(!empty($eshkool_option['kids-phone'])) { ?>
                      <li class="rs-contact-phone">
                          <i class="icon-basic-tablet"></i>                 
                            <a href="tel:+<?php echo esc_attr(str_replace(" ","",($eshkool_option['kids-phone'])))?>"> <?php echo esc_html($eshkool_option['kids-phone']); ?></a>                   
                      </li>
                      <?php } ?>

                      <?php if(!empty($eshkool_option['kids-top-email'])) { ?>
                      <li class="rs-contact-email">
                          <i class="icon-basic-mail-open"></i>                   
                            <a href="mailto:<?php echo esc_attr($eshkool_option['kids-top-email'])?>"><?php echo esc_html($eshkool_option['kids-top-email'])?></a>                   
                      </li>
                      <?php } ?>  

                      <?php if(!empty($eshkool_option['kids-address'])) { ?>
                      <li class="rs-contact-address">
                          <i class="icon-basic-map"></i>                   
                          <?php echo esc_html($eshkool_option['kids-address'])?>                  
                      </li>
                      <?php } ?>             

                    </ul>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-5 col-xs-12 text-right">

                       
                        <?php
                        if(!empty($eshkool_option['kids-show-social'])){     
                             get_template_part('inc/header/header-social');
                         }
                         ?>
                          <?php if (!empty($eshkool_option['kids-login_link'])) { ?>
                         <div class="toolbar-login-btn">
                            <ul>
                            
                                  <li> <a href="<?php  echo esc_url($eshkool_option['kids-login_link']);?> "><i class="fa fa-sign-in"></i> <?php  echo esc_attr($eshkool_option['kids-login_text']);?> </a> </li>
                                            
                            
                            </ul>
                        </div>
                        <?php }    ?>    
                  </div>
                </div>
              </div>
            </div>
        <?php 
        }
    } else{
  ?>
        <div class="toolbar-area">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-sm-7 col-xs-12">
                    <div class="toolbar-contact">
                      <ul>
                        <?php if(!empty($eshkool_option['welcome-text'])) { ?>
                        <li> <?php echo esc_html($eshkool_option['welcome-text']); ?> </li>
                        <?php } ?>                
                      </ul>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-5 col-xs-12">
                        <?php
                          if(!empty($eshkool_option['kids-show-social'])){
                           
                              get_template_part('inc/header/header-social'); 

                          }
                        ?>
                         <?php            
                              if (!empty($eshkool_option['kids-login_link'])) { ?>
                                 <div class="toolbar-login-btn">
                                    <ul class="login">
                                   
                                          <li> <a href="<?php  echo esc_url($eshkool_option['kids-login_link']);?> "><i class="fa fa-sign-in"></i> <?php  echo esc_attr($eshkool_option['kids-login_text']);?> </a> </li>
                                         
                                    </ul>
                                </div>
                         <?php }  ?>
                  </div>
                </div>
              </div>
        </div>
    <?php
    }
} 

