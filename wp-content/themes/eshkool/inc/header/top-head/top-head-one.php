<?php
/* Top Header
*/

global $eshkool_option;
$header_width_meta = get_post_meta(get_the_ID(), 'header_width_custom', true);

if ($header_width_meta != ''){
    $header_width = ( $header_width_meta == 'full' ) ? 'container-fluid': 'container';
}else{
  $header_width = 'container';
}

if(!empty($eshkool_option['show-top'])){ 
    if(is_page()){
        $rs_top_bar = get_post_meta(get_the_ID(), 'select-top', true);
        if($rs_top_bar == 'Show' || $rs_top_bar == ''){
    ?> 
        <div class="toolbar-area">
          <div class="<?php echo esc_attr( $header_width );?>">
            <div class="row">
              <div class="col-lg-6 col-sm-7 col-xs-12">
                <div class="toolbar-contact">
                  <ul>
                    <?php if(!empty($eshkool_option['welcome-text'])) { ?>
                    <li> <?php echo esc_html($eshkool_option['welcome-text']); ?> </li>
                    <?php } ?>                
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-sm-5 col-xs-12">
                  <?php
                    if(!empty($eshkool_option['show-social'])){
                          get_template_part('inc/header/header-social'); 
                      }
                    ?>
                    <?php if (!empty($eshkool_option['login_link'])) { ?>
                      <div class="toolbar-login-btn">
                        <ul class="login">                        
                            <li>
                                <a href="<?php  echo esc_url($eshkool_option['login_link']);?> "><i class="fa fa-sign-in"></i> <?php  echo esc_attr($eshkool_option['login_text']);?> </a>
                            </li>                       
                        </ul>
                    </div>
                   <?php }  ?>
              </div>
            </div>
          </div>
        </div>
    <?php 
    }
 }
    else{
      ?>
      <div class="toolbar-area">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-sm-7 col-xs-12">
                <div class="toolbar-contact">
                  <ul>
                    <?php if(!empty($eshkool_option['welcome-text'])) { ?>
                    <li> <?php echo esc_html($eshkool_option['welcome-text']); ?> </li>
                    <?php } ?>                
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 col-sm-5 col-xs-12">
              
                    <?php
                    if(!empty($eshkool_option['show-social'])){
                          get_template_part('inc/header/header-social'); 
                      }
                    ?>
                    <div class="toolbar-login-btn">
                      <ul class="login">
                      <?php            
                            if (!empty($eshkool_option['login_link'])) { ?>
                            <li> <a href="<?php  echo esc_url($eshkool_option['login_link']);?> "><i class="fa fa-sign-in"></i> <?php  echo esc_attr($eshkool_option['login_text']);?> </a> </li>
                            <?php } 

                       ?>
                      </ul>
                  </div>
              </div>
            </div>
          </div>
        </div>
      <?php
    }
} ?>
