<?php

/*
Header Style 2
*/
global $eshkool_option;
$sticky = $eshkool_option['off_sticky']; 
$sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
?>

<header id="rs-header" class="header-style2">
    <div class="header-inner<?php echo esc_attr($sticky_menu);?>">
       
        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['show-top'])){
                $top_bar = $eshkool_option['show-top'];
                if($top_bar == ""){?>
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-one');
                    ?>
                    </div>
                <?php
                }
                else{
                    if($top_bar =='1'){       
                        get_template_part('inc/header/top-head/top-head-one');
                    } 
                }
            }
        ?>
        <!-- Toolbar End -->

      <!-- Header Menu Start -->  
      <div class="menu-area">
        <div class="container">
          <div class="row menu-middle align-items-center"> 
          <?php if(!empty($eshkool_option['phone']) || !empty($eshkool_option['top-email'])  || !empty($eshkool_option['top-location'])){
            $logo_col = 'col-sm-3';
            }
            else{$logo_col = '';}?>         
            <div class="<?php echo esc_attr($logo_col);?> col-xs-12">
              <?php  get_template_part('inc/header/logo/logo'); ?>
            </div>
            <?php if(!empty($eshkool_option['phone']) || !empty($eshkool_option['top-email']) || !empty($eshkool_option['top-location'])){?>
            <div class="col-sm-9 d-none d-sm-block">
                <div class="toolbar-contact-style4">
                  <ul class="rs-contact-info">

                    <?php if(!empty($eshkool_option['phone'])) { ?>
                    <li class="rs-contact-phone">
                        <i class="icon-basic-tablet"></i>
                        <span class="contact-inf">
                          <span><?php if(!empty($eshkool_option['phone-pretext'])): echo esc_html($eshkool_option['phone-pretext']); endif;?> </span>
                          <a href="tel:+<?php echo esc_attr(str_replace(" ","",($eshkool_option['phone'])))?>"> <?php echo esc_html($eshkool_option['phone']); ?></a> 
                        </span>
                    </li>
                    <?php } ?>

                    <?php if(!empty($eshkool_option['top-email'])) { ?>
                    <li class="rs-contact-email">
                        <i class="icon-basic-mail-open"></i>
                          <span class="contact-inf">
                              <span><?php if(!empty($eshkool_option['email-pretext'])): echo esc_html($eshkool_option['email-pretext']); endif;?> </span>
                              <a href="mailto:<?php echo esc_attr($eshkool_option['top-email'])?>"><?php echo esc_html($eshkool_option['top-email'])?></a> 
                          </span>
                    </li>
                    <?php } ?>

                    <?php if(!empty($eshkool_option['location-pretext'])) { ?>              
                    <li class="rs-contact-location">
                        <!-- <i class="icon-basic-world"></i>
                        <span class="contact-inf">
                          <span><?php if(!empty($eshkool_option['location-pretext'])): echo esc_html($eshkool_option['location-pretext']); endif;?> </span>
                         <?php echo esc_html($eshkool_option['top-location'])?>
                        </span> -->
                        <?php
                  get_template_part('inc/footer_style/footer-social');
                  ?>

                    </li> 
                    

                    <?php
                  } ?>
                    
                  </ul>
              </div>
            </div>
          <?php } ?>
          </div>
          <div class="menu_one">
              <div class="row justify-content-between">

                  <div class="col-md-10 col-xs-10 menu-responsive">  
                    <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                          $canvas_right = 'nav-right-bar';
                        else:
                          $canvas_right = ''; 
                        endif;                    
                        get_template_part('inc/header/menu');               
                        // get_template_part('inc/footer_style/footer-social');
                        ?>
                  </div>            


                 <div class="col-md-2 col-xs-2">
                      <?php get_template_part('inc/header/search'); ?>
                  </div> 

                </div>
          </div>
        </div>    
      </div>
      <!-- Header Menu End --> 
    </div>

    <!-- Slider Start Here -->
    <?php  get_template_part('inc/header/slider/slider'); ?>

    <!-- End Slider area  -->
    <?php 
        if(is_page()) {
          get_template_part( 'inc/page-header/breadcrumbs' );
        }
    ?>
</header>