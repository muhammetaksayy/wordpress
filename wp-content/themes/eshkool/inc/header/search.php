<?php
//search page here
global $eshkool_option;
if(!empty($eshkool_option['off_search'])):
     $sticky_search = $eshkool_option['off_search'];
else:
    $sticky_search ='';
endif;

 if($sticky_search == '1'):
 ?>
<div class="sticky_search"> 
	<a class="hidden-xs rs-search" data-target=".search-modal" data-toggle="modal" href="#"><i class="fa fa-search"></i></a>
</div>

<div class="sticky_form">
  <?php get_search_form(); ?>
</div>
<?php endif; ?>

