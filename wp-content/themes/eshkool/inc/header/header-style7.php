<?php
  /*
  Header style 7
  */
global $eshkool_option;
$sticky = $eshkool_option['off_sticky']; 
$sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
$has_quote = $quote_right = '';

$header_width_meta = get_post_meta(get_the_ID(), 'header_width_custom', true);

if ($header_width_meta != ''){
    $header_width = ( $header_width_meta == 'full' ) ? 'container-fluid': 'container';
}else{
  $header_width = 'container';
}
?>
<header id="rs-header" class="header-transparent style7">
    <div class="header-inner<?php echo esc_attr($sticky_menu);?>">
        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['show-top'])){
                $top_bar = $eshkool_option['show-top'];
                if($top_bar == ""){?>
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-two');
                    ?>
                    </div>
                <?php
                }
                else{
                    if($top_bar =='1'){       
                        get_template_part('inc/header/top-head/top-head-two');
                    } 
                }
            }
        ?>
        <!-- Toolbar End -->
        
        <!-- Header Menu Start -->
        <div class="menu-area">
        <div class="<?php echo esc_attr($header_width);?>">
          <div class="row header-style-6">
            <div class="header-logo">
              <?php  get_template_part('inc/header/logo/logo'); ?>
            </div>
            <?php
                if ( has_nav_menu( 'menu-3' ) ) {
                    // User has assigned menu to this location;
                    // output it
                    ?>
                    <nav class="nav navbar rs-cats-7">
                        <div class="navbar-menu category-menu">
                            <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'menu-3',
                                    'menu_id'        => 'primary-menu-single',
                                ) );
                            ?>
                        </div>                       
                    </nav>
                    <?php
                    }
                ?>
            <div class="sticky_form_style7">
                <?php get_search_form(); ?>
            </div>

            <?php if(!empty($eshkool_option['quote'])){ 
                $has_quote = ' has-quote-text';
            }
            ?>
            <div class="col-menu2 menu-responsive<?php echo esc_attr($has_quote); ?>">   
                <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                        $menu_right='nav-right-bar';
                    else:
                        $menu_right=''; 
                    endif;
                    get_template_part('inc/header/menu');                 
                    
                ?>                
            </div>
            <?php if(!empty($eshkool_option['quote']) || !empty($eshkool_option['off_search'])) { ?>
                <div class="header-6-last">
                  <?php 
                        if(!empty($eshkool_option['quote'])):   
                            $quote_right = $eshkool_option['quote'];                     
                        endif;
                            $rs_quote_menu = ( $quote_right == 1 ) ? 'rs-quote-off' : '';
                        if(!empty($eshkool_option['quote'])) { ?>   
                            <div class="get-quote <?php echo esc_attr($rs_quote_menu);?>">
                                <?php if (!empty($eshkool_option['quote'])) { ?>
                                  <a href="<?php echo esc_url($eshkool_option['quote_link']); ?>" class="quote-button"><?php  echo esc_html($eshkool_option['quote']); ?></a>                            
                            <?php }
                            ?> 
                          </div>
                    <?php } ?>
                </div>
            <?php } ?>
          </div>
        </div>         
        </div>
        <!-- Header Menu End -->
    </div>
     <!-- End Slider area  -->
   <?php 
  if(is_page())
  {
      get_template_part( 'inc/page-header/breadcrumbs' );
  }
  ?>
</header>
<?php  
get_template_part('inc/header/slider/slider'); ?>
