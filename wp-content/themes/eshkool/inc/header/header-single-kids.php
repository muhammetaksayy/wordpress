<?php
  /*
  Onepage Header
  */
  global $eshkool_option;
  $sticky      = $eshkool_option['off_sticky']; 
  $sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
  $has_quote   = $quote_right = '';

?>
<header id="rs-header" class="kids-header">
    <div id="single-head" class="header-inner<?php echo esc_attr($sticky_menu);?>">
        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['kids-show-top'])){?>             
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-kids');
                    ?>
                    </div>
               <?php
            }
        ?>
        <!-- Toolbar End -->
        
        <!-- Header Menu Start -->
        <div class="menu-area" id="single-menu">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 header-logo">
              <?php  get_template_part('inc/header/logo/logo'); ?>
            </div>

            <?php if(!empty($eshkool_option['quote'])){ 
                $has_quote = ' has-quote-text';
            }
            ?>
            <div class="col-sm-9 menu-responsive<?php echo esc_attr($has_quote); ?>">   
                <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                        $menu_right='nav-right-bar';
                    else:
                        $menu_right=''; 
                    endif;

                    get_template_part('inc/header/menu-single');

                    get_template_part('inc/header/search');
                    
                ?>
                <?php 
                    if(!empty($eshkool_option['kids-quote'])):   
                      $quote_right = $eshkool_option['kids-quote'];                     
                    endif;
                    $rs_quote_menu = ( $quote_right == 1 ) ? 'rs-quote-off' : '';

                    if(!empty($eshkool_option['kids-quote'])) { ?>   
                      <div class="get-quote <?php echo esc_attr($rs_quote_menu);?>">
                      <?php if (!empty($eshkool_option['kids-quote'])) { ?>
                      <a href="<?php echo esc_url($eshkool_option['kids-quote_link']); ?>" class="quote-button"><?php  echo esc_html($eshkool_option['kids-quote']); ?></a>                            
                      <?php }
                       ?> 
                      </div>
                  <?php } ?>
            </div>
          </div>
        </div>
         
        </div>
        <!-- Header Menu End -->
    </div>
     <!-- End Slider area  -->
   <?php 
  if(is_page())
  {
      get_template_part( 'inc/page-header/breadcrumbs' );
  }
  ?>
</header>
<?php  
  get_template_part('inc/header/slider/slider'); ?>
