<?php if(!empty($eshkool_option['show_preloader']))
	{
		$loading = $eshkool_option['show_preloader'];
        if(!empty($eshkool_option['preloader_img'])){
            $preloader_img = $eshkool_option['preloader_img'];
        }
		if($loading == 1){
            if(empty($preloader_img['url'])):	  ?>  
                <!--Preloader area start here-->
                <div class="book_preload">
                    <div class="book">
                        <div class="book__page"></div>
                        <div class="book__page"></div>
                        <div class="book__page"></div>
                    </div>
                </div>
            <?php else: ?>
                <div class="book_preload">
                    <img src="<?php echo esc_url($preloader_img['url']);?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                </div>
                <!--Preloader area end here-->
            <?php endif;    
 	 } 
}?>

 <?php if(!empty($eshkool_option['off_sticky'])):   
        $sticky = $eshkool_option['off_sticky'];         
        if($sticky == 1):
         $sticky_menu ='menu-sticky';        
        endif;
       else:
       $sticky_menu ='';
      endif;


if( is_page() ){
 $post_meta_header = get_post_meta($post->ID, 'trans_header', true);  

     if($post_meta_header == 'Default Header'){       
        $header_style = 'default_header';             
     }
     else{
        $header_style = 'transparent_header';
    }
 }
 else{
    $header_style = 'transparent_header';
 }

 ?>   