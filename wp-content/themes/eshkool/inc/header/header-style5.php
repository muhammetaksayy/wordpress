<?php
    /*
    Header style 5
    */
    global $eshkool_option;
    $sticky = $eshkool_option['off_sticky']; 
    $sticky_menu = ($sticky == 1) ? ' menu-sticky' : '';
    $has_quote = $quote_right = '';

?>

<header id="rs-header" class="header-styl-3 header3_transparent_style">
    <div class="header-inner<?php echo esc_attr($sticky_menu);?>">
        <!-- Toolbar Start -->
        <?php
            if(!empty($eshkool_option['show-top'])){
                $top_bar = $eshkool_option['show-top'];
                if($top_bar == ""){?>
                    <div class="top-gap">
                    <?php get_template_part('inc/header/top-head/top-head-two');
                    ?>
                    </div>
                <?php
                }
                else{
                    if($top_bar =='1'){       
                        get_template_part('inc/header/top-head/top-head-two');
                    } 
                }
            }
        ?>
        <!-- Toolbar End -->
        
        <!-- Header Menu Start -->
        <div class="menu-area">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-3 header-logo">
              <?php  get_template_part('inc/header/logo/logo'); ?>
            </div>

            <?php if(!empty($eshkool_option['quote'])){ 
                $has_quote = ' has-quote-text';
            }
            ?>
            <div class="col-sm-9 menu-responsive<?php echo esc_attr($has_quote); ?>">  
                <div class="rs-off-menu-wrap">
                    <?php if(!empty($eshkool_option['off_canvas']) || !empty($eshkool_option['off_search'])):
                            $menu_right='nav-right-bar';
                        else:
                            $menu_right=''; 
                        endif;

                        get_template_part('inc/header/menu');

                        get_template_part('inc/header/search');
                        
                    ?>                    
                </div> 

                <!-- Toggle Menu -->
                <div class="toggle-btn text-right">
                    <span class="border-icon"></span>
                    <span class="border-icon"></span>
                    <span class="border-icon"></span>
                </div>

            </div>
          </div>
        </div>
         
        </div>
        <!-- Header Menu End -->
    </div>
     <!-- End Slider area  -->
   <?php 
  if(is_page())
  {
      get_template_part( 'inc/page-header/breadcrumbs' );
  }
  ?>
</header>

<?php get_template_part('inc/header/slider'); ?>
