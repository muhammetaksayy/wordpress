<div class="rs-breadcrumbs  porfolio-details">
  <?php
  global $eshkool_option;
  if(!empty($eshkool_option['blog_banner_main']['url'])) { ?>
  <div class="breadcrumbs-single" style="background-image: url('<?php echo esc_url($eshkool_option['blog_banner_main']['url']);?>')">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="breadcrumbs-inner">
               <h1 class="page-title"><?php
                        if(!empty($eshkool_option['title_404'])){
                          echo esc_html($eshkool_option['title_404']);
                        }
                        else{
                        esc_html_e( '404.', 'eshkool' ); }
                        ?></h1>
             <?php if(function_exists('bcn_display')) {
                bcn_display();
              }
            ?>   
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php }
  else{   
  ?>
  <div class="rs-breadcrumbs-inner">  
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <div class="breadcrumbs-inner">
            <h1 class="page-title"><?php
                     if(!empty($eshkool_option['title_404'])){
                       echo esc_html($eshkool_option['title_404']);
                     }
                     else{
                     esc_html_e( '404.', 'eshkool' ); }
                     ?></h1>
          <?php if(function_exists('bcn_display')) {
             bcn_display();
           }
         ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  }
?>  
</div>