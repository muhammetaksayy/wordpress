<div class="rs-breadcrumbs  porfolio-details">
  <?php
   global $eshkool_option;    
   if(!empty($eshkool_option['blog_banner']['url'])){
	  $blog_single = $eshkool_option['blog_banner']['url'];  
?>
<div class="breadcrumbs-single" style="background-image: url('<?php echo esc_url( $blog_single );?>')">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="breadcrumbs-inner"> 
            <ul>
                <?php if(get_the_category()){?>
                    <li class="category-name">
                      <?php the_category(', '); ?>
                    </li>
                <?php }?>
            </ul>
        <?php if(get_the_title()):?>         
          <h1 class="page-title"><?php the_title();?></h1>
         <?php endif; ?>          
          <div class="meta-info-wrap meta-info-wrap2">
             <ul class="bs-general-meta"> 
                    <li class="bs-date"><i class="fa fa-calendar-o"></i><span>
                        <?php $post_date = get_the_date(); echo esc_attr($post_date);?>
                    </span></li>                     
                    <li><i class="fa fa-user-o"></i>
                        <?php 
                            while ( have_posts() ) : the_post();                     
                                the_author();
                            endwhile; 
                        ?>
                    </li>
                    <li><i class="fa fa-commenting-o"></i>
                        <span>
                        <?php $post_comment = get_comments_number(); echo esc_attr($post_comment);?>
                        </span>
                    </li>
              </ul>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php 
   }
   else{    
  ?>
  <div class="rs-breadcrumbs-inner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <div class="breadcrumbs-inner">
            <ul>
                <?php if(get_the_category()){?>
                    <li class="category-name">
                      <?php the_category(', '); ?>
                    </li>
                <?php }?>
            </ul>
            <?php if(get_the_title()):?>         
                <h1 class="page-title"><?php the_title();?></h1>
            <?php endif; ?>
            <div class="meta-info-wrap meta-info-wrap2">
              <ul class="bs-general-meta"> 
                    <li class="bs-date"><i class="fa fa-calendar-o"></i><span>
                        <?php $post_date = get_the_date(); echo esc_attr($post_date);?>
                    </span></li>                     
                    <li><i class="fa fa-user-o"></i>
                        <?php 
                            while ( have_posts() ) : the_post();                     
                                the_author();
                            endwhile; 
                        ?>
                    </li>
                    <li><i class="fa fa-commenting-o"></i>
                        <span>
                        <?php $post_comment = get_comments_number(); echo esc_attr($post_comment);?>
                        </span>
                    </li>
              </ul>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
    }
   ?>
</div>