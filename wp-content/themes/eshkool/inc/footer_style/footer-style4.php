<?php
    /*
        footer style 1
    */
    if(is_active_sidebar('footer1')):
        $top_gap='';
    else:
        $top_gap='top_gap';
    endif;

    $footer_bg_image = '';
    global $eshkool_option;

    $has_footer_contact = ' has-footer-contact';
    $disbale_footer_top = get_post_meta(get_the_ID(), 'disbale_footer_top', true);
    if (   is_active_sidebar( 'footer_top_1'  ) && $disbale_footer_top == 'yes'  || is_active_sidebar( 'footer_top_2' ) && $disbale_footer_top == 'yes'  || is_active_sidebar( 'footer_top_3') && $disbale_footer_top == 'yes' ) {
        $has_footer_contact = ' has-footer-contact';
    }

    $footer_bg = get_post_meta(get_the_ID(),'banner_image_footer', true);
    $footer_bg = ($footer_bg) ? $footer_bg : '';    

    $footer_select = get_post_meta(get_the_ID(),'footer_select', true);

    $footer_select = ($footer_select) ? $footer_select : '';  

    if(!empty( $footer_bg)):?>
        <footer id="rs-footer" class="<?php echo esc_attr($footer_select);?> rs-footer footer-style-1 <?php echo esc_attr($top_gap.$has_footer_contact);?>" style="background-image: url('<?php echo esc_url($footer_bg);?>')">
    
    <?php elseif( !empty( $eshkool_option['footer_bg_image']['url'])):?>
        <footer id="rs-footer" class="<?php echo esc_attr($footer_select);?> rs-footer footer-style-1 <?php echo esc_attr($top_gap.$has_footer_contact);?>" style="background-image: url('<?php echo esc_url($eshkool_option['footer_bg_image']['url']);?>')">
        <?php else:?>

            <footer id="rs-footer" class="<?php echo esc_attr($footer_select);?> rs-footer footer-style-1 <?php echo esc_attr($top_gap.$has_footer_contact);?>" >
    <?php endif; 

    if($disbale_footer_top == 'yes' || $disbale_footer_top == '') :
         get_template_part('inc/footer_style/footer-contact'); 
    endif;
    
    get_template_part('inc/footer_style/footer-top'); ?>

    <div class="footer-bottom">
        <div class="container">
            <div class="copyright">
                <?php get_template_part('inc/footer_style/copyright'); ?>
            </div>
        </div>
    </div>
</footer><!-- end footer -->