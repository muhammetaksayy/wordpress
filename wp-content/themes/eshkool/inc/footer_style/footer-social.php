<?php
/*
      Footer Social Links
*/
global $eshkool_option;
?>
<?php 
      if(!empty($eshkool_option['show-social2'])){
            $footer_social = $eshkool_option['show-social2'];
            if($footer_social == 1){?>
                  <div class="footer-share">
                        <ul>  
                              <?php
                               if(!empty($eshkool_option['facebook'])) { ?>
                               <li> 
                                    <a href="<?php echo esc_url($eshkool_option['facebook'])?>" target="_blank"><i class="fa fa-facebook"></i></a> 
                               </li>
                              <?php } ?>
                              <?php if(!empty($eshkool_option['twitter'])) { ?>
                              <li> 
                                    <a href="<?php echo esc_url($eshkool_option['twitter']);?> " target="_blank"><i class="fa fa-twitter"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if(!empty($eshkool_option['rss'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['rss']);?> " target="_blank"><i class="fa fa-rss"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['pinterest'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['pinterest']);?> " target="_blank"><i class="fa fa-pinterest-p"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['linkedin'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['linkedin']);?> " target="_blank"><i class="fa fa-linkedin"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['google'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['google']);?> " target="_blank"><i class="fa fa-google-plus-square"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['instagram'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['instagram']);?> " target="_blank"><i class="fa fa-instagram"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if(!empty($eshkool_option['vimeo'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['vimeo'])?> " target="_blank"><i class="fa fa-vimeo"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['tumblr'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['tumblr'])?> " target="_blank"><i class="fa fa-tumblr"></i></a> 
                              </li>
                              <?php } ?>
                              <?php if (!empty($eshkool_option['youtube'])) { ?>
                              <li> 
                                    <a href="<?php  echo esc_url($eshkool_option['youtube'])?> " target="_blank"><i class="fa fa-youtube"></i></a> 
                              </li>
                              <?php } ?>     
                        </ul>
                  </div>
       <?php } 
}?>
