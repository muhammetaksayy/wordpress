<?php
/*
Eshkool inlcude footer
*/
global $eshkool_option;

$footer_style = 'style1';

if(!empty($eshkool_option['footer_layout'])){
	$footer_style = $eshkool_option['footer_layout'];
}

if($footer_style == 'style1'){		 
	get_template_part('inc/footer_style/footer-style1');			
}
if($footer_style == 'style2'){		 
	get_template_part('inc/footer_style/footer-style2');			
}
if($footer_style == 'style3'){		 
	get_template_part('inc/footer_style/footer-style3');			
}
if($footer_style == 'style4'){		 
	get_template_part('inc/footer_style/footer-style4');			
}
if($footer_style == 'style5'){		 
	get_template_part('inc/footer_style/footer-style5');			
}