<?php
    /* The footer widget area is triggered if any of the areas
     * have widgets. So let's check that first.
     *
     * If none of the sidebars have widgets, then let's bail early.
     */
    if (   ! is_active_sidebar( 'footer_top_1'  )
        && ! is_active_sidebar( 'footer_top_2' )
        && ! is_active_sidebar( 'footer_top_3'  )
    ){
      
    } 

$footer_top_color = get_post_meta(get_the_ID(),'footer_top_bg', true);
$footer_top_color = ($footer_top_color) ? ' style="background-color: '.$footer_top_color.'"' : '';    
?>

<?php
if(is_active_sidebar('footer_top_1') && is_active_sidebar('footer_top_2') && is_active_sidebar('footer_top_3'))
  {?>
    <div class="container contact-container">
        <div class="footer-contact-desc" <?php echo wp_kses_post($footer_top_color); ?>>
            <div class="row">      
                <div class="col-md-4">
                    <?php dynamic_sidebar('footer_top_1'); ?>                            
                </div>
                <div class="col-md-4">
                    <?php dynamic_sidebar('footer_top_2'); ?>   
                </div>
                <div class="col-md-4">
                    <?php dynamic_sidebar('footer_top_3'); ?>
                </div>
            </div>
        </div>
    </div>
  <?php }
 elseif(is_active_sidebar('footer_top_1') && is_active_sidebar('footer_top_2'))
  {?>
    <div class="footer-contact-desc" <?php echo wp_kses_post($footer_top_color); ?>>
        <div class="container">
            <div class="row">      
                <div class="col-md-6">
                    <?php dynamic_sidebar('footer_top_1'); ?>                            
                </div>
                <div class="col-md-6">
                    <?php dynamic_sidebar('footer_top_2'); ?>   
                </div>
            </div>
        </div>
    </div>
<?php } 

elseif(is_active_sidebar('footer_top_1')) {
?>
<div class="footer-contact-desc" <?php echo wp_kses_post($footer_top_color); ?>>
    <div class="container">
        <div class="row">      
            <div class="col-md-12">
                <?php dynamic_sidebar('footer_top_1'); ?>                            
            </div>
        </div>
    </div>
</div>
<?php } ?>    

