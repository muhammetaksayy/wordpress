<?php
/*
dynamic css file. please don't edit it. it's update automatically when settins changed
*/
 add_action('wp_head', 'eshkool_custom_colors', 160);
 function eshkool_custom_colors() { 
   global $eshkool_option;

	if(!empty($eshkool_option['body_bg_color']))
	{
	$body_bg 	  	  = $eshkool_option['body_bg_color'];
	}	
	$body_color       = $eshkool_option['body_text_color'];	
	$site_color       = $eshkool_option['primary_color'];	
	$hover_color      = $eshkool_option['secondary_color'];
	$link_color       = $eshkool_option['link_text_color'];	
	$link_hover_color = $eshkool_option['link_hover_text_color'];	
	$footer_bgcolor   = $eshkool_option['footer_bg_color'];
	$trans_menu_color = $eshkool_option['transparent_menu_text_color'];
	$mobile_drop_text_border_color = $eshkool_option['mobile_drop_text_border_color'];
	
	if(!empty($eshkool_option['footer_bg']['url'])):
		$footer_top   	  = $eshkool_option['footer_bg']['url'];
	endif;

	if(!empty($eshkool_option['menu_text_color'])){		
		$menu_text_color = $eshkool_option['menu_text_color'];
	}
	if(!empty($eshkool_option['menu_text_hover_color'])){		
		$menu_text_hover_color = $eshkool_option['menu_text_hover_color'];
	}
	if(!empty($eshkool_option['menu_text_active_color'])){		
		$menu_active_color = $eshkool_option['menu_text_active_color'];
	}
	if(!empty($eshkool_option['drop_down_bg_color'])){		
		$drop_down_bg = $eshkool_option['drop_down_bg_color'];	
	}
	if(!empty($eshkool_option['drop_text_color'])){		
		$dropdown_text_color = $eshkool_option['drop_text_color'];
	}	
	if(!empty($eshkool_option['drop_text_hover_color'])){		
		$drop_text_hover_color = $eshkool_option['drop_text_hover_color'];
	}		
	
	if(!empty($eshkool_option['drop_text_hoverbg_color'])){		
		$drop_text_hoverbg_color = $eshkool_option['drop_text_hoverbg_color'];
	}
	
	//typography extract for body
	
	if(!empty($eshkool_option['opt-typography-body']['color']))
	{
		$body_typography_color=$eshkool_option['opt-typography-body']['color'];
	}
	if(!empty($eshkool_option['opt-typography-body']['line-height']))
	{
		$body_typography_lineheight=$eshkool_option['opt-typography-body']['line-height'];
	}
		
	$body_typography_font=$eshkool_option['opt-typography-body']['font-family'];

	$body_typography_font_size=$eshkool_option['opt-typography-body']['font-size'];
	
	
	//typography extract for menu
	$menu_typography_color=$eshkool_option['opt-typography-menu']['color'];	
	$menu_typography_weight=$eshkool_option['opt-typography-menu']['font-weight'];	
	$menu_typography_font_family=$eshkool_option['opt-typography-menu']['font-family'];
	$menu_typography_font_fsize=$eshkool_option['opt-typography-menu']['font-size'];	
	if(!empty($eshkool_option['opt-typography-menu']['line-height']))
	{
		$menu_typography_line_height=$eshkool_option['opt-typography-menu']['line-height'];
	}
	
	//typography extract for heading
	
	$h1_typography_color=$eshkool_option['opt-typography-h1']['color'];		
	if(!empty($eshkool_option['opt-typography-h1']['font-weight']))
	{
		$h1_typography_weight=$eshkool_option['opt-typography-h1']['font-weight'];
	}
		
	$h1_typography_font_family=$eshkool_option['opt-typography-h1']['font-family'];
	$h1_typography_font_fsize=$eshkool_option['opt-typography-h1']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h1']['line-height']))
	{
		$h1_typography_line_height=$eshkool_option['opt-typography-h1']['line-height'];
	}
	
	$h2_typography_color=$eshkool_option['opt-typography-h2']['color'];	

	$h2_typography_font_fsize=$eshkool_option['opt-typography-h2']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h2']['font-weight']))
	{
		$h2_typography_font_weight=$eshkool_option['opt-typography-h2']['font-weight'];
	}	
	$h2_typography_font_family=$eshkool_option['opt-typography-h2']['font-family'];
	$h2_typography_font_fsize=$eshkool_option['opt-typography-h2']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h2']['line-height']))
	{
		$h2_typography_line_height=$eshkool_option['opt-typography-h2']['line-height'];
	}
	
	$h3_typography_color=$eshkool_option['opt-typography-h3']['color'];	
	if(!empty($eshkool_option['opt-typography-h3']['font-weight']))
	{
		$h3_typography_font_weightt=$eshkool_option['opt-typography-h3']['font-weight'];
	}	
	$h3_typography_font_family=$eshkool_option['opt-typography-h3']['font-family'];
	$h3_typography_font_fsize=$eshkool_option['opt-typography-h3']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h3']['line-height']))
	{
		$h3_typography_line_height=$eshkool_option['opt-typography-h3']['line-height'];
	}

	$h4_typography_color=$eshkool_option['opt-typography-h4']['color'];	
	if(!empty($eshkool_option['opt-typography-h4']['font-weight']))
	{
		$h4_typography_font_weight=$eshkool_option['opt-typography-h4']['font-weight'];
	}	
	$h4_typography_font_family=$eshkool_option['opt-typography-h4']['font-family'];
	$h4_typography_font_fsize=$eshkool_option['opt-typography-h4']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h4']['line-height']))
	{
		$h4_typography_line_height=$eshkool_option['opt-typography-h4']['line-height'];
	}
	
	$h5_typography_color=$eshkool_option['opt-typography-h5']['color'];	
	if(!empty($eshkool_option['opt-typography-h5']['font-weight']))
	{
		$h5_typography_font_weight=$eshkool_option['opt-typography-h5']['font-weight'];
	}	
	$h5_typography_font_family=$eshkool_option['opt-typography-h5']['font-family'];
	$h5_typography_font_fsize=$eshkool_option['opt-typography-h5']['font-size'];	
	if(!empty($eshkool_option['opt-typography-h5']['line-height']))
	{
		$h5_typography_line_height=$eshkool_option['opt-typography-h5']['line-height'];
	}
	
	$h6_typography_color=$eshkool_option['opt-typography-6']['color'];	
	if(!empty($eshkool_option['opt-typography-6']['font-weight']))
	{
		$h6_typography_font_weight=$eshkool_option['opt-typography-6']['font-weight'];
	}
	$h6_typography_font_family=$eshkool_option['opt-typography-6']['font-family'];
	$h6_typography_font_fsize=$eshkool_option['opt-typography-6']['font-size'];	
	if(!empty($eshkool_option['opt-typography-6']['line-height']))
	{
		$h6_typography_line_height=$eshkool_option['opt-typography-6']['line-height'];
	}	
?>

<!-- Typography -->
<?php if(!empty($body_color)){
	global $eshkool_option;
	$mobile_menu_text_color = $eshkool_option['mobile_menu_text_color'];
	$hex_color = $mobile_menu_text_color;
	list($r, $g, $b) = sscanf($hex_color, "#%02x%02x%02x");
	$border_color_icon  = "$r, $g, $b";

	
	$hex = $mobile_drop_text_border_color;
	list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
	$border_color_mobile  = "$r, $g, $b";

	$site_color_op = $site_color;
	list($r, $g, $b) = sscanf($site_color_op, "#%02x%02x%02x");
	$site_color_rgb  = "$r, $g, $b";

	?>
<style>

body{
	background:<?php echo esc_attr($body_bg); ?> !important; 
	color:<?php echo esc_attr($body_color); ?> !important;
	font-family: <?php echo esc_attr($body_typography_font);?> !important;    
    font-size: <?php echo esc_attr($body_typography_font_size);?> !important; 	
}
.navbar a, .navbar li{	
	font-family:<?php echo esc_attr($menu_typography_font_family);?> !important;
	font-size:<?php echo esc_attr($menu_typography_font_fsize);?> !important;
}
.menu-area .navbar ul li > a{
	color: <?php echo esc_attr($trans_menu_color); ?> !important; 
}
.menu-area:not(.sticky) .navbar ul li.active a,
.page-template-page-single .menu-area:not(.sticky) .navbar ul li.active a,
#rs-header.style1 .menu-area .navbar ul li.current_page_parent > a{
	color: <?php echo esc_attr($menu_active_color); ?> !important; 
}
.menu-area:not(.sticky) .navbar ul li > a:hover,
#rs-header.header-styl-3 .menu-area .navbar ul li a:hover{
	color: <?php echo esc_attr($menu_text_hover_color); ?> !important; 
}
.menu-area .navbar ul li ul.sub-menu{
	background:<?php echo esc_attr($drop_down_bg);?> !important; 
}
#rs-header .menu-area .navbar ul li .sub-menu li a, 
#rs-header .menu-area .navbar ul li .children li a {
	color:<?php echo esc_attr($dropdown_text_color);?> !important; 
}
#rs-header .menu-area .navbar ul ul li a:hover ,
#rs-header .menu-area .navbar ul ul li.current-menu-item a{
	color:<?php echo esc_attr($drop_text_hover_color);?> !important;
}
#rs-header .menu-area .navbar ul ul li a:hover, 
 #rs-header .menu-area .navbar ul ul li.current-menu-item a
,#rs-header.style1 .menu-area .navbar ul ul li.current-menu-item a{
<?php if(!empty($drop_text_hoverbg_color))
{?>
  background:<?php echo esc_attr($drop_text_hoverbg_color); ?> !important; 
<?php }?>	
}


#rs-header .menu-area .navbar ul li .sub-menu li{
  <?php if(!empty($eshkool_option['drop_text_hoverbg_color'])){		
		?>
		border-color:<?php echo esc_attr($dropdown_border_color); ?> !important; 
		<?php
	}	
?>
}

h1{
	color:<?php echo esc_attr($h1_typography_color);?>
	font-family:<?php echo esc_attr($h1_typography_font_family);?> ;
	font-size:<?php echo esc_attr($h1_typography_font_fsize);?> ;
	<?php if(!empty($h1_typography_weight)){
	?>
	font-weight:<?php echo esc_attr($h1_typography_weight);?>;
	<?php }?>
	
	<?php if(!empty($h1_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h1_typography_line_height);?>;
	<?php }?>
	
}
h2{
	color:<?php echo esc_attr($h2_typography_color);?>
	font-family:<?php echo esc_attr($h2_typography_font_family);?>;
	font-size:<?php echo esc_attr($h2_typography_font_fsize);?> ;
	<?php if(!empty($h2_typography_font_weight)){
	?>
	font-weight:<?php echo esc_attr($h2_typography_font_weight);?>;
	<?php }?>
	
	<?php if(!empty($h2_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h2_typography_line_height);?>;
	<?php }?>
}
h3{
	color:<?php echo esc_attr($h3_typography_color);?>;
	font-family:<?php echo esc_attr($h3_typography_font_family);?> ;
	font-size:<?php echo esc_attr($h3_typography_font_fsize);?> ;
	<?php if(!empty($h3_typography_font_weight)){
	?>
	font-weight:<?php echo esc_attr($h3_typography_font_weight);?>;
	<?php }?>
	
	<?php if(!empty($h3_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h3_typography_line_height);?> ;
	<?php }?>
}
h4{
	color:<?php echo esc_attr($h4_typography_color);?> 
	font-family:<?php echo esc_attr($h4_typography_font_family);?> ;
	font-size:<?php echo esc_attr($h4_typography_font_fsize);?> ;
	<?php if(!empty($h4_typography_font_weight)){
	?>
	font-weight:<?php echo esc_attr($h4_typography_font_weight);?>;
	<?php }?>
	
	<?php if(!empty($h4_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h4_typography_line_height);?> ;
	<?php }?>
	
}
h5{
	color:<?php echo esc_attr($h5_typography_color);?> 
	font-family:<?php echo esc_attr($h5_typography_font_family);?> ;
	font-size:<?php echo esc_attr($h5_typography_font_fsize);?> ;
	<?php if(!empty($h5_typography_font_weight)){
	?>
	font-weight:<?php echo esc_attr($h5_typography_font_weight);?> ;
	<?php }?>
	
	<?php if(!empty($h5_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h5_typography_line_height);?> ;
	<?php }?>
}
h6{
	color:<?php echo esc_attr($h6_typography_color);?> 
	font-family:<?php echo esc_attr($h6_typography_font_family);?> ;
	font-size:<?php echo esc_attr($h6_typography_font_fsize);?> ;
	<?php if(!empty($h6_typography_font_weight)){
	?>
	font-weight:<?php echo esc_attr($h6_typography_font_weight);?> ;
	<?php }?>
	
	<?php if(!empty($h6_typography_line_height)){
	?>
		line-height:<?php echo esc_attr($h6_typography_line_height);?> ;
	<?php }?>
}
#rs-header.style1 .menu-area .navbar ul li a{
	color: <?php echo esc_attr($menu_text_color);?> !important;
}

.menu-area .get-quote,
.menu-area .menu-offcanvas .get-quote .nav-link-container a.nav-menu-link,
.readon,
.services-style-2 .services-desc .btn-more,
.rs-portfolio .portfolio-item .p-zoom:hover,
#rs-testimonial .slick-dots button,
.owl-dots .owl-dot span,
#rs-footer .footer-top h3.footer-title:after,
#rs-footer .footer-top .recent-post-widget .post-item .post-date,
#rs-footer .footer-top .mc4wp-form-fields input[type="submit"],
#scrollUp i,
.sidenav .nav-close-menu-li button:hover:after, .sidenav .nav-close-menu-li button:hover:before,
#cl-testimonial .slider4 .slick-active button,
.team-slider-style2 .team-item-wrap .team-content .display-table .display-table-cell .team-title:after,
.team-slider-style2 .team-item-wrap .team-img .normal-text .team-name,
.team-slider-style2 .team-item-wrap .team-content .display-table .display-table-cell .team-social .social-icon:hover,
#cta-sec, 
.owl-carousel .owl-nav [class*="owl-"]:hover, 
#about-sec2 a.mt-20,
.rs-about3 .vc_tta-panel.vc_active .vc_tta-panel-heading a i,
.rs-about3 .vc_tta-panel-heading,
.rs-about3 .vc_tta-panel-heading:hover a i,
.services-tabs .vc_tta-tab.vc_active > a, .services-tabs .vc_tta-tab > a:hover,
#cleaning-sec-contact,
.readon-sm,
.contact-form-area input[type="submit"],
.widget_brochures a:hover,
.inquiry-btn .vc_btn3,
.team-gird .team-style2 .team-content .display-table .display-table-cell .team-title:after,
.team-gird .team-style2 .team-content .display-table .display-table-cell .team-social .social-icon:hover,
.team-gird .team-style2 .team-img .normal-text .team-name,
.team-gird .team-style1 .team-item .team-content,
.team-gird .team-style3 .team-wrapper .team_desc:before,
.team-gird .team-style4 .team-content .team-social a:hover,
.comment-respond .form-submit #submit,
#loading .object,
.services-style-3:after,
.rs-blog-details .author-block,
#rs-testimonial .slider2 .testimonial-content:hover .cl-author-info,
.rs-heading.border-style h2:after,
code,mark, ins,
.rs-course .cource-item .cource-img .course-price,
.rs-calltoaction .cta-button,
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce .wc-forward, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
.rs-events .event-item .event-btn a,
.latest-news-slider .slick-arrow:hover,
.rs-latest-news .news-normal-block .news-btn a,
.rs-video-2 .popup-videos:before,
.rs-video-2 .popup-videos:after,
#rs-footer .footer-share ul li a:hover,
#rs-header.header-style2 .sticky_search .rs-search,
button, html input[type="button"], input[type="reset"], input[type="submit"],
.rs-services-style3 .services-item i,
.rs-courses-2 .cource-item .cource-btn,
.rs-team-style1 .team-item .team-social .social-icon,
.rs-team-style1 .team-item .team-title:after,
.rs-courses-categories .courses-item i,
.rs-gallery .gallery-desc,
#rs-header.header-style-4 .sidebarmenu-area .sticky_search a,
#rs-header.header-styl-3 .menu-area .toggle-btn.active .border-icon,
#rs-header.header-styl-3 .menu-area .toggle-btn:hover .border-icon,
#rs-header.header-style-4 .sidebarmenu-area .nav-link-container a:hover,
.rs-heading.style1:after,
#cl-testimonial .testimonial-slide2.slider2 .slick-dots li.slick-active button,
.latest-news-nav .slick-slide.slick-current, .latest-news-nav .slick-slide:hover,
.rs-heading.style2 h2:after,
.rs-branches .wpb_column .rs-services:before,
.rs-team-style3 .team-item .team-title:after,
.rs-courses-3 .course-item .course-img .course-price,
.rs-courses-details .detail-img .course-seats,
.course-summary .course-tabs .learn-press-nav-tabs .course-nav.active:before,
.course-summary .course-tab-panel-overview .course-description h4.desc-title:before, .course-summary .course-tab-panel-curriculum .course-description h4.desc-title:before, .course-summary .course-tab-panel-instructor .course-description h4.desc-title:before, .course-summary .course-tab-panel-reviews .course-description h4.desc-title:before,
.lp-single-course .author-info ul li a,
.rs-events-2 .event-item .event-btn a,
body.single-events .course-features-info .book-btn a,
.rs-blog .blog-item .full-blog-content .blog-button a,
.comments-area .comment-list li.comment .reply a,
#content .about-tabs .vc_tta-tabs-container li.vc_active a::before,
.rs-blog-details .meta-info-wrap .tag-line a:hover,
.bs-sidebar .tagcloud a:hover,
body .register_form_2 .form-title:after,
#cl-testimonial .testimonial-slide4.slider9 .slick-dots li.slick-active button,
.rs-courses-categories2 .courses-item i,
.rs-courses-4 .cource-item .cource-img .course-value .course-price,
body.search article .blog-button a

{
	background:<?php echo esc_attr($site_color);?>; 
}
.rs-services4 .service-item .service-button a,
.rs-services4 .service-item .service-content:before,
.rs-services4 .service-item .service-img:after,
.rs-team-style5 .team-item .team-content,
.kindergarten-about .vc_tta-container .vc_tta-tabs-container ul li.vc_active a,
.rs-team-style3 .team-item .team-social .social-icon:hover,
#cl-testimonial .slick-active button{
	background:<?php echo esc_attr($site_color);?> !important;
}
#cl-testimonial ul.slick-dots li button {
    border: 1px solid <?php echo esc_attr($site_color);?> !important;
}
.kindergarten-about .vc_tta-container .vc_tta-tabs-container ul li.vc_active a:after{
	 border-top: 15px solid <?php echo esc_attr($site_color);?> !important;
}
.rs-course.rs-course-style2 .cource-item .course-body .course-title a:hover{
	color: <?php echo esc_attr($site_color);?> !important;
}
#content .about-tabs .vc_tta-tabs-container li.vc_active a::after{
	border-bottom: 10px solid <?php echo esc_attr($site_color);?> !important;
}

@-webkit-keyframes flip {
  20% {
    background: <?php echo esc_attr($site_color);?> !important;
  }
  29.9% {
    background: <?php echo esc_attr($site_color);?> !important;
  }
  30% {
    background:<?php echo esc_attr($hover_color); ?> !important; 
  }
  60% {
    background:<?php echo esc_attr($hover_color); ?> !important; 
  }
  100% {
    background:<?php echo esc_attr($hover_color); ?> !important; 
  }
}

@keyframes flip {
  20% {
    background: <?php echo esc_attr($site_color);?> !important;
  }
  29.9% {
    background: <?php echo esc_attr($site_color);?> !important;
  }
  30% {
    background:<?php echo esc_attr($hover_color); ?> !important;
  }
  60% {
    background:<?php echo esc_attr($hover_color); ?> !important; 
  }
  100% {
    background:<?php echo esc_attr($hover_color); ?> !important;
  }
}

.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce .wc-forward, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
#rs-pricing .price-table.style1 .cl-pricetable-wrap .bottom .btn-table,
#rs-pricing .price-table.style1 .cl-pricetable-wrap.featured .bottom .btn-table,
.readon.black:hover,
.rs-accordion .vc_tta-panel.vc_active .vc_tta-panel-heading,
.rs-accordion .vc_tta-panel-heading:hover,
.course-curriculum ul.curriculum-sections .section-content .course-item .course-item-meta .duration, .course-curriculum ul.curriculum-sections .section-content .course-item.item-preview .course-item-status,
.readon.white:hover,
.lp-label.label-enrolled, .lp-label.label-started, .course-rate .course-rate .review-bar .rating,
.skew_bg::before,
#rs-home-video.rs-acheivements::after,
#skew-style .slotholder::before,
#rs-skills .vc_progress_bar .vc_single_bar .vc_bar,
.bs-sidebar .widget-title::after,
.breadcrumbs-inner .category-name::after,
body.learnpress .profile-tabs ul.nav-tabs li.active:before
{
	background-color:<?php echo esc_attr($site_color);?> !important;
}

#rs-header .toolbar-area .toolbar-contact ul li i,
.course-summary .course-tab-panel-overview .course-description ul.requirements-list li:before, .course-summary .course-tab-panel-curriculum .course-description ul.requirements-list li:before, .course-summary .course-tab-panel-instructor .course-description ul.requirements-list li:before, .course-summary .course-tab-panel-reviews .course-description ul.requirements-list li:before,
.widget_lp-widget-recent-courses .course-entry .course-detail .course-meta-field, .widget_lp-widget-featured-courses .course-entry .course-detail .course-meta-field, .widget_lp-widget-popular-courses .course-entry .course-detail .course-meta-field,
.widget_lp-widget-recent-courses .course-entry .course-detail a h3:hover, .widget_lp-widget-featured-courses .course-entry .course-detail a h3:hover, .widget_lp-widget-popular-courses .course-entry .course-detail a h3:hover,
.course-summary .course-tabs .learn-press-nav-tabs .course-nav.active a,
#rs-home-video .counter-top-area .rs-counter,
.rs-latest-news .news-list-block .news-list-item .news-title a:hover, .rs-latest-news .news-list-block .news-list-item .news-title a:focus,
.t-con-inner i,
.rs-courses-3 .course-item .course-body .course-title-archive a:hover,
.rs-services-style1 .services-icon,
.rs-courses-categories .courses-item:hover i,
.rs-latest-news .news-normal-block .news-date i,
#cl-testimonial .testimonial-slide3 .slick-active button:before,
testimonial-slide3 .slick-active:hover button:before,
.rs-course .cource-item .course-body .course-title a:hover,
.rs-course .cource-item .course-body span a:hover,
.rs-team-style2 .team-item .team-content h3 a:hover,
#rs-header .logo-area a,
article.sticky .blog-title a:after,
.btm-cate li a:hover,
.toolbar-contact-style4 ul li i,
#rs-header .toolbar-area .toolbar-sl-share ul li a:hover,
.primary-color,
.rs-events .event-item .event-date i,
.rs-events .event-item .event-title a:hover,
.rs-events .event-item .event-meta > div i,
.rs-services1 .services-icon i,
.rs-video-2 .popup-videos,
.services-style-2 .services-desc h3 a:hover,
.counter-top-area .rs-counter-list i,
.rs-portfolio .portfolio-item .p-zoom,
.team-slider-style1 .team-inner-wrap .social-icons a:hover,
#rs-footer .footer-top .recent-post-widget .post-item .post-title a:hover,
#rs-footer .footer-top ul#menu-footer-menu li:hover a, #rs-footer .footer-top ul#menu-footer-menu li:hover:before,
.nav-footer ul li a:hover,
#rs-footer .footer-bottom .footer-bottom-share ul li a:hover,
#rs-header .toolbar-area .toolbar-contact ul li a:hover,
#rs-header.style2 .menu-area .menu-responsive .nav-link-container a:hover,
.team-slider-style2 .team-item-wrap .team-content .display-table .display-table-cell .team-title,
.rs-blog .blog-item .blog-content h3 a:hover,
#cta-sec .readon:hover,
#cta-sec .readon:hover:before,
.team-slider-style2 .team-item-wrap .team-content .display-table .display-table-cell .team-name a:hover,
.services-tabs .dropcap:first-letter,
#rs-header.header-styl-3 .toolbar-contact i, #rs-header.header-styl-3 .toolbar-contact a:hover,
#rs-header.header-styl-3 .toolbar-sl-share ul li a:hover,
.rs-breadcrumbs ul li,
.widget_contact_widget i,
.rs-breadcrumbs ul li a:hover,
.team-gird .team-style2 .team-content .display-table .display-table-cell .team-title,
.team-gird .team-style1 .team-item .social-icons a:hover,
.team-gird .team-style4 .team-content .team-name a:hover,
.single-teams .ps-informations ul li.social-icon i,
.bs-sidebar ul a:hover,
.main-contain ol li:before, 
.rs-blog .blog-item .full-blog-content .blog-button-icon a:hover, 
.rs-heading .sub-text, 
.menu-area .navbar ul li.current-menu-parent > a, 
.menu-area .navbar ul li.current-menu-parent > a, 
.menu-area .navbar ul li.current-menu-ancestor > a, 
#rs-header.style2 .menu-area .navbar ul li.current-menu-parent > a, 
#rs-header.style2 .menu-area .navbar ul li.current-menu-parent > a, 
#rs-header.style2 .menu-area .navbar ul li.current-menu-ancestor > a,
#rs-header .toolbar-area .toolbar-sl-share ul li a i.fa-sign-in,
.rs-services-style1.icon-left .services-icon i,
.review-stars-rated .review-stars.empty, .review-stars-rated .review-stars.filled,
.rs-products-slider .product-item .product-title a:hover, .rs-products-slider .product-item .product-title a:focus, .rs-products-slider .product-item .product-title a:active,
.rs-course .cource-item .cource-img .image-link,
#cl-testimonial .testimonial-slide3 .slick-active button:before, #cl-testimonial .testimonial-slide3 .slick-active:hover button:before,
.event-item .event-img .image-link, .cource-item .event-img .image-link, .event-item .cource-img .image-link, .cource-item .cource-img .image-link,
.rs-latest-news .news-list-block .news-list-item .news-date i,
.latest-news-slider .slick-arrow,
#rs-footer.has-footer-contact .footer-contact-desc .contact-inner .phone-icon:before,
#rs-footer.has-footer-contact .footer-contact-desc .contact-inner .mail-icon:before,
#rs-footer.has-footer-contact .footer-contact-desc .contact-inner .address-icon:before,
.rs-courses-2 .cource-item .course-value .course-price,
#cl-testimonial .testimonial-slide2.slider2 .image-testimonial .testimonial-content:before, #cl-testimonial .testimonial-slide2.slider2 .image-testimonial .testimonial-content:after,
.portfolio-filter button:hover, .portfolio-filter button.active,
.rs-heading.style4 .title-inner h2 span.red-color, .rs-heading.style5 .title-inner h2 span.red-color, .rs-heading.style4 .title-inner h2 span.red-color-last, .rs-heading.style5 .title-inner h2 span.red-color-last,
.t-con-inner .contact-inf a:hover,
.rs-services-style1.icon-left .services-desc .services-title a:hover,
.rs-latest-news .news-normal-block .news-title a:hover, .rs-latest-news .news-normal-block .news-title a:focus,
.lp-archive-courses .course-left .course-icons a.active-grid i,
.lp-archive-courses .course-left .course-icons a:hover i,
.course-features-info h3.title,
.course-features-info ul li i,
.related-courses.rs-courses-3 .course-item .course-body .course-title a:hover,
.related-courses.rs-courses-3 .course-item .course-body .course-author .author-contain .course-price,
.related-courses.rs-courses-3 .course-meta .course-students i,
.related-courses.rs-courses-3 .course-meta .course-comments-count i,
.rs-events-2 .event-item .event-meta > div i,
.rs-events-2 .event-item .event-location i,
.woocommerce ul.products li.product .images-product .overley .winners-details .product-info ul li a:hover,
.woocommerce ul.products li.product .images-product .overley .winners-details .product-info ul li a i:hover,
.rs-breadcrumbs .breadcrumbs-inner .current-item, 
.rs-breadcrumbs .breadcrumbs-inner span a:hover,
body.rs-list-view .course-icons .active-list i,
.rs-blog .blog-item .blog-img .blog-link,
.rs-blog .blog-item .blog-full-area .blog-meta h3.blog-title a:hover,
.bs-sidebar .recent-post-widget .post-desc a:hover,
#rscontact-pages .contact-details .vc_icon_element .vc_icon_element-inner .vc_icon_element-icon,
#rscontact-pages .contact-details .wpb_text_column a:hover,
.rs-blog .blog-item .full-blog-content .blog-date a:hover,
.ps-navigation ul a:hover,
.rs-services-style3 .services-item .services-title a:hover,
.rs-courses-2 .cource-item .course-body span a,
.rs-courses-2 .cource-item .course-body .course-title a:hover,
.woocommerce .star-rating span::before, 
.woocommerce .star-rating::before,
.rs-latest-news2 .news-normal-block .news-title a:hover,
.rs-latest-news2 .news-list-block .news-list-item .news-title a:hover,
#home9-acheivements .services-item i,
.description ul li:before,
.services-style_5 .services-item:hover .services-title a,
body.learnpress .profile-tabs ul.nav-tabs li.active a
{
	color:<?php echo esc_attr($site_color); ?> !important;
}

<?php if(!empty($eshkool_option['preloader_bg_color'])){
	?>
		.book_preload{
		background: <?php echo esc_attr($eshkool_option['preloader_bg_color']);?>
	}
	<?php
}
?>
<?php if(!empty($eshkool_option['preloader_text_color'])){
	?>
		.book__page{
			background:<?php echo esc_attr($eshkool_option['preloader_text_color']); ?> !important; 
		}

	<?php
}
?>

#cl-testimonial .testimonial-slide3 .slick-active button:before, 
#cl-testimonial .testimonial-slide3 .slick-active:hover button:before,
body .about-tabs li.vc_tta-tab:hover a, body .about-tabs li.vc_tta-tab.vc_active a,
.learn-press-pagination .page-numbers > li span.page-numbers.current,
.lp-archive-courses .page-numbers li a:hover, .lp-archive-courses .page-numbers li a.active,
.woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce ul.products li.product .price{
	color:<?php echo esc_attr($site_color); ?> !important;
}

.readon,
.rs-services1 .services-item,
.rs-video-2 .overly-border:before,
.overly-border::before,
.overly-border::after,
.rs-portfolio .portfolio-item .portfolio-content .display-table:before,
.rs-portfolio .portfolio-item .portfolio-content .display-table:after,
.rs-portfolio .portfolio-item .p-zoom,
.rs-partner .partner-item img:hover,
.menu-area .navbar ul li ul.sub-menu,
.services-style-2 .services-desc,
.widget_brochures a:hover,
.single-teams .ps-informations ul li.social-icon i,
#rs-services .services-style-2:hover .services-desc,
.rs-services-style1.icon-left .services-desc .services-title a:hover,
.rs-course .cource-item .cource-img .course_author_img,
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce .wc-forward, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,
.rs-courses-2 .cource-item .cource-btn,
.readon.black:hover,
.services-style-2.center:hover,
body #cl-testimonial ul.slick-dots li button,
.rs-courses-3 .course-item .course-img .course_author_img,
.related-courses.rs-courses-3 .course-item .course-body .course-author .course_author_img,
.woocommerce ul.products li.product .images-product .overley .winners-details .product-info ul li a:hover,
.woocommerce-message, .woocommerce-error, .woocommerce-info, .woocommerce-message,
.rs-blog .blog-item .blog-img .blog-link{
	border-color: <?php echo esc_attr($site_color);?> !important;
}

.rs-acheivements.rs-acheivements2 .counter-top-area,
.readon.white:hover{
	border-color: <?php echo esc_attr($site_color);?> !important;
}

.rs-footer{
	background-color:<?php echo esc_attr($footer_bgcolor); ?> !important; 
}

.woocommerce span.onsale{
	background:<?php echo esc_attr($site_color);?> !important;
}

a{
	color:<?php echo  esc_attr($link_color);?> 
}
a:hover,.rs-blog .blog-item .full-blog-content .blog-title a:hover,
.toolbar-contact-style4 ul li .contact-inf a:hover {
	color:<?php echo esc_attr($link_hover_color);?>
}

#rs-header .menu-area .navbar ul li .sub-menu li a, #rs-header.style1 .menu-area .navbar ul li ul li a{
	color:<?php echo esc_attr($dropdown_text_color);?> !important;
}
#rs-header .menu-area .navbar ul ul li a:hover ,
#rs-header .menu-area .navbar ul ul li.current-menu-item a{
	color:<?php echo esc_attr($drop_text_hover_color);?> !important;
}

#rs-header .menu-area .navbar ul > li > a:hover,
#rs-header.style1 .menu-area .navbar ul li a:hover
{
	 color: <?php echo esc_attr($menu_text_hover_color) ?> !important;
}
#rs-header .menu-area .navbar ul > li.current-menu-item > a {
    color: <?php echo esc_attr($menu_active_color) ?> !important;
}


.hover-color
{
	color:<?php echo esc_attr($hover_color); ?> !important; 
}

.rs-services4 .service-item .service-button a:hover,
.hover-bg,
.services-style-2 .services-desc .btn-more:hover,
#cta-sec .readon,
#about-sec2 .mt-20:hover,
.team-slider-style2 .team-item-wrap .team-img .normal-text .team-title,
.rs-about3 .vc_tta-panel-heading:hover,
.rs-about3 .vc_tta-panel.vc_active .vc_tta-panel-heading,
.rs-about3 .vc_tta-panel-heading a i,
.services-tabs .vc_tta-tab > a,
#scrollUp i:hover,
.readon:hover, .readon:focus,
.readon-sm:hover,
.contact-form-area input[type="submit"]:hover,
.services-style-3:hover:after,
.team-gird .team-style3 .team-wrapper:hover .team_desc,
#rs-header.style1 .has-quote-text .get-quote:hover,
.rs-calltoaction .cta-button:hover,
.rs-events .event-item .event-btn a:hover,
.rs-courses-2 .cource-item .cource-btn:hover,
.rs-team-style1 .team-item .team-social .social-icon:hover,
#cl-testimonial .hide_bullet .testimonial-slide2.slider2 .slick-arrow:hover,
#rs-header.header-style-4 .sidebarmenu-area .nav-link-container a,
#rs-header.header-style-4 .sidebarmenu-area .sticky_search a:hover,
.rs-events-2 .event-item .event-btn a:hover,
body.single-events .course-features-info .book-btn a:hover,
.rs-blog .blog-item .full-blog-content .blog-button a:hover,
.comments-area .comment-list li.comment .reply a:hover,
.comment-respond .form-submit #submit:hover{
	background:<?php echo esc_attr($hover_color); ?> !important; 
}

.rs-porfolio-details.project-gallery .file-list-image:hover .p-zoom:hover{
	color: #fff; 
}

.hover-border,
.readon:hover, .readon:focus,
.services-style-2:hover .services-desc,
.rs-courses-2 .cource-item .cource-btn{
	border-color: <?php echo esc_attr($hover_color); ?> !important;
}

.rs-courses-2 .cource-item .cource-btn:hover{
	border-color: <?php echo esc_attr($hover_color); ?> !important;
}

.rs-portfolio .portfolio-item .title-block
{
	background: rgba(<?php echo esc_attr($site_color_rgb)?>,.9) !important; 
}
.rs-portfolio .portfolio-item .portfolio-content,
.rs-blog .blog-item .blog-content:before{
	background: rgba(<?php echo esc_attr($site_color_rgb)?>,.8) !important; 
}
.team-slider-style1 .team-inner-wrap .overlay,
.team-slider-style2 .team-item-wrap .team-content:before{
	background: rgba(<?php echo esc_attr($site_color_rgb)?>,.7) !important; 
}

.footer-top .container, .footer-top ul, .footer-top ul li, .footer-top li a{
	position:relative;
	z-index: 100;
}
.owl-carousel .owl-nav [class*="owl-"]{
	color:#fff !important
}
kbd,
.owl-carousel .owl-nav [class*="owl-"]
{
	background: <?php echo esc_attr($eshkool_option['secondary_color']) ;?> !important;
}
<?php if(!empty($eshkool_option['copyright_bg'])){
	?>
	<?php
} 

	if(!empty($eshkool_option['kids-topbar-bg'])){
		?>
		.kids-toolbar-area{
			background: <?php echo esc_attr($eshkool_option['kids-topbar-bg']) ;?> !important;
		}
		<?php
	}
	
	if(!empty($eshkool_option['kids-topbar_text_color'])){
		?>
		.kids-toolbar-area, .kids-toolbar-area ul li{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_text_color']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-topbar_link_color'])){
		?>
		.kids-toolbar-area a, .kids-toolbar-area ul li a{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_link_color']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-topbar_link_hover_color'])){
		?>
		.kids-toolbar-area a:hover, .kids-toolbar-area ul li a:hover{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_link_hover_color']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-topbar_icon_color'])){
		?>
		.kids-toolbar-area i, .kids-toolbar-area ul li i{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_icon_color']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-topbar_icon_hover_color'])){
		?>
		.kids-toolbar-area i:hover, .kids-toolbar-area ul li i:hover{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_icon_hover_color']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-topbar_icon_hover_color'])){
		?>
		.kids-toolbar-area i:hover, .kids-toolbar-area ul li i:hover{
			color: <?php echo esc_attr($eshkool_option['kids-topbar_icon_hover_color']) ;?> !important;
		}
		<?php
	}

	
	if(!empty($eshkool_option['kids-header-bg'])){
		?>
		.kids-header .menu-area{
			background: <?php echo esc_attr($eshkool_option['kids-header-bg']) ;?> !important;
		}
		<?php
	}

	if(!empty($eshkool_option['kids-header_link_hover_color'])){
		?>
		#rs-header.kids-header .menu-area .get-quote{
			background: <?php echo esc_attr($eshkool_option['kids-header_link_hover_color']) ;?> !important;
		}
		<?php
	}


	if(!empty($eshkool_option['kids-header_link_color'])){
		?>
		.kids-header .menu-area ul > li > a{
			color: <?php echo esc_attr($eshkool_option['kids-header_link_color']) ;?> !important;
		}
		<?php
	}
	if(!empty($eshkool_option['kids-header_link_hover_color'])){
		?>
		#rs-header.kids-header .menu-area .navbar ul > li > a:hover,
		#rs-header.kids-header .menu-area .navbar ul > li:hover > a,
		#rs-header.kids-header .menu-area .navbar ul > li.current_page_item > a,
		#rs-header.kids-header .menu-area#single-menu .navbar ul li.active a,
		#rs-header.kids-header .menu-area#single-menu .navbar ul li:hover a
		{
			color: <?php echo esc_attr($eshkool_option['kids-header_link_hover_color']) ;?> !important;
		}

		<?php
	}
	if(!empty($eshkool_option['kids-dropdown-bg'])){
		?>
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu{
			background: <?php echo esc_attr($eshkool_option['kids-dropdown-bg']) ;?> !important;
		}
		<?php
	}
	if(!empty($eshkool_option['kids-dropdown_link_color'])){
		?>
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu li a{
			color: <?php echo esc_attr($eshkool_option['kids-dropdown_link_color']) ;?> !important;
		}
		<?php
	}
	if(!empty($eshkool_option['kids-dropdown_link_hover_color'])){
		?>
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu li a:hover,
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu > li:hover > a,
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu ul.sub-menu > li:hover > a,
		#rs-header.kids-header .menu-area .navbar ul li ul.sub-menu li.current_page_item  a
		{
			color: <?php echo esc_attr($eshkool_option['kids-dropdown_link_hover_color']) ;?> !important;
		}
		<?php
	}
	?>
<?php if(!empty($eshkool_option['mobile_menu_icon_color'])){ ?>
	.header-styl-3 .menu-area .mobile-menu-link .nav-menu-link i,
	#rs-header.style1 .menu-area .mobile-menu-link .nav-menu-link i,
	.menu-area .mobile-menu-link .nav-menu-link i{
	
	color: <?php echo esc_attr($eshkool_option['mobile_menu_icon_color']) ;?> !important;
}
<?php } ?>

<?php if(!empty($eshkool_option['mobile_menu_container_bg'])){ ?>
	.sidenav{	
		background: <?php echo esc_attr($eshkool_option['mobile_menu_container_bg']) ;?> !important;
	}
<?php } ?>

<?php if(!empty($eshkool_option['mobile_menu_text_color'])){ ?>
	.mobile-menu-container ul li a{	
		color: <?php echo esc_attr($eshkool_option['mobile_menu_text_color']) ;?>;
	}
<?php } ?>

.sidenav .nav-close-menu-li button{
	background:transparent !important;
}

.sidenav .nav-close-menu-li button,
.sidenav .nav-close-menu-li button:after,
.sidenav .nav-close-menu-li button:before{
	border-color: <?php echo esc_attr($eshkool_option['mobile_menu_text_hover_color']) ;?> !important;
}

.sidenav .nav-close-menu-li button:after,
.sidenav .nav-close-menu-li button:before{
	background-color: <?php echo esc_attr($eshkool_option['mobile_menu_text_hover_color']) ;?> !important;
}

.sidenav .nav-close-menu-li button:hover:after, 
.sidenav .nav-close-menu-li button:hover:before{
	background-color: <?php echo esc_attr($eshkool_option['mobile_menu_text_hover_color']) ;?> !important;
}

.sidenav .nav-close-menu-li button:hover{
	border-color:<?php echo esc_attr($eshkool_option['mobile_menu_text_hover_color']) ;?> !important;
}


ul.sidenav .menu > li.menu-item-has-children:before{
	color: rgba(<?php echo esc_attr($border_color_icon)?>, 1) !important;
	background: rgba(<?php echo esc_attr($border_color_icon)?>, .2) !important;
	border-color: rgba(<?php echo esc_attr($border_color_icon)?>, .3) !important;
}


.mobile-menu-container ul li a{
	border-color: rgba(<?php echo esc_attr($border_color_mobile)?>,.02) !important;
}



<?php if(!empty($eshkool_option['mobile_menu_text_hover_color'])){ ?>
	.mobile-menu-container ul li a:hover{	
		color: <?php echo esc_attr($eshkool_option['mobile_menu_text_hover_color']) ;?>;
}
<?php } ?>


<?php if(!empty($eshkool_option['mobile_menu_text_active_color'])){ ?>
	.sidenav .menu-main-menu-container .menu li.current-menu-parent > a, .sidenav .menu-main-menu-container .menu li.current-menu-parent > ul .current-menu-item > a, .sidenav .menu-main-menu-container .menu li.current-menu-ancestor > a{	
		color: <?php echo esc_attr($eshkool_option['mobile_menu_text_active_color']) ;?> !important;
	}
<?php } ?>

<?php } ?>


</style>
<?php
if(is_page() || is_single()){
  	$padding_top = get_post_meta(get_the_ID(), 'content_top', true);
  	$padding_bottom = get_post_meta(get_the_ID(), 'content_bottom', true);
  	$content_page_width = get_post_meta(get_the_ID(), 'content_page_width', true);
  	if($padding_top != '' || $padding_bottom != ''){
	  	?>
	  	  <style>
	  	  	.main-contain #content{
	  	  		<?php if(!empty($padding_top)): ?>padding-top:<?php echo esc_attr($padding_top); endif;?> !important;
	  	  		<?php if(!empty($padding_bottom)): ?>padding-bottom:<?php echo esc_attr($padding_bottom); endif;?> !important;
	  	  	}
	  	  </style>	
	  	<?php
	  }
	if(!empty($content_page_width)){
			?>
	  	  <style>
	  	  	@media screen and (min-width: 1500px){
				.container {
				    max-width: <?php echo esc_attr( $content_page_width );?>;
				    width: 100%;
				}
			}
	  	  </style>	
	  	<?php
	}
  }   
}
?>