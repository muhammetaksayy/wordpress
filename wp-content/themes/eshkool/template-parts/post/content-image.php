<?php
/**
 * @author  rs-theme
 * @since   1.0
 * @version 1.0 
 */
?>

<?php if(has_post_thumbnail()):
?>
	<div class="bs-img">
	  <?php the_post_thumbnail()?>
	</div>
<?php
endif;?>
<div class="single-content-full">
 <div class="bs-desc">
	<?php if ( is_single() || '' === get_the_post_thumbnail() ) {

		// Only show content if is a single post, or if there's no featured image.
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'eshkool' ),
			get_the_title()
		) );

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'eshkool' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) 
	 );

	};
?>
</div>
<div class="clearfix"></div>
</div>