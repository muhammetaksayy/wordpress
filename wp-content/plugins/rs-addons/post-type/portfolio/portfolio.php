<?php
/**
 * Team custom post type
 * This file is the basic custom post type for use any where in theme.
 * 
 * @package grassywp
 * @author RS Theme
 * @link http://www.rstheme.com
 */
?>
<?php
// Register Portfolio Post Type
function rs_portfolio_register_post_type() {
	$labels = array(
		'name'               => esc_html__( 'Portfolios', 'rsconstruction' ),
		'singular_name'      => esc_html__( 'Portfolio', 'rsconstruction' ),
		'add_new'            => esc_html_x( 'Add New Portfolio', 'rsconstruction', 'rsconstruction' ),
		'add_new_item'       => esc_html__( 'Add New Portfolio', 'rsconstruction' ),
		'edit_item'          => esc_html__( 'Edit Portfolio', 'rsconstruction' ),
		'new_item'           => esc_html__( 'New Portfolio', 'rsconstruction' ),
		'all_items'          => esc_html__( 'All Portfolios', 'rsconstruction' ),
		'view_item'          => esc_html__( 'View Portfolio', 'rsconstruction' ),
		'search_items'       => esc_html__( 'Search Portfolios', 'rsconstruction' ),
		'not_found'          => esc_html__( 'No Portfolios found', 'rsconstruction' ),
		'not_found_in_trash' => esc_html__( 'No Portfolios found in Trash', 'rsconstruction' ),
		'parent_item_colon'  => esc_html__( 'Parent Portfolio:', 'rsconstruction' ),
		'menu_name'          => esc_html__( 'Portfolio', 'rsconstruction' ),
	);
	$args = array(
		'labels'             => $labels,
		'public'             => true,	
		'show_in_menu'       => true,
		'show_in_admin_bar'  => true,
		'can_export'         => true,
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          =>  plugins_url( 'img/icon.png', __FILE__ ),
		'supports'           => array( 'title', 'thumbnail','editor' ),		
	);
	register_post_type( 'portfolios', $args );
}
add_action( 'init', 'rs_portfolio_register_post_type' );
function tr_create_portfolio() {
	register_taxonomy(
		'portfolio-category',
		'portfolios',
		array(
			'label' => __( 'Categories','rsconstruction' ),
			'rewrite' => array( 'slug' => 'portfolio-category' ),
			'hierarchical' => true,
		)
	);
}
add_action( 'init', 'tr_create_portfolio' );
