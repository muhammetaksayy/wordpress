<?php
/**
Plugin Name: Rs Addons
Plugin URI: https://codecanyon.net
Version: 1.0
Description: Theme Core Plugins
Author: RS Theme
Author URI: http://www.rstheme.com
Text Domain: rs-addons
Domain Path: /languages/
*/


add_action( 'init', 'rsaddon_load_textdomain' );
  
/**
 * Load plugin textdomain.
 */
function rsaddon_load_textdomain() {
  load_plugin_textdomain( 'rs-addons', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' ); 
}


// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
    die( 'You shouldnt be here' );
}
function rs_visual_composer_plugin() {
  if ( is_plugin_active('js_composer/js_composer.php') ) {
        
      // Create multi dropdown param type
        vc_add_shortcode_param( 'dropdown_multi', 'dropdown_multi_settings_field' );
        function dropdown_multi_settings_field( $param, $value ) {
           $param_line = '';
           $param_line .= '<select multiple name="'. esc_attr( $param['param_name'] ).'" class="wpb_vc_param_value wpb-input wpb-select '. esc_attr( $param['param_name'] ).' '. esc_attr($param['type']).'">';
           foreach ( $param['value'] as $text_val => $val ) {
               if ( is_numeric($text_val) && (is_string($val) || is_numeric($val)) ) {
                            $text_val = $val;
                        }
                        $text_val = __($text_val, "js_composer");
                        $selected = '';

                        if(!is_array($value)) {
                            $param_value_arr = explode(',',$value);
                        } else {
                            $param_value_arr = $value;
                        }

                        if ($value!=='' && in_array($val, $param_value_arr)) {
                            $selected = ' selected="selected"';
                        }
                        $param_line .= '<option class="'.$val.'" value="'.$val.'"'.$selected.'>'.$text_val.'</option>';
                    }
           $param_line .= '</select>';

           return  $param_line;
        } 



  }

}
add_action( 'admin_init', 'rs_visual_composer_plugin' );

  if ( !function_exists( 'rs_vc_modules' ) ) {
    function rs_vc_modules(){
          
      $modules = array( 'inc/abstruct','blog', 'latest_blog', 'latest_event', 'team-member-slider','latest_blog_slider', 'latest_product_slider', 'rs_gallery', 'banner', 'course-filter', 'about', 'banner', 'calltoaction', 'client', 'contact', 'countdown', 'course-slider', 'course-grid', 'course-category', 'heading', 'icon_title', 'team-grid', 'team-slider', 'rs_service_slider', 'services', 'video', 'event-slider', 'course-search', 'countdown-shedule');
      foreach ( $modules as $module ) {
        require_once 'vc-modules/' . $module. '.php';
      }
    }
  }
 add_action( 'after_setup_theme', 'rs_vc_modules');
//All Post type include here

$dir = plugin_dir_path( __FILE__ );

//For portfolio
require_once $dir .'/post-type/portfolio/portfolio.php';

//For client
require_once $dir .'/post-type/client/client.php';
//For Event
require_once $dir .'/post-type/event/event.php';
//
require_once $dir .'/post-type/team/team.php';
//
require_once $dir .'/post-type/service/service.php';