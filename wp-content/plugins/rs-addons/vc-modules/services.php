<?php
/*
Element Description: Rs Services Box
*/
    // Element Mapping
     function vc_RsServices_mapping() {         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }         
        // Map the block with vc_map()
        vc_map( 
            array(
				'name'        => __('Rs Service Box', 'eshkool'),
				'base'        => 'vc_RsServices',
				'description' => __('Rs Service Box Information', 'eshkool'), 
				'category'    => __('by RS Theme', 'eshkool'),   
				'icon'        => get_template_directory_uri().'/framework/assets/img/vc-icon.png',           
				'params'      => array(   
                         
                    array(
						'type'        => 'textfield',
						'holder'      => 'h3',
						'class'       => 'title-class',
						'heading'     => __( 'Rs Service Title ', 'eshkool' ),
						'param_name'  => 'title',
						'value'       => __( '', 'eshkool' ),
						'description' => __( 'Rs services title here', 'eshkool' ),
						'admin_label' => false,
						'weight'      => 0,                       
                    ),                   
             				
					array(
						"type"        => "textarea_html",
						"holder"      => "div",
						"class"       => "",
						"heading"     => __( "Services content here", "eshkool" ),
						"param_name"  => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a 	"param_name"
						"value"       =>'',
						"description" => __( "Duis in mi erat. Phasellus vitae in to lorem vehicula, viverra libero quis, sodalesnulla. Donec at the turpis quis tellus laoreet.", "eshkool" )
					 ),
					 
					 array(
						"type" => "dropdown",
						"heading" => __("Select Sevices Style", "eshkool"),
						"param_name" => "service_style",
						"value" => array(						    
							'Style 1' => "Style 1",						
							'Style 2' => "Style 2", 
							'Style 3' => "Style 3", 				
							'Style 4' => "Style 4", 				
							'Style 5' => "Style 5", 				
							'Style 6' => "Style 6",
							'Style 7' => "Style 7", 				
						),						
					),					 
					
					 array(
							"type"        => "attach_image",
							"heading"     => __( "Service Image", "eshkool" ),
							"description" => __( "Add services image", "eshkool" ),
							"param_name"  => "screenshots",
							"value"       => "",
							"dependency" => Array('element' => 'service_style', 'value' => array('Style 2', 'Style 4', 'Style 5', 'Style 6', 'Style 7')),
						),

					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'eshkool' ),
						'value' => array(
							__( 'Font Awesome', 'eshkool' ) => 'fontawesome',
							__( 'Open Iconic', 'eshkool' )  => 'openiconic',
							__( 'Typicons', 'eshkool' )     => 'typicons',
							__( 'Entypo', 'eshkool' )       => 'entypo',
							__( 'Linecons', 'eshkool' )     => 'linecons',
							__( 'Mono Social', 'eshkool' )  => 'monosocial',
							__( 'Material', 'eshkool' )     => 'material',
						),
						'admin_label' => true,
						'param_name' => 'icon_type',
						'description' => __( 'Select icon library.', 'eshkool' ),
						"dependency" => Array('element' => 'service_style', 'value' => array('Style 1', 'Style 3', 'Style 6'))
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),

					array(
						"type"       => "dropdown",
						"heading"    => __("Icon Alignment", "eshkool"),
						"param_name" => "icon_align",
						"value" => array(						    
							'Top'  => "icon-top",				
							'Left' => "icon-left",					
						),
						"dependency" => Array('element' => 'service_style', 'value' => array('Style 1')),					
					),

					array(
						'type'       => 'iconpicker',
						'heading'    => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_openiconic',
						'value'      => 'vc-oi vc-oi-dial',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon'    => false,
							// default true, display an "EMPTY" icon?
							'type'         => 'openiconic',
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					array(
						'type'       => 'iconpicker',
						'heading'    => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_typicons',
						'value'      => 'typcn typcn-adjust-brightness',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'entypo',
						),
					),
					array(
						'type'       => 'iconpicker',
						'heading'    => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_linecons',
						'value'      => 'vc_li vc_li-heart',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value'   => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					array(
						'type'       => 'iconpicker',
						'heading'    => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_monosocial',
						'value'      => 'vc-mono vc-mono-fivehundredpx',
						// default value to backend editor admin_label
						'settings' => array(
						'emptyIcon' => false,
						// default true, display an "EMPTY" icon?
						'type'         => 'monosocial',
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'monosocial',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'eshkool' ),
						'param_name' => 'icon_material',
						'value' => 'vc-material vc-material-cake',
						// default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'type' => 'material',
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'icon_type',
							'value' => 'material',
						),
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					
					array(
						'type'        => 'vc_link',
						'heading'     => __( 'URL (Link)', 'eshkool' ),
						'param_name'  => 'button_link',
						'description' => __( 'Add link to Serices Pages.', 'eshkool' ),						
					),

					
					array(
						'type'       => 'dropdown',
						'heading'    => __( 'Services Align', 'rsaddon' ),
						'param_name' => 'align',
						'value' => array(
							__( 'Left', 'rsaddon' )   => 'left',
							__( 'Center', 'rsaddon' ) => 'center',
							__( 'Right', 'rsaddon' )  => 'right',
						),
						"dependency" => Array('element' => 'service_style', 'value' => array('Style 2', 'Style 4', 'Style 5', 'Style 6' ,'Style 7')),
						'description' => __( 'Select alignment here.', 'rsaddon' ),
					),

					array(
						"type"       => "dropdown",
						"heading"    => __("Show ReadMore", 'rsaddon'),
						"param_name" => "readmore",
						"value"      => array(
							'Yes' => "Yes",
							'No' => "No",
						),	
						"dependency" => Array('element' => 'service_style', 'value' => array('Style 4')),					
					),	
												
					
					array(
						"type"        => "colorpicker",
						"class"       => "",
						"heading"     => __( "Title color", "eshkool" ),
						"param_name"  => "titlecolor",
						"value"       => '', //Default Red color
						"description" => __( "Choose color", "eshkool" ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Style',
					 ),

					 array(
						"type"        => "colorpicker",
						"class"       => "",
						"heading"     => __( "Icon background color", "eshkool" ),
						"param_name"  => "iconbg",
						"value"       => '', //Default Red color
						"description" => __( "Choose color", "eshkool" ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Style',
						"dependency"  => Array('element' => 'service_style', 'value' => array('Style 3')),
					 ),	

					array(
						"type"        => "textfield",
						"heading"     => __("Read More Text", 'rsaddon'),
						"param_name"  => "more_text",
						'description' => __( 'Type your read more text here', 'rsaddon' ),
						"dependency"  => Array('element' => 'readmore_type', 'value' => array('Style 4')),
					),	

					 array(
						"type"        => "colorpicker",
						"class"       => "",
						"heading"     => __( "Icon color", "eshkool" ),
						"param_name"  => "iconcolor",
						"value"       => '', //Default Red color
						"description" => __( "Choose color", "eshkool" ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Style',
					 ),				 
					

					array(
						"type"        => "colorpicker",
						"class"       => "",
						"heading"     => __( "Text color", "eshkool" ),
						"param_name"  => "textcolor",
						"value"       => '', //Default Red color
						"description" => __( "Choose color", "eshkool" ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Style',
					 ),				 
					
					array(
						"type"        => "colorpicker",
						"class"       => "",
						"heading"     => __( "Background color", "eshkool" ),
						"param_name"  => "bgcolor",
						"value"       => '', //Default Red color
						"description" => __( "Choose color", "eshkool" ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Style',
					 ),				 
								 
					
					array(
						'type'        => 'textfield',
						'heading'     => __( 'Extra class name', 'eshkool' ),
						'param_name'  => 'el_class',
						'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool' ),
					),	
					
					array(
					'type'       => 'css_editor',
					'heading'    => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group'      => __( 'Design Options', 'eshkool' ),
				),            
                        
                ),				
					
            )
        );                                
        
    }
     
  add_action( 'vc_before_init', 'vc_RsServices_mapping' );
     
    // Element HTML
    function vc_RsServices_html( $atts,$content ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'title'            => '',
					'icon_fontawesome' => 'fa fa-users',
					'icon_openiconic'  => '',
					'icon_typicons'    => '',
					'icon_entypo'      => '',
					'icon_linecons'    => '',
					'icon_monosocial'  => '',
					'icon_material'    => '',
					'icon'             => 'Image',					
					'titlecolor'       => '',
					'iconbg'           => '',
					'bgcolor'		   => '',
					'align'             => 'left',
					'textcolor'        => '',
					'iconcolor'        => '',
					'bgcolor'          => '', 
					'text_font'        => '',
					'el_class'         =>'',
					'service_style'    => 'Style 1',
					'icon_align'       => 'top',
					'button_link'      => '',
					'icon_type'        => '',					
					'css'              => '',
					'readmore'              => 'Yes',
					'more_text'        => 'Read More',
                ), 
                $atts,'vc_RsServices'
            )
        );
	
        $a = shortcode_atts(array(
          'screenshots' => 'screenshots',
        ), $atts);
           
		// Enqueue needed icon font.
		vc_icon_element_fonts_enqueue( $icon_type );
		
		//parse link for vc linke		
		$button_link = ( '||' === $button_link ) ? '' : $button_link;
		$button_link = vc_build_link( $button_link );
		$use_link = false;
		if ( strlen( $button_link['url'] ) > 0 ) {
			$use_link = true;
			$a_href = $button_link['url'];
			$a_title = $button_link['title'];
			$a_target = $button_link['target'];
		}
		
		if ( $use_link ) {
			$attributes[] = 'href="' . esc_url( trim( $a_href ) ) . '"';
			$attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
			if ( ! empty( $a_target ) ) {
				$attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
			}
		}
		$attributes = implode( ' ', $attributes );
		
		
		//Checl icon or image here

		if($icon_fontawesome != ''):
		  $services_icon = esc_attr($icon_fontawesome);
		endif;

		if ($icon_openiconic != ''):
			$services_icon = esc_attr($icon_openiconic);
		endif;

		if ($icon_typicons != ''):
			$services_icon = esc_attr($icon_typicons);
		endif;

		if ($icon_entypo != ''):
			$services_icon = esc_attr($icon_entypo);
		endif;

		if ($icon_linecons != ''):
			$services_icon = esc_attr($icon_linecons);
		endif;

		if ($icon_monosocial != ''):
			$services_icon = esc_attr($icon_monosocial);
		endif;
		
		if ($icon_material != ''):
			$services_icon = esc_attr($icon_material);
		endif;	
		
        $img = wp_get_attachment_image_src($a["screenshots"], "large");
        $imgSrc = $img[0];
        $imageClass	= '';
        if(!empty($imgSrc)):
			$imageClass='<div class="services-icon"><img src="'.$imgSrc.'" alt="Rs-service" /></div>';	
		endif;	

		$readmore_text = ($readmore == 'Yes') ? '<div class="service-button"><a '. $attributes.' class="readon rs_button">'.$more_text.'</a></div>' : '';

		
		//extract content
		$atts['content'] = $content;

		//extract css edit box
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
		
		 //custom class added
		$wrapper_classes = array($el_class) ;			
		$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
		$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );


		$iconcolor  = ($iconcolor) ? ' style="color: '.$iconcolor.'"' : '';
		$titlecolor = ($titlecolor) ? ' style="color: '.$titlecolor.'"' : '';
		$textcolor  = ($textcolor) ? ' style="color: '.$textcolor.'"' : '';
		$bgcolor    = ($bgcolor) ? ' style="background: '.$bgcolor.'"' : '';
		$iconbg    = ($iconbg) ? ' style="background: '.$iconbg.'"' : '';
		
        //checking services style
                
        // Fill $html var with data
		if($service_style=='Style 1')
		{
	        $html = '
			<div  class="rs-services-style1 '.$css_class.' '.$css_class_custom.' '.$icon_align.'">
				<div class="services-item rs-animation-hover" '.$bgcolor.'>
				    <div class="services-icon" '.$bgcolor.'>
				        <i '.$iconcolor.'class="vc_icon_element-icon '.$services_icon.' rs-animation-scale-up"></i>
				    </div>
				    <div class="services-desc">
				        <h4 class="services-title"><a '.$titlecolor.' '. $attributes.'>'.$title.'</a></h4>
				        <p '.$textcolor.'>'.$atts['content'].'</p>
				    </div>
				</div>
			</div>
			';   	
	  		return $html;
		} 
		if($service_style=='Style 3')
		{
	        $html = '
			<div class="rs-services-style3 '.$css_class.' '.$css_class_custom.'">
				<div class="services-item rs-animation-hover">
				    <div class="services-icon">
				        <i '.$iconbg.' class="vc_icon_element-icon '.$services_icon.' rs-animation-scale-up"></i>
				    </div>
				    <div class="services-desc">
				        <h4 class="services-title"><a '. $attributes.'>'.$title.'</a></h4>
				        <p>'.$atts['content'].'</p>
				    </div>
				</div>
			</div>
			';   	
	  		return $html;
		}
		if($service_style=='Style 4')
		{
	        $html = '
	        <div class="rs-services4 '.$align.' '.$css_class.' '.$css_class_custom.' ">
				<div class="service-item content-overlay">
	                <div class="service-img">
	                	'.$imageClass.'
	                	<h4 class="services-title"><a '. $attributes.'>'.wp_kses_post($title).'</a></h4>';
	                $html .='</div>

	                <div class="service-content">
	                	<div class="service-excerpt">
	                    	'.$atts['content'].'
	                    	'.$readmore_text.'
	                    </div>
	                </div>
	            </div>
            </div>
			';   	
	  		return $html;
		}
		if($service_style=='Style 5')
		{
	        $html = '<div class="rs-services"><div class="service-inner services-style_5 '.$align.' '.$css_class.' '.$css_class_custom.'">
					<div class="services-wrap"> 
				        <div class="services-item">
				            <a '. $attributes.'>'.$imageClass.'</a>
							<div class="services-desc">	
								<h4 class="services-title"><a '. $attributes.'>'.wp_kses_post($title).'</a></h4>		
								'.$atts['content'].' 
							</div>		
						</div>	
					</div>
				</div>
			</div>';   	
	  		return $html;
		}
		if($service_style=='Style 6')
		{
	        $html = '<div class="rs-services"><div class="service-inner services-style_6 '.$align.' '.$css_class.' '.$css_class_custom.'">
					<div class="services-wrap"> 
				        <div class="services-item">
				            '.$imageClass.'
							<div class="services-desc">	
							    <i '.$iconbg.' class="vc_icon_element-icon '.$services_icon.' rs-animation-scale-up"></i>
								<h4 class="services-title"><a '. $attributes.'>'.wp_kses_post($title).'</a></h4>		
								'.$atts['content'].' 
							</div>		
						</div>	
					</div>
				</div>
			</div>';   	
	  		return $html;
		}

		if($service_style == 'Style 7')
		{
	        $html = '<div class="rs-services"><div class="service-inner services-style_5 style_7 '.$align.' '.$css_class.' '.$css_class_custom.'">
					<div class="services-wrap"> 
				        <div class="services-item">
				            <a '. $attributes.'>'.$imageClass.'</a>
							<div class="services-desc">	
								<h4 class="services-title"><a '. $attributes.'>'.wp_kses_post($title).'</a></h4>		
								'.$atts['content'].' 
							</div>		
						</div>	
					</div>
				</div>
			</div>';   	
	  		return $html;
		}

		else{
			$html = '<div class="rs-services"><div class="service-inner services-style-2 '.$align.' '.$css_class.' '.$css_class_custom.'">
					<div class="services-wrap"> 
				        <div class="services-item">
				            '.$imageClass.'
							<div class="services-desc">	
								<h4 class="services-title"><a '. $attributes.'>'.wp_kses_post($title).'</a></h4>		
								'.$atts['content'].' 
							</div>		
						</div>	
					</div>
				</div>
			</div>';   	
	  		return $html;
		}
 }
 add_shortcode( 'vc_RsServices', 'vc_RsServices_html' ); 