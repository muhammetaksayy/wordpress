<?php 
	global $eshkool_option;
?>


<div id="rs-courses-4" class="rs-courses-4">	
	<div class="row">
		<?php 	
			while($best_wp->have_posts()): $best_wp->the_post();
			$taxonomy     = "course_category";		
			$post_title   = get_the_title() ? get_the_title() : '';
			$post_img_url = get_the_post_thumbnail_url($best_wp->ID);
			$post_url     = get_post_permalink($best_wp->ID);
		    $cats_show    = get_the_term_list( $best_wp->ID, $taxonomy, ' ', '<span class="separator">,</span> ? ');
		    $excerpt      = get_the_excerpt();
		    $course_id = get_the_ID();
			$rstheme_course = LP()->global['course'];

			if ( empty( $rstheme_course ) ) return;

			$course_author = get_post_field( 'post_author', $course_id );
			$course_enroll_count = $rstheme_course->get_users_enrolled();
			$course_enroll_count = $course_enroll_count ? $course_enroll_count : 0;	
		    
		    if ( function_exists( 'learn_press_get_course_rate' ) ) {
				$course_rate_res = learn_press_get_course_rate( $course_id, false );
				$course_rate     = $course_rate_res['rated'];
				$course_rate_total = $course_rate_res['total'];
				$course_rate_text = $course_rate_total > 1 ? esc_html__( 'Reviews', 'rs-addons' ) : esc_html__( 'Review', 'rs-addons' );
			}
			$duration   = get_post_meta( $course_id, '_lp_duration', true );
			$duration   = absint( $duration );
			$duration   = !empty( $duration ) ? $duration : false;

			$show_cat = '';
   			if(!empty($eshkool_option['off_course_cat'])) {
				$show_cat = '<span> '.$cats_show.'</span>';
			}

		    	echo '<div class="col-lg-'.$course_col_grid.' col-md-6">
	                <div class="cource-item">
	                    <div class="cource-img">
	                     	<img src="'.$post_img_url.'" alt="'.$post_title.'"/>
	                        <a class="image-link" style="color:#fff; font-size:15px;" href="'.$post_url.'">
							<i class="fab fa-staylinked" style="color:#fff; font-size:35px;"></i>
	                        </a>'; 
	                        ?>
	                        <?php
							if(!empty($eshkool_option['off_course_apply']) || !empty($eshkool_option['off_course_price']) ){
	                    	echo '<div class="course-value">'; ?>
		                    	<?php 
		                    	if(!empty($eshkool_option['off_course_price'])):
		                    		learn_press_course_price(); ?>
		                        	
		                        <?php
		                        endif;
		                         echo '</div>


	                   </div>
	                    <div class="course-body">


	                    	'.$show_cat.' ';
		                    ?>
		                    <?php
			                    if(!empty($eshkool_option['off_course_title'])){

			                    	echo '<h4 class="course-title">
			                    	<a href="'.$post_url.'">'.$post_title.'	
										  	</a>
			                    	</h4>';
								}
							?>

							<?php
							if($show_des == 'yes'){
								if(!empty($eshkool_option['off_course_shortdes'])):
				                echo '<div class="course-desc">
		                    		<p>'.$excerpt.'</p>
		                    	</div>';
		                    		endif;
							}							
	                    	?>						

	                    <?php }
		                echo '</div>'; 

		                ?>

		                <?php if(!empty($eshkool_option['off_course_enroll_count']) || !empty($eshkool_option['off_course_review']) ){?>
	                    <div class="course-footer">
	                    	<?php if(!empty($eshkool_option['off_course_enroll_count'])):?>
		                        <div class="course-seats">
		                            <i class="fa fa-user"></i> <?php echo esc_attr($course_enroll_count); ?>
		                        </div>


	                        <?php endif; if(!empty($eshkool_option['off_course_review'])):?>

		                        <div class="course-button">
		                            <?php learn_press_course_review_template( 'rating-stars.php', array( 'rated' => $course_rate ) );?><span class="course-rating-total"> </span>
		                        </div>

	                    	<?php endif; ?>
	                    </div>
	                <?php }?>





	                <?php echo '</div>						
				</div>';
			endwhile; 
		wp_reset_query();?>	
   </div>
   <?php if(!empty($view_text)):?>
   		<div class="view-btn text-center">
   			<a class="readon" href="<?php echo esc_url($view_text_link); ?>"><?php echo esc_html($view_text); ?></a>
   		</div>
   <?php endif; ?>
</div>
