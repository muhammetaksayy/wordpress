<?php 
global $eshkool_option;
echo '<div class="rs-course rs-course-'.$slider_style.'">
		<div class="team-carousel owl-carousel owl-navigation-yes" data-carousel-options="'.esc_attr( $owl_data ).'">';	
				$cat;
		        $arr_cats=array();
		        $arr= explode(',', $cat);  

					for ($i=0; $i < count($arr) ; $i++) { 
		           	//$cats = get_term_by('slug', $arr[$i], $taxonomy);
		           	// if(is_object($cats)):
		           	$arr_cats[]= $arr[$i];
		           	//endif;
		        }

				if(empty($cat)){
		        	$best_wp = new wp_Query(array(
							'post_type' => 'lp_course',
							'posts_per_page' =>$team_per,
							'ignore_sticky_posts' => 1,
							
					));	 
				}        
		        else{
		        	$best_wp = new wp_Query(array(
							'post_type' => 'lp_course',
							'posts_per_page' =>$team_per,
							'ignore_sticky_posts' => 1,
							'tax_query' => array(
						        array(
						            'taxonomy' => 'course_category',
						            'field' => 'slug', //can be set to ID
						            'terms' => $arr_cats//if field is ID you can reference by cat/term number
						        ),
						    )
					));	  
		        }  
     
				while($best_wp->have_posts()): $best_wp->the_post();
				$taxonomy       = "course_category";		
				$post_title     = get_the_title() ? get_the_title() : '';
				$post_img_url   = get_the_post_thumbnail($best_wp->ID, 'eshkool_course_slider_iamge');
				$post_url       = get_post_permalink($best_wp->ID);
				$cats_show      = get_the_term_list( $best_wp->ID, $taxonomy, ' ', '<span class="separator">,</span> ? ');
				$excerpt        = get_the_excerpt()? '<p>'.get_the_excerpt().'</p>' : '';
				$course_id      = get_the_ID();
				$rstheme_course = LP()->global['course'];

				if ( empty( $rstheme_course ) ) return;

				$course_author = get_post_field( 'post_author', $course_id );
				$course_enroll_count = $rstheme_course->get_users_enrolled();
				$course_enroll_count = $course_enroll_count ? $course_enroll_count : 0;			   		    
			    
			    if ( function_exists( 'learn_press_get_course_rate' ) ) {
					$course_rate_res = learn_press_get_course_rate( $course_id, false );
					$course_rate     = $course_rate_res['rated'];
					$course_rate_total = $course_rate_res['total'];
					$course_rate_text = $course_rate_total > 1 ? esc_html__( 'Reviews', 'rs-addons' ) : esc_html__( 'Review', 'rs-addons' );
				}
				$duration           = get_post_meta( $course_id, '_lp_duration', true );
        		$duration_type      = get_post_meta( $course_id, '_lp_duration_select', true );
       			$duration_total     = $duration.' '.$duration_type;
       			$show_cat = '';

       			if(!empty($eshkool_option['off_course_cat'])) {
					$show_cat = '<span> '.$cats_show.'</span>';
				}


				

			    	echo '<div class="cource-item">
			                    <div class="cource-img">
			                        '.$post_img_url.'
                                    <a class="image-link" href="'.$post_url.'">
                                        <i class="fa fa-link"></i>
                                    </a>';

                                    if(!empty($eshkool_option['off_course_author'])): ?>
                                    <?php

                                    echo '<div class="course_author_img">';?>
                                     	<a class="rs-author" href="<?php echo esc_url( learn_press_user_profile_link( $course_author ) );?>"><?php echo get_avatar( $course_author, 35 ); ?>
                                     		
                                     	</a>
                                    <?php echo '</div>';
                                    	endif; 
                                    ?>

                                    <div class="price-inner">
			                        <?php if(!empty($eshkool_option['off_course_price'])):
			                        	learn_press_course_price();
			                        endif;



			                    echo '</div> </div>
			                    <div class="course-body">

			                    	'.$show_cat.' ';
			                    	?>

			                    	<?php

			                    	if(!empty($eshkool_option['off_course_title'])){

			                    	echo '<h4 class="course-title">
			                    		<a href="'.$post_url.'">'.$post_title.'	
									  	</a>
									</h4>';
									}
									?>



									<?php
			                    	echo '<div class="review-wrap">';?>
			                    		<?php 
			                    		if ( function_exists( 'learn_press_get_course_rate' ) ) : 

			                    		?>
				
				                    	</div>

				                    	<?php 

				                    	if(!empty($eshkool_option['off_course_review'])):
				                    		learn_press_course_review_template( 'rating-stars.php', array( 'rated' => $course_rate ) );
				                    	

				                    	?>


				                    	<span class="course-rating-total"> <?php echo esc_html( $course_rate_total );?> <?php echo esc_html( $course_rate_text );?></span>
				                    	
									<?php endif; endif; 
									if(!empty($eshkool_option['off_course_shortdes'])):

									?>
				                    	<div class="course-desc">
				                    		<?php the_excerpt();?>
				                    	</div>
			                   		<?php endif; ?>
			                    </div>
			                    <?php if(!empty($eshkool_option['off_course_enroll_count']) || !empty($eshkool_option['off_course_time']) ){?>

			                    <div class="course-footer">
			                    	      
			                    	<?php if(!empty($eshkool_option['off_course_time'])): ?>         	
				                    	<div class="course-time">
				                    		<span class="label"><?php echo esc_html__('Course Duration', 'rs-addons'); ?></span>
				                    		<span class="desc"><?php echo $duration_total; ?></span>
				                    	</div>
				                    <?php endif; ?>	
				                    <?php if(!empty($eshkool_option['off_course_enroll_count'])): ?>
				                    	<div class="course-student">
				                    		<span class="label"><?php echo esc_html__('Course Enrollment','rs-addons');?></span>
				                    		<span class="desc"><?php echo esc_html( $course_enroll_count ); ?></span>
				                    	</div>
			                    	<?php endif; ?>	
			                    </div>
			                	<?php } ?>


			                </div>
			<?php  endwhile; 
   		wp_reset_query();?>
	</div>
</div>
