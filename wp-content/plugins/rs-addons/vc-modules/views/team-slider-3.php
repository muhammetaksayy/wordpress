<div class="rs-team-<?php echo esc_attr($team_style);?>">
	<div class="team-carousel owl-carousel owl-navigation-yes" data-carousel-options="<?php echo esc_attr($owl_data);?>">
	    <?php
	     $args = array(
	        'role'    => LP_TEACHER_ROLE,        
	        'order'   => 'ASC',
	        'number'  => $team_per
	    );

	    $users = get_users( $args );	

	 	foreach ( $users as $user ) {
	        $id = $user->ID;
			$profile_link  = esc_url( learn_press_user_profile_link($id));
			$profile_image = get_avatar( $id, 360);			
			$designation   = get_the_author_meta( 'designation',  $id );
			$facebook      = get_the_author_meta('facebook', $id);		
			$twitter       = get_the_author_meta(  'twitter', $id );
			$google_plus   = get_the_author_meta( 'google', $id );
			$linkedin      = get_the_author_meta( 'linkedin', $id);
            

			$fb = ($facebook != '') ?'<a '.$social_color.' href="'.$facebook.'" class="social-icon"><i class="fa fa-facebook"></i></a>': '';
			
			$tw = ($twitter != '') ?'<a '.$social_color.' href="'.$twitter.'" class="social-icon"><i class="fa fa-twitter"></i></a>': '';
				
			
			$gp = ($google_plus != '') ? '<a '.$social_color.' href="'.$google_plus.'" class="social-icon"><i class="fa fa-google-plus"></i></a>' : '';
			
			
			$ldin= ($linkedin != '') ?'<a '.$social_color.' href="'.$linkedin.'" class="social-icon"><i class="fa fa-linkedin"></i></a>' : '';
			
			$team_normal_text = '<h3 class="team-name"><a href="'.$profile_link.'">'.$user->display_name.'</a></h3>
			   <span class="subtitle">'.$designation.'</span>';
			   ?>
			<div class="team-item">
				<div class="team-img">
				       <a href="<?php echo esc_attr($profile_link);?>"><?php echo wp_kses_post($profile_image);?></a>

				      
				</div>
			    <div class="team-content" <?php echo $bottom_color?> >
			    	 <?php if ($fb != '' || $tw != '' || $ldin != '' || $gp != '') {?>
		                  	<div class="team-social">			  
			                  	 <?php echo $fb;?>
			                  	  <?php echo $gp;?>
			                  	  <?php echo $tw;?>
			                  	  <?php echo $ldin;?>				                	 
		                  	</div>
		              <?php  }?>

	                <h3 class="team-name"><a <?php echo $name_color?> href="<?php echo $profile_link ;?>"><?php echo $user->display_name;?></a></h3>
	                <span class="team-title" <?php echo $designation_color?>><?php echo $designation;?></span>
			    </div>
			</div>
			<?php 
			}
		?>
	</div>
</div>