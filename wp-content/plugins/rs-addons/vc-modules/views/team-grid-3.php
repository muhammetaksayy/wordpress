<div class="rs-team-<?php echo $team_style; ?>">
	<div class="row">
		<?php 
	 	foreach ( $users as $user ) {
		        $id = $user->ID;
				$profile_link  = esc_url( learn_press_user_profile_link($id));
				$profile_image = get_avatar( $id, 360);			
				$designation   = get_the_author_meta( 'designation',  $id );
				$facebook      = get_the_author_meta('facebook', $id);		
				$twitter       = get_the_author_meta(  'twitter', $id );
				$google_plus   = get_the_author_meta( 'google', $id );
				$linkedin      = get_the_author_meta( 'linkedin', $id);	            

				$fb   = ($facebook != '') ?'<a href="'.$facebook.'" class="social-icon"><i class="fa fa-facebook"></i></a>': '';
				$tw   = ($twitter != '') ?'<a href="'.$twitter.'" class="social-icon"><i class="fa fa-twitter"></i></a>': '';		
				
				$gp   = ($google_plus != '') ? '<a href="'.$google_plus.'" class="social-icon"><i class="fa fa-google-plus"></i></a>' : '';				
				
				$ldin = ($linkedin != '') ?'<a href="'.$linkedin.'" class="social-icon"><i class="fa fa-linkedin">' : '';
				
				$team_normal_text = '<h3 class="team-name"><a href="'.$profile_link.'">'.$user->display_name.'</a></h3>
				   <span class="subtitle">'.$designation.'</span>';
				$output = '<div class="col-md-'.$team_col_grid.' col-sm-6"><div class="team-item">
					    <div class="team-img">
					       <a href="'.$profile_link.'">'.$profile_image.'</a>';

					       	if ($fb != '' || $tw != '' || $ldin != '' || $gp != '') {
			                  	$output .= '<div class="team-social">			  
			                  	  '.$fb.'
			                	  '.$gp.'
			                	  '.$tw.'
			                	  '.$ldin.'	
			                  	</div>';
			                }
					    $output .= '</div>
					    <div class="team-content">
			                <h3 class="team-name"><a href="'.$profile_link.'">'.$user->display_name.'</a></h3>
			                <span class="team-title">'.$designation.'</span>
					    </div>
					</div></div>';
				echo $output;
		}?>
	</div>
</div>