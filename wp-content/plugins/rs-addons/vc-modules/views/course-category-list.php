<div id="rs-courses-categories" class="rs-courses-categories style4-cat">	
	<div class="row">
		<?php

       if(!empty($cat)){
        $arr_cats=array();
        $arr= explode(',', $cat);  
			for ($i=0; $i < count($arr) ; $i++) { 
				$args = array(
				   'post_type' => 'lp_course',
				   'posts_per_page' => $course_per,
				   'tax_query' => array(
				        array(
				          'taxonomy' => 'course_category',
				          'field' => 'slug',
				          'terms' => $arr[$i],
				        )
				    )
				);
				if(!empty($arr[$i]))
				{
				$obj_name = new WP_Query($args);				
				wp_reset_query();				
				$term_name = get_term_by( 'slug', $arr[$i], 'course_category' );			
				$arr_name  = $term_name->name; 
				$term_link = get_term_link( $term_name );
				$term_id="";			
				$term_id= $term_name->term_id;
				 wp_reset_query();
				$icon= get_term_meta($term_id, 'category_icon', true);

				$icon_cat = (!empty($icon)) ? '<i class="'.$icon.'"></i>' : '<i class="fa fa-user"></i>';

	            echo '<div class="col-lg-3 col-md-6">
	        		<div class="courses-item">
	        	        '.$icon_cat.'
	        	        <h4 class="courses-title"><a href="'.$term_link.'">'.$arr_name.'</a></h4>	        	        
	        	    </div>
	        	</div>';
	        }  
	        }		

		} else{
			$args = array(
	            'taxonomy' => array('course_category'),//ur taxonomy
	            'hide_empty' => false,                  
	        );

			$terms_= new WP_Term_Query( $args );
			foreach ( (array)$terms_->terms as $term ) {
				$arr[] = $term->slug;
				$arr_name[]=$term->name;		
			} 
			for ($i=0; $i < count($arr) ; $i++) { 
					$args = array(
					   'post_type' => 'lp_course',
					   'tax_query' => array(
					        array(
					          'taxonomy' => 'course_category',
					          'field' => 'slug',
					          'terms' => $arr[$i],
					        )
					    )
					);
				$obj_name = new WP_Query($args);				
				$term_name = get_term_by('slug', $arr[$i], 'course_category');	

				$term_link = get_term_link( $term_name);
				$term_id="";			
				$term_id= $term_name->term_id;
				 wp_reset_query();
				$icon= get_term_meta($term_id, 'category_icon', true);

				$icon_cat = (!empty($icon)) ? '<i class="'.$icon.'"></i>' : '<i class="fa fa-user"></i>';

	            echo '<div class="col-lg-3 col-md-6">
	        		<div class="courses-item">
	        	        '.$icon_cat.'
	        	        <h4 class="courses-title"><a href="'.$term_link.'">'.$arr_name[$i].'</a></h4>	        	       
	        	    </div>
	        	</div>';
	        }  		
		}	
		?>	
   </div>
</div>
