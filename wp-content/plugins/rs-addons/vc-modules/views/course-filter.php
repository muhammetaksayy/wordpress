<?php 
global $eshkool_option;
	echo '<div class="rs-courses-2 '.$css_class_custom.' '.$col_filter.'">
		<div class="'.$css_class.'">';
        // Get taxonomy form portfolio
       
        if($show_filter !='No'):
		    echo '<div class="portfolio-filter  filter-'.strtolower($filter_align).'">
	                <button class="active" data-filter="*">'.$all_project.'</button>'; 
	                $taxonomy = "course_category";
	                $arr= explode(',', $cat);

					for ($i=0; $i < count($arr) ; $i++) { 
	               	 	$cats = get_term_by('slug', $arr[$i], $taxonomy);
		               	if(is_object($cats)):
		               	 	$slug= '.filter_'.$cats->slug;
		               	 	echo '<button data-filter="'.$slug.'">'.$cats->name.'</button>';	
		               	endif;
	                }			

	               	if ($read_btn == 'yes') {
       	        		echo '<div class="view-more">
       	            		<a '.$attributes.'>'.$rm_text.' <i class="fa fa-angle-double-right"></i></a>
       	            	</div>';
                    }
		            
		    echo '</div>'; 
		endif;
		
        echo '<div class="grid row">'; 
                
		        $cat;
		        $arr_cats=array();
		        $arr= explode(',', $cat);  

					for ($i=0; $i < count($arr) ; $i++) { 
		           	//$cats = get_term_by('slug', $arr[$i], $taxonomy);
		           	// if(is_object($cats)):
		           	$arr_cats[]= $arr[$i];
		           	//endif;
		        }  

				if(empty($cat)){
		        	$best_wp = new wp_Query(array(
						'post_type'           => 'lp_course',
						'posts_per_page'      => $course_per,
						'ignore_sticky_posts' => 1							
					));	 
				}		        
		        else{
		        	$best_wp = new wp_Query(array(
							'post_type'           => 'lp_course',
							'posts_per_page'      => $course_per,
							'ignore_sticky_posts' => 1,
							'tax_query' => array(
						        array(
									'taxonomy' => 'course_category',
									'field'    => 'slug', //can be set to ID
									'terms'    => $arr_cats//if field is ID you can reference by cat/term number
						        ),
						    )
					));	  
		        } 
       			if( $best_wp->have_posts() ): while( $best_wp->have_posts() ) : $best_wp->the_post();
					$termsArray = get_the_terms( $best_wp->ID, "course_category" );  //Get the terms for this particular item
					$termsString = ""; //initialize the string that will contain the terms
					foreach ( $termsArray as $term ) { // for each term 
					 	$termsString .= 'filter_'.$term->slug.' '; //create a string that has all the slugs 
					}
				
						$post_title     = get_the_title() ? get_the_title() : '';
						$post_img_url   = get_the_post_thumbnail_url($best_wp->ID);
						$post_url       = get_post_permalink($best_wp->ID);
						$cats_show      = get_the_term_list( $best_wp->ID, $taxonomy, ' ', '<span class="separator">, </span>');
						$excerpt        = get_the_excerpt();
						$course_id      = get_the_ID();
						$rstheme_course = LP()->global['course'];

		   			if ( empty( $rstheme_course ) ) return;
						
						$course_author       = get_post_field( 'post_author', $course_id );
						$course_enroll_count = $rstheme_course->get_users_enrolled();
						$course_enroll_count = $course_enroll_count ? $course_enroll_count : 0;	
		   		    
		   		    if ( function_exists( 'learn_press_get_course_rate' ) ) {
						$course_rate_res   = learn_press_get_course_rate( $course_id, false );
						$course_rate       = $course_rate_res['rated'];
						$course_rate_total = $course_rate_res['total'];
						$course_rate_text  = $course_rate_total > 1 ? esc_html__( 'Reviews', 'rs-addons' ) : esc_html__( 'Review', 'rs-addons' );
		   			}

		   			$duration   = get_post_meta( $course_id, '_lp_duration', true );
		   			$duration   = absint( $duration );
		   			$duration   = !empty( $duration ) ? $duration : false;

		   			$show_cat = '';
		   			if(!empty($eshkool_option['off_course_cat'])) {
						$show_cat = '<span> '.$cats_show.'</span>';
					}
						
					echo '
				
					<div class="col-md-'.$col_group.' '.$col_full.' col-sm-6 grid-item '.$termsString.'">
		                <div class="cource-item">
		                    <div class="cource-img">
	                         	<img src="'.$post_img_url.'" alt="'.$post_title.'"/>
                                <a class="image-link" href="'.$post_url.'">
                                    <i class="fa fa-link"></i>
                                </a>                             
		                    </div>
		                    <div class="course-body">


	                    	'.$show_cat.'';
		                    ?>
		                    <?php
			                    if(!empty($eshkool_option['off_course_title'])){

			                    	echo '<h4 class="course-title">
			                    	<a href="'.$post_url.'">'.$post_title.'	
										  	</a>
			                    	</h4>';
								}
							?>

							<?php

							if(!empty($eshkool_option['off_course_shortdes'])):
			                echo '<div class="course-desc">
	                    		<p>'.$excerpt.'</p>
	                    	</div>';
	                    		endif;
	                    	?>

							<?php
							if(!empty($eshkool_option['off_course_apply']) || !empty($eshkool_option['off_course_price']) ){
	                    	echo '<div class="course-value">'; ?>


		                    	<?php 
		                    	if(!empty($eshkool_option['off_course_apply'])):
		                    		echo '<a href="'.$post_url.'" class="cource-btn">'.$apply_text.'</a>'; 
		                    	endif;	
		                    	?>
		                    	<?php 
		                    	if(!empty($eshkool_option['off_course_price'])):
		                    		learn_press_course_price(); ?>
		                        	
		                        <?php
		                        endif;
		                         echo '</div>'; ?>

	                    <?php
		                        }
		                echo '</div>
		                </div>            
	                </div>';
					
					endwhile; 
				wp_reset_query();
			endif;
			
		echo '</div>
	</div>
</div>';		
 