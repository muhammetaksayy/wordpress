
	<?php global $eshkool_option; 
		echo '
		<div id="rs-events" class="rs-events event-list-items">';
		    	
			$cat;
	        $arr_cats=array();
	        $arr= explode(',', $cat);  

				for ($i=0; $i < count($arr) ; $i++) { 
	           	//$cats = get_term_by('slug', $arr[$i], $taxonomy);
	           	// if(is_object($cats)):
	           	$arr_cats[]= $arr[$i];
	           	//endif;
	        }  

			if(empty($cat)){
	        	$best_wp = new wp_Query(array(
						'post_type' => 'events',
						'posts_per_page' =>$team_per,
						
				));	  
	        }   
	        else{
	        	$best_wp = new wp_Query(array(
						'post_type' => 'events',
						'posts_per_page' =>$team_per,
						'tax_query' => array(
					        array(
					            'taxonomy' => 'event-category',
					            'field' => 'slug', //can be set to ID
					            'terms' => $arr_cats//if field is ID you can reference by cat/term number
					        ),
					    )
				));	  
	        }  

			while($best_wp->have_posts()): $best_wp->the_post();
			$start_time = get_post_meta( get_the_ID(), 'ev_start_time', true);	
			$start_date = get_post_meta( get_the_ID(), 'ev_start_date', true);	
			$end_time   = get_post_meta( get_the_ID(), 'ev_end_time', true);
			$ev_location   = get_post_meta( get_the_ID(), 'ev_location', true);			
			$event_color = get_post_meta(get_the_ID(), 'event_color', true);
			$event_color_main = ($event_color) ? 'style = "color: '.$event_color.'"': '';
			$event_bg    = ($event_color) ? 'style = "background: '.$event_color.'"': '';	

			$newDate = date("d/m/Y", strtotime($start_date));  
			$date_style = $eshkool_option['date_style'];
			if( 'style2' == $date_style ){
				$start_date = $newDate;
			}

			$time_style = $eshkool_option['time_style'];
			$new_stime  = date("H:i", strtotime($start_time));
			$new_etime  = date("H:i", strtotime($end_time));
			if( 'style2' == $time_style ){
				$start_time = $new_stime;
				$end_time   = $new_etime;
			}					
  
		  ?>
		   	<div class="event-item">
                <div class="events-details sec-color">
                	<?php if($start_date): ?>
                        <div class="event-date">
                            <i class="fa fa-calendar"></i>
                            <span><?php  
                            echo esc_html($start_date);?></span>
                        </div>
                    <?php endif; ?>
                    <h4 class="event-title"><a <?php echo $event_color_main;  ?> href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                    <div class="event-meta">
                        <div class="event-time">
                            <i class="fa fa-clock-o"></i>
                            <span><?php echo esc_attr($start_time);?>-<?php echo esc_attr($end_time);?></span>
                        </div>
                        <div class="event-location">
                            <i class="fa fa-map-marker"></i>
                            <span><?php echo esc_attr($ev_location);?></span>
                        </div>
                    </div>
                  
            	</div>
            </div>
		   <?php

		
		endwhile;
		wp_reset_query(); 
		?>


		<?php if($event_text): ?>
			<div class="button_box">
				<a class="readon" href="<?php echo esc_url($event_link); ?>">
					<?php echo esc_html($event_text); ?></a>
			</div>
		<?php endif; ?>

		<?php

	echo '</div></div>';
?>