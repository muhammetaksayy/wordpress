<?php
/**
 * @author  RS Theme
 * @since   1.0
 * @version 1.0
 */
?>
<div class="rs-search-courses <?php echo esc_attr($css_class);?>  <?php echo esc_attr($css_class_custom);?>">	
	<div class="rs-search">
	    <form method="get" action="<?php echo esc_url( get_post_type_archive_link( 'lp_course' ) ); ?>">
	    <input type="hidden" name="ref" value="course">
	        <input type="text" value="<?php echo esc_attr( get_search_query() );?>" name="s" placeholder="<?php esc_attr_e( 'Search our publishers', 'rs-addons' ) ?>" class="form-control" />
	   		<button type="submit"><?php echo esc_html__('Search publishers', 'rs-addons');?></button>	    
	    </form>
	</div>
</div>