<div id="rs-courses-categories" class="rs-courses-categories2 rs-courses-cat-slider">	
	<div class="row">
		<div id="slider_inner" class="owl-carousel">
		<?php

       if(!empty($cat)){
        $arr_cats=array();
        $arr= explode(',', $cat);  

			for ($i=0; $i < count($arr) ; $i++) { 
				$args = array(
				   'post_type' => 'lp_course',
				   'tax_query' => array(
				        array(
				          'taxonomy' => 'course_category',
				          'field' => 'slug',
				          'terms' => $arr[$i],
				        )
				    )
				);
				if(!empty($arr[$i]))
				{
				$obj_name = new WP_Query($args);
				$n = $obj_name->post_count; 
				wp_reset_query();
				$term_name = get_term_by( 'slug', $arr[$i], 'course_category' );			
				$arr_name  = $term_name->name; 
				$term_link = get_term_link( $term_name );
				$term_id="";

				$cat_bg_img="";	

				$term_id= $term_name->term_id;
				 wp_reset_query();


				$cat_img = get_term_meta($term_id, 'category_img', true);
				if(!empty($cat_img)){
					$cat_bg_img = 'style="background:url('.$cat_img.');background-size:cover";';
				}

				$icon= get_term_meta($term_id, 'category_icon', true);

				$icon_cat = (!empty($icon)) ? '<i class="'.$icon.'"></i>' : '<i class="fa fa-user"></i>';

	            echo '<div class="courses-item" '.$cat_bg_img.'>
	        	        '.$icon_cat.'
	        	        <h4 class="courses-title"><a href="'.$term_link.'">'.$arr_name.'</a></h4>
	        	        <span class="courses-amount">'.$n.' COURSES</span>
	        	    </div>';
	        }  
	        }		

		} else{
			$args = array(
	            'taxonomy' => array('course_category'),//ur taxonomy
	            'hide_empty' => false,                  
	        );

			$terms_= new WP_Term_Query( $args );
			foreach ( (array)$terms_->terms as $term ) {
				$arr[] = $term->slug;
				$arr_name[]=$term->name;		
			} 
			for ($i=0; $i < count($arr) ; $i++) { 
					$args = array(
					   'post_type' => 'lp_course',
					   'tax_query' => array(
					        array(
					          'taxonomy' => 'course_category',
					          'field' => 'slug',
					          'terms' => $arr[$i],
					        )
					    )
					);
				$obj_name = new WP_Query($args);
				$n= $obj_name->post_count; 
				$term_name = get_term_by('slug', $arr[$i], 'course_category');	

				$term_link = get_term_link( $term_name);
				$term_id="";			
				$term_id= $term_name->term_id;
				 wp_reset_query();
				$icon= get_term_meta($term_id, 'category_icon', true);

				$icon_cat = (!empty($icon)) ? '<i class="'.$icon.'"></i>' : '<i class="fa fa-user"></i>';
				$post_img_url = get_the_post_thumbnail_url();


				$category_img = get_post_meta(get_the_ID(), 'category_img', true);

				if($category_img){
					$cat_img_bg = 'background: url("'.$category_img.'")';
				}

				

	            echo '
	        		<div class="courses-item" '.$category_img.'>
	        	        '.$icon_cat.'
	        	        <h4 class="courses-title"><a href="'.$term_link.'">'.$arr_name[$i].'</a></h4>
	        	        <span class="courses-amount">'.$n.' COURSES</span>
	        	    </div>';
	        }  		
		}	
		?>	
   		</div>
   </div>
</div>
