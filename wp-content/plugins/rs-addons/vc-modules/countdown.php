<?php

	// Element Mapping
	 function vc_counter_mapping() {
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
         
    // Map the block with vc_map()
    vc_map( 
  
        array(
            'name'        => __('RS Counter', 'eshkool'),
            'base'        => 'vc_counter',
            'description' => __('Rs counter for project', 'eshkool'), 
            'category'    => __('by RS Theme', 'eshkool'),   
            'icon'        => get_template_directory_uri().'/framework/assets/img/vc-icon.png',            
            'params' => array(   
                array(
                    "type"       => "dropdown",
                    "heading"    => __("Counter Style", "eshkool"),
                    "param_name" => "style",
                    "value" => array(
                        __( 'Dark', 'eshkool')        => '',
                        __( 'Light', 'eshkool')       => 'style-light',
                        __( 'Image Style', 'eshkool') => 'image-style',
                    ),
                    "description" => __("Select your counter style here", "eshkool")
                ),
                array(
                    "type"       => "dropdown",
                    "heading"    => __("Border Style", "eshkool"),
                    "param_name" => "border_style",
                    "value" => array(
                        __( 'No', 'eshkool')   => '',
                        __( 'Yes', 'eshkool') => 'border-style',
                    ),
                    "description" => __("Select yes for border style", "eshkool")
                ), 
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Porject Title', 'eshkool'),
                    'param_name' => 'title',
                    'value' => __( '', 'eshkool'),
                    'description' => __( 'Project Title', 'eshkool'),
                    'admin_label' => false,
                    'weight' => 0,
                   
                ),				
				array(
                    'type' => 'textfield',
                    'heading' => __( 'Counter Number', 'eshkool'),
                    'param_name' => 'project',
                    'value' => __( '', 'eshkool'),
                    'description' => __( 'Project counter (Example: 100 only number)', 'eshkool'),
                    'admin_label' => false,
                    'weight' => 0,
                    
                ),

                array(
                    'type' => 'textfield',
                    'heading' => __( 'Counter Post Text', 'eshkool'),
                    'param_name' => 'post_text',
                    'value' => __( '', 'eshkool'),
                    'description' => __( 'Counter post text here. Example: %, $ etc', 'eshkool'),                    
                ),


                array(
                    "type"        => "attach_image",
                    "heading"     => __( "Counter Image", "eshkool" ),
                    "description" => __( "Add image", "eshkool" ),
                    "param_name"  => "screenshots",
                    "value"       => "",
                    "dependency" => Array('element' => 'style', 'value' => array('image-style')),
                ),

                array(
					'type' => 'iconpicker',
					'heading' => __( 'Counter Icon', 'eshkool'),
					'param_name' => 'icon_fontawesome',
					'value' => '', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
				
					'description' => __( 'Select icon from library.', 'eshkool'),                   
				),
                array(
                    "type" => "dropdown",
                    "heading" => __("Select Icon Align", "eshkool"),
                    "param_name" => "align",
                    "value" => array(
                        __( 'Center', 'eshkool') => '',
                        __( 'Left', 'eshkool')   => 'left',
                        __( 'Right', 'eshkool')  => 'right',
                    ),
                    "std" => 'style1',
                    "admin_label" => true,
                    "description" => __("", "eshkool")
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Counter Background Color", "eshkool" ),
                    "param_name" => "counter_bg",
                    "value" => '',
                    "description" => __( "Counter background here", "eshkool" ),
                    'group' => 'Styles',
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Icon Color", "eshkool" ),
                    "param_name" => "icon_color",
                    "value" => '',
                    "description" => __( "Choose icon color here", "eshkool" ),
                    'group' => 'Styles',
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Number Color", "eshkool" ),
                    "param_name" => "number_color",
                    "value" => '',
                    "description" => __( "Choose number color here", "eshkool" ),
                    'group' => 'Styles',
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Title Color", "eshkool" ),
                    "param_name" => "title_color",
                    "value" => '',
                    "description" => __( "Choose title color here", "eshkool" ),
                    'group' => 'Styles',
                ),
				array(
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'eshkool'),
					'param_name' => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool'										                            ),
				),        
                     
           		 )
        )
    );                                
        
}

 add_action( 'vc_before_init', 'vc_counter_mapping' );
 
// Element HTML
function vc_counter_html( $atts ) {
     
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'title'            => 'Counter Title',
                'style'            => '',
                'border_style'            => '',
                'project'          => '',
                'post_text'        => '',
                'align'            => '',
                'icon_fontawesome' =>'',
                'icon_color'       =>'',
                'icon_color'       =>'',
                'counter_bg'       =>'',
                'title_color'      =>'',
                'number_color'     =>'',
                'el_class'         =>'',
            ), 
            $atts,'vc_counter'
        )
    );
	
     //custom class added
	$wrapper_classes = array($el_class) ;			
	$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
	$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );

    $border_style = ($border_style) ? 'border-style' : '';
    $icon_color = ($icon_color) ? ' style="color: '.$icon_color.'"' : '';
    $title_color = ($title_color) ? ' style="color: '.$title_color.'"' : '';
    $counter_bg = ($counter_bg) ? ' style="background: '.$counter_bg.'"' : '';
    $number_color = ($number_color) ? ' style="color: '.$number_color.'"' : '';

    $post_text = ($post_text) ? '<span>'.$post_text.'</span>' : '';
    $count_icon = ($icon_fontawesome !== '') ? '<div class="count-icon"><i class="'.$icon_fontawesome.'"'.$icon_color.'></i></div>' : '';

    $a = shortcode_atts(array(
        'screenshots' => 'screenshots',
    ), $atts);
    $img = wp_get_attachment_image_src($a["screenshots"], "large");
    $imgSrc = $img[0];

    if(!empty($imgSrc)){
        $count_icon = ($imgSrc != '') ? '<div class="count-icon"><img src="'.$imgSrc.'"></div>' : '';
    }else{
        $count_icon = ($icon_fontawesome != '') ? '<div class="count-icon"><i class="'.$icon_fontawesome.'"'.$icon_color.'></i></div>' : '';
    }

    $html = '
	<div class="counter-top-area '.$align.''.$style.' '.$border_style.'" '.$counter_bg.'>
    <div class="rs-counter-list">
		'.$count_icon.'
        <div class="count-text"><div class="count-number"'.$number_color.'><h3 class="rs-counter"'.$number_color.'>' . $project. '</h3>'.$post_text.'</div>         
        <h4'.$title_color.'>'.$title.'</h4></div>
    </div></div>';
     
    return $html;
     
}
add_shortcode( 'vc_counter', 'vc_counter_html' );