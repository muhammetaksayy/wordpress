<?php
/**
 * @author  RSTheme
 * @since   1.0
 * @version 1.0
 */
 if ( !defined( 'WPB_VC_VERSION' ) ) {
        return;
}
if ( !class_exists( 'RS_Course_Search' ) ) {

	class RS_Course_Search extends RSTheme_VC_Modules {

		public function __construct(){
			$this->name = __( "RS Course Search", 'eshkool' );
			$this->base = 'rs-course-search';	
			 $fields = $this->fields();
                vc_map( 
                    array(
                        "name"     => $this->name,
                        "base"     => $this->base,
                        "class"    => "",
                        "icon"     => plugins_url( 'assets/vc-icon.png', dirname(__FILE__) ),
                        "controls" => "full",
                        "category" => __( 'by RS Theme', 'rs-option'),
                        "params"   => $fields,
                    )
                );    		
			parent::__construct();
		}

		public function fields(){
			$fields = array(
				array(
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'js_composer' ),
					'param_name' => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'bstart' ),
					),

				array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
		     
				),   
			);
			return $fields;
		}

		public function shortcode( $atts, $content = '' ){
			extract( shortcode_atts( array(				
				'el_class'         =>'',
				'css'              => ''
				), $atts ) );

			//extract css edit box
			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
			
			 //custom class added
			$wrapper_classes = array($el_class) ;			
			$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
			$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );

			$template = 'course-search';

			return $this->template( $template, get_defined_vars() );
		}
	}
}

new RS_Course_Search;