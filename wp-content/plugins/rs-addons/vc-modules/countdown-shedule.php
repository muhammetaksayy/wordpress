<?php

	// Element Mapping
	 function vc_countdown_mapping() {
         
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
    }
    $event_id = '';
    $args = array(
        'post_type'           => 'events',
        'posts_per_page'      => -1,
        'suppress_filters'    => false,
        'ignore_sticky_posts' => 1,
    );

    $posts_array = get_posts( $args );

    if( !empty( $posts_array ) ){

        foreach ( $posts_array as $post ) {
            $post_dropdown[$post->post_title] = $post->ID;                  
        }        
        $event_id      = $posts_array[0]->ID;
    }
    else {
        $post_dropdown = $event_id;
    }

    // Map the block with vc_map()
    vc_map(
  
        array(
            'name'        => __('Rs Courses Countdown', 'eventsia'),
            'base'        => 'vc_countdown',
            'description' => __('Rs countdown for courses', 'eventsia'), 
            'category'    => __('by RS Theme', 'eventsia'),   
            'icon'        => get_template_directory_uri().'/framework/assets/img/vc-icon.png',      
            'params'      => array(			

                array(
                    "type"       => "dropdown",
                    "holder"     => "div",
                    "class"      => "",
                    "heading"    => __( "Select An Courses", 'rsaddon' ),
                    "param_name" => "event_title",
                    'value'      => $post_dropdown,
                ),
                
                array(
                    "type"       => "textfield",
                    "class"      => "",
                    "heading"    => __( "Courses Custom Title", 'rsaddon' ),
                    "param_name" => "ev_custom_title",
                    'value'      => "", 
                ),

    

                array(
                    "type"       => "dropdown",
                    "heading"    => __("Content Align", 'rsaddon'),
                    "param_name" => "text_align",
                    "value"      => array(                           
                    'Center'     => "center",                     
                    'Left'       => "left",                     
                    'Right'      => "right",
                    ),                     
                ),

                array(
                    "type"       => "dropdown",
                    "heading"    => __("Show or Hide Courses Information", 'rsaddon'),
                    "param_name" => "on_off_information",
                    "value"      => array(                           
                    'On'         => "on",                     
                    'Off'        => "off",
                    ),                    
                ),

                array(
                    "type"       => "dropdown",
                    "heading"    => __("Show or Hide Courses Countdown", 'rsaddon'),
                    "param_name" => "on_off_countdown",
                    "value"      => array(                           
                    'On'         => "on",                     
                    'Off'        => "off",
                    ),                 
                ),

                array(
                    "type"        => "colorpicker",
                    "class"       => "",
                    "heading"     => __( "Courses Title Color", 'rsaddon' ),
                    "param_name"  => "ev_title_color",
                    "value"       => '',
                    "description" => __( "Choose title color", 'rsaddon' ),
                    'group'       => 'Courses Styles',
                ),

                array(
                    "type"        => "colorpicker",
                    "class"       => "",
                    "heading"     => __( "Courses Icon Color", 'rsaddon' ),
                    "param_name"  => "ev_meta_color",
                    "value"       => '',
                    "description" => __( "Choose Icon color", 'rsaddon' ),
                    'group'       => 'Courses Styles',
                    ),   

                array(
                    "type"        => "colorpicker",
                    "class"       => "",
                    "heading"     => __( "Courses Meta Text Color", 'rsaddon' ),
                    "param_name"  => "ev_meta_text_color",
                    "value"       => '',
                    "description" => __( "Choose Meta Text color", 'rsaddon' ),
                    'group'       => 'Courses Styles',
                    ), 


                
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => __( "Days Text", 'rsaddon' ),
                    "param_name"  => "days_text",
                    "value"       => 'Days',                   
                    'group'       => 'Days'                    
                ),


                array(
                        "type"        => "colorpicker",
                        "class"       => "",
                        "heading"     => __( "Circle Border Color", 'rsaddon' ),
                        "param_name"  => "ev_days_color",
                        "value"       => '',
                        "description" => __( "Choose Days color", 'rsaddon' ),
                        'group'       => 'Days',
                    ),


                array(
                        "type"        => "colorpicker",
                        "class"       => "",
                        "heading"     => __( "Courses Days Number Color", 'rsaddon' ),
                        "param_name"  => "ev_days_text_color",
                        "value"       => '',
                        "description" => __( "Choose Days Number color", 'rsaddon' ),
                        'group'       => 'Days',
                    ), 

                array(
                    "type"        => "colorpicker",
                    "class"       => "",
                    "heading"     => __( "Courses Days Text Color", 'rsaddon' ),
                    "param_name"  => "ev_days_num_color",
                    "value"       => '#e41f05',
                    "description" => __( "Choose Days Text color", 'rsaddon' ),
                    'group'       => 'Days',
                    ),

                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => __( "Hours Text", 'rsaddon' ),
                    "param_name"  => "hours_text",
                    "value"       => 'Hours',                   
                    'group'       => 'Hours'                    
                ),

                array(
                        "type"        => "colorpicker",
                        "class"       => "",
                        "heading"     => __( "Circle Border Color", 'rsaddon' ),
                        "param_name"  => "ev_hours_color",
                        "value"       => '',
                        "description" => __( "Choose Hours color", 'rsaddon' ),
                        'group'       => 'Hours',
                    ),

                array(
                        "type"        => "colorpicker",
                        "class"       => "",
                        "heading"     => __( "Courses Hours Number Color", 'rsaddon' ),
                        "param_name"  => "ev_hours_text_color",
                        "value"       => '',
                        "description" => __( "Choose Hours Number color", 'rsaddon' ),
                        'group'       => 'Hours',
                    ), 

                array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => __( "Courses Hours Text Color", 'rsaddon' ),
                        "param_name" => "ev_hours_num_color",
                        "value" => '#e41f05',
                        "description" => __( "Choose Hours Text color", 'rsaddon' ),
                        'group' => 'Hours',
                    ),

                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => __( "Minutes Text", 'rsaddon' ),
                    "param_name"  => "minutes_text",
                    "value"       => 'Minutes',                   
                    'group'       => 'Minutes'                    
                ),

        
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Circle Border Color", 'rsaddon' ),
                    "param_name" => "ev_min_color",
                    "value" => '',
                    "description" => __( "Choose Minutes color", 'rsaddon' ),
                    'group' => 'Minutes',
                ),

                array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => __( "Courses Minutes Number Color", 'rsaddon' ),
                        "param_name" => "ev_min_text_color",
                        "value" => '',
                        "description" => __( "Choose Minutes Number color", 'rsaddon' ),
                        'group' => 'Minutes',
                    ), 

                array(
                        "type" => "colorpicker",
                        "class" => "",
                        "heading" => __( "Courses Minutes Text Color", 'rsaddon' ),
                        "param_name" => "ev_min_num_color",
                        "value" => '',
                        "description" => __( "Choose Minutes Text color", 'rsaddon' ),
                        'group' => 'Minutes',
                    ),

                 array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => __( "Seconds Text", 'rsaddon' ),
                    "param_name"  => "seconds_text",
                    "value"       => 'Seconds',                   
                    'group'       => 'Seconds'                    
                ),

             

                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Circle Border Color", 'rsaddon' ),
                    "param_name" => "ev_sec_color",
                    "value" => '',
                    "description" => __( "Choose Seconds color", 'rsaddon' ),
                    'group' => 'Seconds',
                ),

                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Courses Seconds Number Color", 'rsaddon' ),
                    "param_name" => "ev_sec_text_color",
                    "value" => '',
                    "description" => __( "Choose Minutes Number color", 'rsaddon' ),
                    'group' => 'Seconds',
                ), 

                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Courses Seconds Text Color", 'rsaddon' ),
                    "param_name" => "ev_sec_num_color",
                    "value" => '',
                    "description" => __( "Choose Seconds Text color", 'rsaddon' ),
                    'group' => 'Seconds',
                ),  

				array(
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'eventsia'),
					'param_name' => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eventsia'										                            ),
				),        
                     
           	)
        )
    );                                
        
}

add_action( 'vc_before_init', 'vc_countdown_mapping' );

 
// Element HTML
function vc_countdown_html( $atts, $content ) {
     
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'event_title'                   => '',
                'ev_custom_title'               => '',
                'ev_title_color'                => '',
                'ev_meta_color'                 => '',
                'ev_days_text_color'            => '#fff',
                'ev_hours_text_color'           => '#fff',
                'ev_min_text_color'             => '#fff',
                'ev_sec_text_color'             => '#fff',
                'ev_days_num_color'             => '#e41f05',
                'ev_hours_num_color'            => '#e41f05',
                'ev_min_num_color'              => '#e41f05',
                'ev_sec_num_color'              => '#e41f05',
                'ev_meta_text_color'            => '',
                'ev_days_color'                 => '#e41f05',
                'ev_hours_color'                => '#e41f05',
                'ev_min_color'                  => '#e41f05',
                'ev_sec_color'                  => '#e41f05',
                'text_align'                    => 'center',
                'on_off_information'            => 'on',
                'on_off_countdown'              => 'on',
                'days_text'                     => 'Days',
                'hours_text'                    => 'Hours',
                'minutes_text'                  => 'Minutes',
                'seconds_text'                  => 'Seconds',
                'el_class'                      =>'',
            ), 
            $atts,'vc_counter'
        )
    );

    $event_localize_data = array(
        'ev_days_color'      => $ev_days_color, 
        'ev_hours_color'     => $ev_hours_color,
        'ev_min_color'       => $ev_min_color, 
        'ev_sec_color'       => $ev_sec_color, 
        'days_text'          => $days_text,
        'hours_text'         => $hours_text,
        'minutes_text'       => $minutes_text,
        'seconds_text'       => $seconds_text,
    );
    wp_localize_script( 'time-custom', 'events_data', $event_localize_data );
    wp_localize_script( 'time-circle', 'events_data', $event_localize_data );

    //custom color added
	$ev_title_color                = ($ev_title_color) ? ' style="color: '.$ev_title_color.'"' : '';
	$ev_meta_color                 = ($ev_meta_color) ? ' style="color: '.$ev_meta_color.'"' : '';
	$ev_meta_text_color            = ($ev_meta_text_color) ? ' style="color: '.$ev_meta_text_color.'"' : '';



	
    //custom class added
	$wrapper_classes  = array($el_class) ;			
	$class_to_filter  = implode( ' ', array_filter( $wrapper_classes ) );		
	$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );

    //query
    $best_wp = new wp_Query(array(
        'post_type' => 'events',
        'post__in' => array($event_title),
    ));


    while($best_wp->have_posts()): $best_wp->the_post();

	$event_title    = get_the_title();
	$event_date     = get_post_meta(get_the_ID(), 'ev_start_date', true);
	$event_end_date = get_post_meta(get_the_ID(), 'ev_end_date', true);
	$evt_location   = get_post_meta(get_the_ID(), 'ev_location', true);
	$event_time     = get_post_meta(get_the_ID(), 'ev_start_time', true);
    //setlocale(LC_ALL, 'pt_BR');   
	$event_schedule = $event_date.' '.$event_time; //"2018-10-31 00:00:00";
	$book_btn_link  = (!empty($book_btn_title)) ? '<a class="readon readon1" href="'.esc_url($book_btn_url).'">'.$book_btn_title.'</a>' : '';
        if(!empty($event_date) && !empty($event_end_date)){
            if($event_date!==$event_end_date){
                $day        = date("d",strtotime($event_date));
                //$end_day  = date("d F Y",strtotime($event_end_date));
                $day1       = date("d",strtotime($event_end_date));
                $month     = date("m",strtotime($event_end_date));
                $year      = date("Y",strtotime($event_end_date));
                $end_day    = strftime("%d %B %Y", mktime(0, 0, 0, $month, $day1, $year));
                $event_date = $day ."-". $end_day;
              
            }
        }
  
        
          if( 'on' !=  $on_off_information){

               $colfull = 'col-lg-12'; 
            } else{
               $colfull = 'col-lg-4 col-sm-12'; 
            }

             if( 'on' !=  $on_off_countdown ){            
               $colfull1 = 'col-lg-12'; 
            } else{
               $colfull1 = 'col-lg-8 col-sm-12'; 
            }
            

       

      

   
            $event_title_main = (!empty($ev_custom_title)) ? $ev_custom_title : $event_title;
            $eventinfo = '';
              if( 'on' ==  $on_off_information ){ 
                    $eventinfo = '
                    <div class="banner-content text-'.$text_align.'">
                        <h1 class="slider-title" data-animation-in="fadeInLeft" data-animation-out="animate-out">
                            <span '.$ev_title_color.'>
                                '.$event_title_main.' 
                            </span>
                        </h1>                                              
                        <div data-animation-in="fadeInUp" data-animation-out="animate-out" class="slider-desc">
                            <i class="icofont icofont-social-google-map" '.$ev_meta_color.'></i>
                            <i '.$ev_meta_text_color.'>'.$evt_location.'</i>
                        </div>
                        <div class="date-meta">
                            <span><i class="fa fa-calendar" aria-hidden="true" '.$ev_meta_color.'></i> <i '.$ev_meta_text_color.'>'.$event_date.'</i></span>
                        </div>

                    </div>';
              }

            $countdownschedule = '';
                if( 'on' ==  $on_off_countdown ){ 
                    $countdownschedule = '
                    <div class="banner-counter5">
                        <div class="timecounter-inner">
                            <div class="coming-soon-part2">
                                <div class="coming-soon-text">    
                                    <div data-animation-in="slideInLeft" data-animation-out="animate-out fadeOut" class="CountDownTimer3" data-date="'.$event_schedule.'"> </div>
                                </div>                                                        
                            </div>
                        </div>
                    </div>';
                }

            $html = '<div class="slide-content event_counter6">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                           '.$countdownschedule.'                 
                                           '.$eventinfo.'                                    
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                    $html .='
                        <style>
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Days span{
                                color:'.$ev_days_text_color.' !important;
                            }       
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Hours span{
                                color:'.$ev_hours_text_color.' !important;
                            }
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Minutes span{
                                color:'.$ev_min_text_color.' !important;
                            }
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Seconds span{
                                color:'.$ev_sec_text_color.' !important;
                            }


                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Days h4{
                                color:'.$ev_days_num_color.' !important;
                            }
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Hours h4{
                                color:'.$ev_hours_num_color.' !important;
                            }
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Minutes h4{
                                color:'.$ev_min_num_color.' !important;
                            }
                            .event_counter6 .coming-soon-part2 .coming-soon-text .time_circles div.textDiv_Seconds h4{
                                color:'.$ev_sec_num_color.' !important;
                            }
                          
                            
                        </style>';
             
            return $html;
    endwhile;
     
}
add_shortcode( 'vc_countdown', 'vc_countdown_html' );