<?php
/*
Element Description: Course Slider
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}     
    // Element Mapping
if ( !class_exists( 'RSTheme_VC_Course_Category' ) ) {    
         
       class RSTheme_VC_Course_Category extends RSTheme_VC_Modules {
		public function __construct(){
			$this->name = __( "Rs Course Category", 'rs-options' );
			$this->base = 'vc_coursecategory';				
			parent::__construct();
		}       
       
		public function fields(){

			$category_dropdown = array( __( 'All Categories', 'eshkool' ) => '0' );	
	        $args = array(
	            'taxonomy' => array('course_category'),//ur taxonomy
	            'hide_empty' => false,                  
	        );

			$terms_= new WP_Term_Query( $args );
			foreach ( (array)$terms_->terms as $term ) {
				$category_dropdown[$term->name] = $term->slug;		
			} 

        	$fields = array(                         
                     
					array(
						"type" => "dropdown_multi",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Categories", 'eshkool' ),
						"param_name" => "cat",
						'value' => $category_dropdown,
					),

					array(
						"type" => "dropdown",
						"heading" => __("Select Course Slider Style", "eshkool"),
						"param_name" => "cat_style",
						"value" => array(							
							'Style 1' => "style1", 
							'Style 2' => "style2",						
							'Style 3' => "style3",
							'Style 4' => "style4"							
						),
										
					),
					
					array(
                        "type" => "textfield",
                        "heading" => __("Category Per Pgae", "eshkool"),
                        "param_name" => "course_per",
                        'value' =>"6",
                        'description' => __( 'You can write how many course show. ex(2)', 'eshkool' ),                 
                    ),        
                   
                    
                    array(
						'type' => 'css_editor',
						'heading' => __( 'CSS box', 'eshkool' ),
						'param_name' => 'css',
						'group' => __( 'Design Options', 'eshkool' ),
					),     
         
        );
        return $fields;                                   
    }    
  
     
    // Element HTML
    public function shortcode( $atts, $content = '' ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(					
                    'course_per' => '6',                    
                    'cat_style' => 'style1',                    
                    'css'        => '' ,
                    'cat'        => '',           
                ), 
                $atts,'vc_coursecategory'
           )
        );	
        

		
		//extract css edit box
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );



	

        $dir = plugin_dir_path( __FILE__ );
        $template = 'course-category';


        switch ( $cat_style ) {

			case 'style2':
				$template = 'course-category-image';
				break;
			case 'style3':
				$template = 'course-category-slider';
				break;	
			case 'style4':
				$template = 'course-category-list';
				break;				

			default:
				$template = 'course-category';
				break;
		}



        return $this->template( $template, get_defined_vars() );
        }
	}
}

new RSTheme_VC_Course_Category;