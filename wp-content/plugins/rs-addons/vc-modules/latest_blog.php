<?php
/*
	Element Description: Rs Blog Box*/
    
    // Element Mapping
    function vc_latest_blog_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }  

        $categories = get_categories();
		$category_dropdown = array( 'All Categories' => '0' );

		foreach ( $categories as $category ) {
			$category_dropdown[$category->name] = $category->slug;
		}    
        // Map the block with vc_map()
        vc_map( 
            array(
				'name'        => __('Rs Latest Blog Grid', 'eshkool'),
				'base'        => 'vc_latest_blog',
				'description' => __('Latest blog grid layout', 'eshkool'), 
				'category'    => __('by RS Theme', 'eshkool'),   
				'icon'        => get_template_directory_uri().'/framework/assets/img/vc-icon.png',           
				'params'      => array(			
					array(
						"type"       => "dropdown_multi",
						"holder"     => "div",
						"class"      => "",
						"heading"    => __( "Categories", 'eshkool' ),
						"param_name" => "cat",
						'value'      => $category_dropdown,
					),
					array(
						"type"       => "dropdown",
						"heading"    => __("Show Featured Post", "eshkool"),
						"param_name" => "featured_show",
					    "value" => array(
							__( 'Yes', 'eshkool') => 'yes',
							__( 'Yes ( Style2 )', 'eshkool') => 'style2',
							__( 'No', 'eshkool')  => 'no',
							__( 'No ( Style2)', 'eshkool')  => 'style3',
					    ),
					),       
					array(
						"type" => "textfield",
						"heading" => __("Read More Text", 'eshkool'),
						"param_name" => "more_text",
						'description' => __( 'Type your read more text here', 'eshkool' ),
						"dependency" => Array('element' => 'readmore_type', 'value' => array('rm_text')),
					),
					array(
						"type" => "textfield",
						"heading" => __("Post Per page", 'rsaddon'),
						"param_name" => "post_per",
						'description' => __( 'Write -1 to show all', 'rsaddon' ),										
					),                        
                ),	
            )
        );                                   
    }
     
add_action( 'vc_before_init', 'vc_latest_blog_mapping' );    
 
     
function rs_latest_blog_html( $atts, $content ='' ) {
    $attributes = array();
    // Params extraction
    extract(
        shortcode_atts(
            array(
				'cat'              => '' ,
				'more_text'        => '',
				'featured_show'    => 'yes',
				'post_per'         => '3',	
            ), 
            $atts,'vc_latest_blog'
       )
    );
	
	$a      = shortcode_atts(array( 'screenshots' => 'screenshots',), $atts);
	$cat    = empty( $cat ) ? '' : $cat;
	$img    = wp_get_attachment_image_src($a["screenshots"], "large");
	$imgSrc = $img[0];
	
	//extract content
	// $atts['content'] = isset($content) ? $content : "";
 

	$post_title_show='';
	$degination='';
	$taxonomy='category';
    if( empty($cat) ){
    	$best_wp = new wp_Query(array(
				'posts_per_page' => 1,
				'order' => 'DESC',								
				'tax_query' => array(
				array(                
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => array( 
				'post-format-aside',
				'post-format-audio',
				'post-format-chat',
				'post-format-gallery',
				'post-format-image',
				'post-format-link',
				'post-format-quote',
				'post-format-status',
				'post-format-video'
				),
				'operator' => 'NOT IN'
				)
			)
		));	
    }
    else{
		$best_wp = new wp_Query(array(
			'posts_per_page' => 1,
			'order' => 'DESC',
			'category_name'       => $cat,
			'tax_query' => array(
				array(                
					'taxonomy' => 'post_format',
					'field' => 'slug',
					'terms' => array( 
						'post-format-aside',
						'post-format-audio',
						'post-format-chat',
						'post-format-gallery',
						'post-format-image',
						'post-format-link',
						'post-format-quote',
						'post-format-status',
						'post-format-video'
					),
					'operator' => 'NOT IN'
				)
			)
		));	
    }

    if( empty($cat) ){
    	$best_list_wp = new wp_Query(array(
				'posts_per_page' => 3,
				'offset' => 1,
				'order' => 'DESC',								
				'tax_query' => array(
					array(                
						'taxonomy' => 'post_format',
						'field' => 'slug',
						'terms' => array( 
							'post-format-aside',
							'post-format-audio',
							'post-format-chat',
							'post-format-gallery',
							'post-format-image',
							'post-format-link',
							'post-format-quote',
							'post-format-status',
							'post-format-video'
						),
						'operator' => 'NOT IN'
					)
				)
		));	
    }
    else{
		$best_list_wp = new wp_Query(array(
			'posts_per_page' => $post_per,
			'offset'         => 1,
			'order'          => 'DESC',
			'category_name'  => $cat,
			'tax_query' => array(
				array(                
					'taxonomy' => 'post_format',
					'field' => 'slug',
					'terms' => array( 
						'post-format-aside',
						'post-format-audio',
						'post-format-chat',
						'post-format-gallery',
						'post-format-image',
						'post-format-link',
						'post-format-quote',
						'post-format-status',
						'post-format-video'
					),
					'operator' => 'NOT IN'
				)
			)
		));	
    }

    if("yes"==$featured_show){
		$html ='
			<div class="rs-latest-news">
				<div class="row">
			        <div class="col-md-6">';

			        	while($best_wp->have_posts()): $best_wp->the_post();
			        	   
			        	   	$post_title_show = get_the_title($best_wp->ID);
			        				
			        	    $post_img = get_the_post_thumbnail($best_wp->ID, 'eshkool_latest_blog_big');
			        	    
			        	    $post_url = get_post_permalink($best_wp->ID); 
			        		
			        		if($degination!='No'){
			        	    $designation = get_post_meta( get_the_ID(), 'designation', true );
			        		}
			        	    
			        		$post_date = get_the_date();				
			        		$post_admin = get_the_author();
			        		$post_author_image = get_avatar(( $best_wp->ID ) , 70 ); 	
			        		$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 15) ); 
			        		$user_id='';
			        		$author_desigination=get_the_author_meta( 'position', $user_id );
			        		$comments_count=get_comments_number( '0', '1', '%' );	
			        		$categories = get_the_category();
			        		if ( ! empty( $categories ) ) {
			        						
			        		    $cat_name = esc_html( $categories[0]->name );
			        		    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
			        		}
			        		$read_more_btn = '';
			        		if( ! empty($more_text) ){
			        			$read_more_btn = '<div class="news-btn">
			                		<a href="'.$post_url.'">'.$more_text.'</a>
			                	</div>';
			        		}			

							$html .='<div class="news-normal-block">';

								if ($post_img) {
				                    $html .='<div class="news-img">
				                    	<a href="'.$post_url.'">
				                        	'.$post_img.'
				                    	</a>
				                    </div>';
				                }

			                	$html .='<div class="news-info">
			                	<div class="news-date">
			                		<i class="fa fa-calendar-check-o"></i>
			                		<span>'.$post_date.'</span>
			                	</div>

			                	<h4 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h4>

			                	<div class="news-desc">
			                		<p>
			                			'.$post_content.'
			                		</p>
			                	</div>

			                	'.$read_more_btn.'

		                	</div></div>';

		                endwhile; 
		                wp_reset_query();	

			        $html .='</div>
			        <div class="col-md-6">
			        	<div class="news-list-block">';

			        		while($best_list_wp->have_posts()): $best_list_wp->the_post();
			        		   $post_title= get_the_title($best_list_wp->ID);
			        		   
			        		   	$post_title_show= get_the_title($best_list_wp->ID);

			        		    $post_img = get_the_post_thumbnail($best_list_wp->ID, 'eshkool_latest_blog_small');

			        		    $post_url = get_post_permalink($best_list_wp->ID); 
			        			
			        			if($degination!='No'){
			        		    $designation = get_post_meta( get_the_ID(), 'designation', true );
			        			}
			        		    
			        			$post_date = get_the_date();				
			        			//$post_comment=get_wp_count_comments($best_list_wp->ID);
			        			$post_admin = get_the_author();
			        			$post_author_image=get_avatar(( $best_list_wp->ID ) , 70 ); 	
			        			$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 15) ); 
			        			$user_id='';
			        			$read_more_btn = '';
			        			$author_desigination = get_the_author_meta( 'position', $user_id );
			        			$comments_count = get_comments_number( '0', '1', '%' );	
			        			$categories = get_the_category();
			        			if ( ! empty( $categories ) ) {
			        							
			        			    $cat_name = esc_html( $categories[0]->name );
			        			    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
			        			}			

			        		$html .= '<div class="news-list-item">';

			        			if ($post_img) {
				                    $html .= '<div class="news-img">
				                    	<a href="'.$post_url.'">
				                        	'.$post_img.'
				                    	</a>
				                    </div>';
				                }

								$html .='<div class="news-content">
			                    	<h5 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h5>
			                    	<div class="news-date">
			                    		<i class="fa fa-calendar-check-o"></i>
			                    		<span>'.$post_date.'</span>
			                    	</div>
			                    	<div class="news-desc">
			                    		<p>
			                    			'.$post_content.'
			                    		</p>
			                    	</div>
				                </div>			        			
			        		</div>';

			        		endwhile; 
			        		wp_reset_query();


			        	$html .='</div>
			        </div>
			    </div>
		    </div>
		';
	}
	if("style2"==$featured_show){
		$html ='
			<div class="rs-latest-news2">
				<div class="row">
			        <div class="col-lg-6">';

			        	while($best_wp->have_posts()): $best_wp->the_post();
			        	   
			        	   	$post_title_show = get_the_title($best_wp->ID);
			        				
			        	    $post_img = get_the_post_thumbnail($best_wp->ID, 'eshkool_latest_blog_big2');
			        	    
			        	    $post_url = get_post_permalink($best_wp->ID); 
			        		
			        		if($degination!='No'){
			        	    $designation = get_post_meta( get_the_ID(), 'designation', true );
			        		}
			        	    
			        		$post_date = get_the_date();				
			        		$post_admin = get_the_author();
			        		$post_author_image = get_avatar(( $best_wp->ID ) , 70 ); 	
			        		$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 15) ); 
			        		$user_id='';
			        		$read_more_btn = '';
			        		$author_desigination=get_the_author_meta( 'position', $user_id );
			        		$comments_count=get_comments_number( '0', '1', '%' );	
			        		$categories = get_the_category();
			        		if ( ! empty( $categories ) ) {
			        						
			        		    $cat_name = esc_html( $categories[0]->name );
			        		    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
			        		}
			        		if( ! empty($more_text) ){
			        			$read_more_btn = '<div class="news-btn">
			                		<a href="'.$post_url.'">'.$more_text.'</a>
			                	</div>';
			        		}			

							$html .='<div class="news-normal-block">';

								if ($post_img) {
				                    $html .='<div class="news-img">
				                    	<a href="'.$post_url.'">
				                        	'.$post_img.'
				                    	</a>
				                    </div>';
				                }

			                	$html .='<div class="news-info">
			                	<div class="news-date">
			                		<span>'.$post_date.'</span>
			                	</div>

			                	<h4 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h4>

			                	<div class="news-desc">
			                		<p>
			                			'.$post_content.'
			                		</p>
			                	</div>

			                	'.$read_more_btn.'

		                	</div></div>';

		                endwhile; 
		                wp_reset_query();	

			        $html .='</div>
			        <div class="col-lg-6">
			        	<div class="row">';

			        		while($best_list_wp->have_posts()): $best_list_wp->the_post();
			        		   $post_title= get_the_title($best_list_wp->ID);
			        		   
			        		   	$post_title_show= get_the_title($best_list_wp->ID);

			        		    $post_img = get_the_post_thumbnail($best_list_wp->ID);

			        		    $post_url = get_post_permalink($best_list_wp->ID); 
			        			
			        			if($degination!='No'){
			        		    $designation = get_post_meta( get_the_ID(), 'designation', true );
			        			}
			        		    
			        			$post_date = get_the_date();				
			        			//$post_comment=get_wp_count_comments($best_list_wp->ID);
			        			$post_admin = get_the_author();
			        			$post_author_image=get_avatar(( $best_list_wp->ID ) , 70 ); 

			        			//$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 20) );
			        			//$post_content = get_the_excerpt();

			        			$trimexcerpt = get_the_content();
								$post_content2 = wp_trim_words( $trimexcerpt, $num_words = 12, $more = '… ' ); 

			        			$user_id='';
			        			$read_more_btn = '';
			        			$author_desigination = get_the_author_meta( 'position', $user_id );
			        			$comments_count = get_comments_number( '0', '1', '%' );	
			        			$categories = get_the_category();
			        			if ( ! empty( $categories ) ) {
			        							
			        			    $cat_name = esc_html( $categories[0]->name );
			        			    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
			        			}			

			        		$html .= '
						        <div class="col-md-6">
						        	<div class="news-list-block">
							        	<div class="news-list-item">';

					        			if ($post_img) {
						                    $html .= '<div class="news-img">
						                    	<a href="'.$post_url.'">
						                        	'.$post_img.'
						                    	</a>
						                    </div>';
						                }

										$html .='<div class="news-content">
											<div class="news-date">
					                    		<span>'.$post_date.'</span>
					                    	</div>
					                    	<h5 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h5>
					                    	
					                    	<div class="news-desc">
					                    		<p>
					                    			'.$post_content2.'
					                    		</p>
					                    	</div>
						                </div>			        			
					        		</div>
				        		</div>			        			
			        		</div>';

			        		endwhile; 
			        		wp_reset_query();

				        $html .='</div>
				    </div>
			    </div>
		    </div>
		';
	}
	if("no"==$featured_show){
		$html ='
			<div class="rs-latest-news">';	

			        $html .='
			        <div class="latest_wrap">
			        	<div class="news-list-block">';

			        		while($best_list_wp->have_posts()): $best_list_wp->the_post();
			        		   $post_title= get_the_title($best_list_wp->ID);
			        		   
			        		   	$post_title_show= get_the_title($best_list_wp->ID);

			        		    $post_img = get_the_post_thumbnail($best_list_wp->ID, 'eshkool_latest_blog_small');

			        		    $post_url = get_post_permalink($best_list_wp->ID); 
			        			
			        			if($degination!='No'){
			        		    $designation = get_post_meta( get_the_ID(), 'designation', true );
			        			}
			        		    
			        			$post_date = get_the_date();				
			        			//$post_comment=get_wp_count_comments($best_list_wp->ID);
			        			$post_admin = get_the_author();
			        			$post_author_image=get_avatar(( $best_list_wp->ID ) , 70 ); 	
			        			$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 15) ); 
			        			$user_id='';
			        			$author_desigination = get_the_author_meta( 'position', $user_id );
			        			$comments_count = get_comments_number( '0', '1', '%' );	
			        			$categories = get_the_category();
			        			if ( ! empty( $categories ) ) {
			        							
			        			    $cat_name = esc_html( $categories[0]->name );
			        			    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
			        			}		

			        		$html .= '<div class="news-list-item">';

			        			if ($post_img) {
				                    $html .= '<div class="news-img">
				                    	<a href="'.$post_url.'">
				                        	'.$post_img.'
				                    	</a>
				                    </div>';
				                }

								$html .='<div class="news-content">
			                    	<h5 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h5>
			                    	<div class="news-date">
			                    		<i class="fa fa-calendar-check-o"></i>
			                    		<span>'.$post_date.'</span>
			                    	</div>
			                    	<div class="news-desc">
			                    		<p>
			                    			'.$post_content.'
			                    		</p>
			                    	</div>
				                </div>			        			
			        		</div>';

			        		endwhile; 
			        		wp_reset_query();


			        $html .='</div>
			    </div>
		    </div>
		';
	}
	if("style3"==$featured_show){
		$html ='
			<div class="rs-latest-news-list2">';	
		        $html .='
		        <div class="latest_wrap">
		        	<div class="news-list-block">';

		        		while($best_list_wp->have_posts()): $best_list_wp->the_post();
		        		   $post_title= get_the_title($best_list_wp->ID);
		        		   
		        		   	$post_title_show= get_the_title($best_list_wp->ID);

		        		    $post_img = get_the_post_thumbnail($best_list_wp->ID, 'eshkool_latest_blog_small2');

		        		    $post_url = get_post_permalink($best_list_wp->ID); 
		        			
		        			if($degination!='No'){
		        		    $designation = get_post_meta( get_the_ID(), 'designation', true );
		        			}
		        		    
		        			$post_date = get_the_date();				
		        			//$post_comment=get_wp_count_comments($best_list_wp->ID);
		        			$post_admin = get_the_author();
		        			$post_author_image=get_avatar(( $best_list_wp->ID ) , 70 ); 	
		        			$post_content = eshkool_blog_get_excerpt( $defaults = array( 'length' => 15) ); 
		        			$user_id='';
		        			$author_desigination = get_the_author_meta( 'position', $user_id );
		        			$comments_count = get_comments_number( '0', '1', '%' );	
		        			$categories = get_the_category();
		        			if ( ! empty( $categories ) ) {
		        							
		        			    $cat_name = esc_html( $categories[0]->name );
		        			    $link= esc_url( get_category_link( $categories[0]->term_id ) ) ;
		        			}	

		        			$read_more_btn = '';
			        		if( ! empty($more_text) ){
			        			$read_more_btn = '<div class="news-btn">
			                		<a href="'.$post_url.'">'.$more_text.'</a>
			                	</div>';
			        		}		

		        		$html .= '<div class="news-list-item">';

		        			if ($post_img) {
			                    $html .= '<div class="news-img">
			                    	<a href="'.$post_url.'">
			                        	'.$post_img.'
			                    	</a>
			                    </div>';
			                }

							$html .='<div class="news-content">
								<div class="news-date">
		                    		<span>'.$post_date.'</span>
		                    	</div>
		                    	<h5 class="news-title"><a href="'.$post_url.'">'.$post_title_show.'</a></h5>
		         
		                    	'.$read_more_btn.'
			                </div>			        			
		        		</div>';

		        		endwhile; 
		        		wp_reset_query();
		        $html .='</div>
			    </div>
		    </div>
		';
	}
			  
	return $html;

}


add_shortcode( 'vc_latest_blog', 'rs_latest_blog_html' );