<?php
/*
Element Description: RS Course Filter
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}   
     if ( !class_exists( 'RSTheme_VC_Course_Filter' ) ) {    
              
            class RSTheme_VC_Course_Filter extends RSTheme_VC_Modules {
     		public function __construct(){
     			$this->name = __( "Rs Course Filter", 'rs-options' );
     			$this->base = 'rs_course_filter';				
     			parent::__construct();
     		}       
            
     		public function fields(){

     			$category_dropdown = array( __( 'All Categories', 'eshkool' ) => '0' );	
     	        $args = array(
     	            'taxonomy' => array('course_category'),//ur taxonomy
     	            'hide_empty' => false,                  
     	        );

     			$terms_= new WP_Term_Query( $args );

     			foreach ( (array)$terms_->terms as $term ) {
     				$category_dropdown[$term->name] = $term->slug;		
     			} 

             	$fields = array(    

                    array(
						"type" => "dropdown_multi",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Categories", 'eshkool' ),
						"param_name" => "cat",
						'value' => $category_dropdown,
					),

					array(
						"type" => "dropdown",
						"heading" => __("Show Filter", "eshkool"),
						"param_name" => "show_filter",
						"value" => array(	
						    						
							'Yes' => "Yes", 
							'No' => "No", 																																															
						),
					),

					array(
						"type" => "dropdown",
						"heading" => __("Filter Alignment", "eshkool"),
						"param_name" => "filter_align",
						"value" => array(
                            'Center' => "Center",		
							'Left' 	 => "Left", 
							'Right'	 => "Right", 
																					
						),
						"dependency" => Array('element' => 'show_filter', 'value' => array('Yes')),						
					),

					array(
						'type' => 'textfield',
						'holder' => 'h3',						
						'heading' => __( 'Filter Default Title', 'eshkool' ),
						'param_name' => 'filter_title',
						'value' => __( '', 'eshkool' ),
						'description' => __( 'You can add here filter default title (ex: All Projects)' ),
						'admin_label' => false,
						'weight' => 0,
						'value' => 'All Projects',
						"dependency" => Array('element' => 'show_filter', 'value' => array('Yes')),
					   
					),

					array(
						'type' => 'textfield',
						'holder' => 'h3',						
						'heading' => __( 'Course Per Page', 'eshkool' ),
						'param_name' => 'course_per',
						'value' => __( '', 'eshkool' ),
						'description' => __( 'How many course want to show per page', 'eshkool' ),
						'admin_label' => false,
						'weight' => 0,
						'value' => '9'
					   
					),  
					array(
						"type" => "dropdown",
						"heading" => __("How many Column ", "eshkool"),
						"param_name" => "column",
						"value" => array(							    						
							
							'Two' => "Two",
							'Four' => "Four", 
							'Three' => "Three",
							'Full' => "Full",
						),
						"std" => "Three"
						
					),
					array(
						"type"       => "dropdown",
						"heading"    => __("Show Read More", "eshkool"),
						"param_name" => "read_btn",
					    "value" => array(
							__( 'No', 'eshkool')  => '',
							__( 'Yes', 'eshkool') => 'yes',
					    ),
					),
					array(
						'type'        => 'textfield',
						'heading'     => __( 'Read More Text', 'eshkool'),
						'param_name'  => 'rm_text',
						'value'       => __( '', 'eshkool'),
						'description' => __( 'Your read more text here', 'eshkool'),
						"dependency"  => Array('element' => 'read_btn', 'value' => array('yes')),
					),
					array(
						'type'        => 'vc_link',
						'heading'     => __( 'Read More URL (Link)', 'eshkool'),
						'param_name'  => 'read_more_link',
						'description' => __( 'Add link read more.', 'eshkool'),
						'dependency'  => array('element' => 'read_btn', 'value' => array('yes')),
					),
                    array(
						"type" => "textfield",
						"heading" => __("Apply Now Text", "eshkool"),
						"param_name" => "apply_text",
						'value' =>"",
						'description' => __( 'Apply now text here.', 'eshkool' ),					
					),	
				  	array(
						'type' => 'textfield',
						'heading' => __( 'Extra class name', 'js_composer' ),
						'param_name' => 'el_class',
						'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool' ),
					),
					
					array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
				),            
                        
             
            );
            return $fields;                                   
        }   
 
     
    // Element HTML
     public function shortcode( $atts, $content = '' ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'column'         =>'Three',
					'course_per'     => '9',
					'el_class'       => '',
					'css'            => '',
					'cat'            => '',
					'apply_text'     => 'Apply Now',
					'read_btn'       => '',
					'rm_text'        => '',
					'read_more_link' => '',
					'filter_align'   => 'center',
					'show_filter'    => '',
					'filter_title'   =>  'All Projects'
					
                ), 
                $atts,'rs_course_filter'
           )
        );


        //parse link for vc linke		
		$read_more_link = ( '||' === $read_more_link ) ? '' : $read_more_link;
		$read_more_link = vc_build_link( $read_more_link );
		$use_link = false;
		if ( strlen( $read_more_link['url'] ) > 0 ) {
			$use_link = true;
			$a_href = $read_more_link['url'];
			$a_title = $read_more_link['title'];
			$a_target = $read_more_link['target'];
		}
		
		if ( $use_link ) {
			$attributes[] = 'href="' . esc_url( trim( $a_href ) ) . '"';
			$attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
			if ( ! empty( $a_target ) ) {
				$attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
			}
		}
		$attributes = implode( ' ', $attributes );
		

        

        $taxonomy = '';
		
		//extract content
		$atts['content'] = $content;
		//extact icon 		
		//extract css edit box
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
		
		 //custom class added
		$wrapper_classes = array($el_class) ;			
		$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
		$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );
  
		
		$col_grid='';
		$col_group='';
		$col_full='';
		$col_filter='';
		$col_grid=$column;
		
		if($col_grid =='Two'){
			$col_group = 6;
		}
		if($col_grid =='Three'){
			$col_group = 4;
		}
		
		if($col_grid == 'Four'){
			$col_group = 3;
		}
		
		if($col_grid == 'Full'){
			$col_group=3;
			$col_full='full-grid';		
			$col_filter='col-filter';		
		}
		
        //******************//
        // query post
        //******************//
        $all_project = $filter_title;


        
   $dir = plugin_dir_path( __FILE__ );
         $template = 'course-filter';

         return $this->template( $template, get_defined_vars() );

         }
 	}
 }

 new RSTheme_VC_Course_Filter;