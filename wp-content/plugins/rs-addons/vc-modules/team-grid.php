<?php
/*
Element Description: Rs Team Box
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}     
    // Element Mapping
    if ( !class_exists( 'RSTheme_VC_Team_Grid' ) ) {    
         
       class RSTheme_VC_Team_Grid extends RSTheme_VC_Modules {
		public function __construct(){
			$this->name = __( "Rs Instructor Grid", 'rs-options' );
			$this->base = 'vc_teamgrid';				
			parent::__construct();
		}     
       
		public function fields(){
			$fields = array(
					array(
						"type" => "textfield",
						"heading" => __("Team Per Pgae", "eshkool"),
						"param_name" => "team_per",
						'value' =>"6",
						'description' => __( 'You can write how many team member show. ex(2)', 'eshkool' ),					
					),
					

					array(
						"type" => "dropdown",
						"heading" => __("Select Team Grid", "eshkool"),
						"param_name" => "team_col",
						"value" => array(							
							'2 Column' => "2 Column", 
							'3 Column' => "3 Column",
							'4 Column' => "4 Column",
							'Full Width' => "Full Width"																	
						),
											
					),	

					array(
						"type" => "dropdown",
						"heading" => __("Select Team Style", "eshkool"),
						"param_name" => "team_style",
						"value" => array(							
							'Style 1' => "style1", 
							'Style 2' => "style2",														
							'Style 3' => "style3",	
							'Style 4' => "style4",																										
						),
											
					),
										 
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Team Icon', 'eshkool' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-users', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false,
							// default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000,
							// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					
						'description' => __( 'Select icon from library.', 'eshkool' ),
					),
					array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
			     
				),     
		); 
        return $fields;                                   
    }    
     

     
    // Element HTML
     public function shortcode( $atts, $content = '' ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'title'                 => '',
					'degination'            => '',		
					'description'           => '',	
					'team_per'              => '6',				
					'icon_fontawesome'      => 'fa fa-users',						
					'team_col'              => '2 Column',
					'team_style'            => 'style1',									
					'css'                   => '' ,
					'cat'					=> '',           
                ), 
                $atts,'vc_teamgrid'
           		)
        	);

	         $a = shortcode_atts(array(
	            'screenshots' => 'screenshots',
	        ), $atts);

	        $img = wp_get_attachment_image_src($a["screenshots"], "large");
	        $imgSrc = $img[0];		
			

			//extact icon 
			$iconClass = isset( ${'icon_fontawesome'} ) ? esc_attr( ${'icon_fontawesome'} ) : 'fa fa-users';
			//extract css edit box
			$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts ); 

			$team_col_grid ='';		
			//echo $team_col;
	        if($team_col == '2 Column'){
	        	$team_col_grid = 6;
	        }
	         if($team_col == '3 Column'){
	        	$team_col_grid = 4;
	        }
	         if($team_col == '4 Column'){
	        	$team_col_grid = 3;
	        }
	         if($team_col == 'Full Width'){
	        	$team_col_grid = 12;
	        }



		   	$post_title_show='';
			$degination='';
			$description_team='';
       
	    
		    $args = array(
		        'role'    => LP_TEACHER_ROLE,        
		        'order'   => 'ASC',
		        'number'  => $team_per
		    );
		    $users = get_users( $args );

	   


        	switch ( $team_style ) {
        		case 'style4':
					$template = 'team-grid-4';
					break;
				case 'style2':
					$template = 'team-grid-2';
					break;
				case 'style3':
					$template = 'team-grid-3';
					break;
				default:
					$template = 'team-grid-1';
					break;
			}
			return $this->template( $template, get_defined_vars() );
		}
	}
}

new RSTheme_VC_Team_Grid;