<?php
/*
Element Description: Rs Gallery elements
*/
// Element Mapping

function rs_gallery_mapping() {
	 
	// Stop all if VC is not enabled
	if ( !defined( 'WPB_VC_VERSION' ) ) {
		return;
	}

    $category_dropdown = array( __( 'All Categories', 'eshkool' ) => '0' );	
    $args = array(
        'taxonomy' => array('portfolio-category'),//ur taxonomy
        'hide_empty' => false,                  
    );

	$terms_= new WP_Term_Query( $args );
	foreach ( (array)$terms_->terms as $term ) {
		$category_dropdown[$term->name] = $term->slug;		
	}
	 
	// Map the block with vc_map()
	vc_map( 
		array(
			'name' => __('Rs Gallery Module', 'eshkool'),
			'base' => 'rs_gallery',
			'description' => __('Rs Gallery', 'eshkool'), 
			'category' => __('by RS Theme', 'eshkool'),   
			'icon' => get_template_directory_uri().'/framework/assets/img/vc-icon.png',    
			'params' => array(
                array(
					"type" => "dropdown_multi",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Categories", 'eshkool' ),
					"param_name" => "cat",
					'value' => $category_dropdown,
				),

				array(
					"type" => "dropdown",
					"heading" => __("Show Filter", "eshkool"),
					"param_name" => "show_filter",
					"value" => array(					    						
						'Yes' => "Yes", 
						'No' => "No",																
					),
				),
				array(
					"type" => "dropdown",
					"heading" => __("Filter Alignment", "eshkool"),
					"param_name" => "filter_align",
					"value" => array(
                        'Center' => "Center",		
						'Left' 	 => "Left", 
						'Right'	 => "Right", 
																				
					),
					"dependency" => Array('element' => 'show_filter', 'value' => array('Yes')),						
				),

                array(
					"type" => "dropdown",
					"heading" => __("Gutter Gap", "eshkool"),
					"param_name" => "gutter",
					"value" => array(							    						
						'Yes' => "", 
						'No'  => "no",
					),						
				),

				array(
					"type" => "dropdown",
					"heading" => __("Show Zoom Icon", "eshkool"),
					"param_name" => "show_zoom",
					"value" => array(							    						
						'No'  => "",
						'Yes' => "yes", 
					),						
				),

				array(
					'type' => 'textfield',
					'holder' => 'h3',						
					'heading' => __( 'Filter Default Title', 'eshkool' ),
					'param_name' => 'filter_title',
					'value' => __( '', 'eshkool' ),
					'description' => __( 'You can add here filter default title (ex: All Projects)' ),
					'admin_label' => false,
					'weight' => 0,
					'value' => 'All',
					"dependency" => Array('element' => 'show_filter', 'value' => array('Yes')),
				   
				),
				
				array(
					'type' => 'textfield',					
					'heading' => __( 'Gallery Title', 'eshkool' ),
					'param_name' => 'title',
					'value' => __( '', 'eshkool' ),
					'description' => __( 'Enter title here', 'eshkool' ),			   
				),
				array(
					'type' => 'textfield',					
					'heading' => __( 'Gallery Sub Title', 'eshkool' ),
					'param_name' => 'sub_title',
					'value' => __( '', 'eshkool' ),
					'description' => __( 'Enter sub title here', 'eshkool' ),			   
				),
				array(
					'type' => 'textfield',
					'holder' => 'h3',						
					'heading' => __( 'Photo Per Page', 'eshkool' ),
					'param_name' => 'per_page',
					'value' => __( '', 'eshkool' ),
					'description' => __( 'How many photo want to show per page', 'eshkool' ),
					'admin_label' => false,
					'weight' => 0,
					'value' => '8'				   
				),
				array(
					"type" => "dropdown",
					"heading" => __("How many Column ", "eshkool"),
					"param_name" => "column",
					"value" => array(
						'Two' => "Two",
						'Three' => "Three",
						'Four' => "Four", 
					),
					'std' => "Three", 
				), 
				array(
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'eshkool' ),
					'param_name' => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool' ),
				),
				array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
				),           
		 )
		)
	);                                
	
}
     
 add_action( 'vc_before_init', 'rs_gallery_mapping' );
     
    // Element HTML
   function rs_gallery_html( $atts,$content ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'per_page'      => '8',
					'column'        =>'Three',
					'cat'           => '',
					'title'         => '',
					'sub_title'     => '',
					'el_class'      => '',
					'cat'           => '',
					'filter_align'  => 'center',
					'gutter'        => '',
					'show_filter'   => '',
					'show_category' => 'yes',
					'filter_title'  =>  'All',
					'show_zoom'     =>  '',
					'css'           => ''
                ), 
                $atts,'rs_gallery'
           )
        );
		
		$gutter_gap = '';
		$gutter_gap = ($gutter !== '') ? ' no-gutters' : '';

		$taxonomy = '';

		$col_grid='';
		$col_group='';
		$col_full='';
		$col_filter='';
		$col_grid=$column;
		
		if($col_grid =='Two'){
			$col_group = 6;
		}
		if($col_grid =='Three'){
			$col_group = 4;
		}
		
		if($col_grid == 'Four'){
			$col_group = 3;
		}
		
		if($col_grid == 'Full'){
			$col_group=3;
			$col_full='full-grid';		
			$col_filter='col-filter';		
		}
	  
	   //custom class added
		$wrapper_classes = array($el_class) ;			
		$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
		$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );
		
	
		//extract css edit box
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );

		$all_project = $filter_title;	   
	
		
		$html = '<div class="rs-gallery '.$css_class.' '.$css_class_custom.'">';

		        if($show_filter !='No'):
				    $html .= '<div class="portfolio-filter  filter-'.strtolower($filter_align).'">
				                <button class="active" data-filter="*">'.$all_project.'</button>'; 
				                $taxonomy = "portfolio-category";
				                $arr= explode(',', $cat);

								for ($i=0; $i < count($arr) ; $i++) { 
				               	 $cats = get_term_by('slug', $arr[$i], $taxonomy);

				               	 if(is_object($cats)):
				               	 	$slug= '.filter_'.$cats->slug;

				               	 	$html .= '<button data-filter="'.$slug.'">'.$cats->name.'</button>';	
				               	 endif;
				               }			

				            
				    $html .=' </div>'; 
				endif; 

				if ($title !== '' || $sub_title !== '') {
					$html .= '<div class="gallery-desc">';

						if ($title) {
		                	$html .= '<h2 class="title">'.$title.'</h2>';
						}

						if ($sub_title) {
			                $html .= '<h3 class="sub-title">'.$sub_title.'</h3>';
			            }

		            $html .= '</div>';
				}

				$html .= '<div class="grid row'.$gutter_gap.'">';

			        $arr_cats=array();
			        $arr= explode(',', $cat);
						for ($i=0; $i < count($arr) ; $i++) { 
			           	//$cats = get_term_by('slug', $arr[$i], $taxonomy);
			           	// if(is_object($cats)):
			           	$arr_cats[]= $arr[$i];
			           	//endif;
			        } 
				   	$best_wp = new wp_Query(array(
						'post_type' => 'portfolios',
						'posts_per_page' =>$per_page,
						'tax_query' => array(
					        array(
					            'taxonomy' => 'portfolio-category',
					            'field' => 'slug', //can be set to ID
					            'terms' => $arr_cats//if field is ID you can reference by cat/term number
					        ),
					    )
					));	

					while($best_wp->have_posts()): $best_wp->the_post();

						$termsArray = get_the_terms( $best_wp->ID, "portfolio-category" );  //Get the terms for this particular item
						$termsString = ""; //initialize the string that will contain the terms
						foreach ( $termsArray as $term ) { // for each term 
							$termsString .= 'filter_'.$term->slug.' '; //create a string that has all the slugs 
						}

					   $post_title= get_the_title($best_wp->ID);					
					   $post_img_url = get_the_post_thumbnail_url($best_wp->ID,'full');

					   $show_zoom_yes = ($show_zoom != '') ? ' show-zoom-yes' : '';
							
						$html .='
							<div class="col-md-'.$col_group.' col-sm-6 mb-30 grid-item '.$termsString.'">
								<div class="gallery-item'.$show_zoom_yes.'">';

									if ($show_zoom != '') {
										
										$html .='<a class="image-popup zoom-icon" href="'.$post_img_url.'" title="'.$post_title.'">
			            						<i class="fa fa-search"></i>
			            					</a>';
									}


									$html .='<a class="image-popup img-wrap" href="'.$post_img_url.'">
										<img src="'.$post_img_url.'" alt="'.$post_title.'">
									</a>
								</div>
							</div>';
					endwhile; 
					wp_reset_query();
		$html .='</div></div>';
 return $html; 
}
add_shortcode( 'rs_gallery', 'rs_gallery_html' );