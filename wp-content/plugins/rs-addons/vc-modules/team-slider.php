<?php
/*
Element Description: Rs Team Box
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}     
// Element Mapping
if ( !class_exists( 'RSTheme_VC_Team_Slider' ) ) {    
         
    class RSTheme_VC_Team_Slider extends RSTheme_VC_Modules {
		public function __construct(){
			$this->name = __( "Rs Instructor Slider", 'eshkool' );
			$this->base = 'rs_team_slider';				
			parent::__construct();
		}     
       
		public function fields(){
			$fields = array(
				array(
					"type" => "textfield",
					"heading" => __("Team Per Pgae", "eshkool"),
					"param_name" => "team_per",
					'value' =>"6",
					'description' => __( 'You can write how many team member show. ex(2)', 'eshkool' ),					
				),

				array(
					"type" => "dropdown",
					"heading" => __("Select Team Style", "eshkool"),
					"param_name" => "team_style",
					"value" => array(							
						'Style 1' => "style1", 
						'Style 2' => "style2",
						'Style 3' => "style3",
						'Style 4' => "style4",
						'Style 5' => "style5",
						'Style 6' => "style6",
					),
										
				),
									 
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Number of columns ( Desktops > 1199px )", 'eshkool' ),
					"param_name" => "col_lg",
					"value" => array(							
						'1' => "1", 
						'2' => "2",
						'3' => "3",	
						'4' => "4",
						'5' => "5",
						'6' => "6",																						
					),
					"std" => "3",
					"group" 	  => __( "Slider Options", 'eshkool' ),
				
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Number of columns ( Desktops > 991px )", 'eshkool' ),
					"param_name" => "col_md",
					"value" => array(							
								'1' => "1", 
								'2' => "2",
								'3' => "3",	
								'4' => "4",
								'5' => "5",
								'6' => "6",																						
							),
					"std" => "3",
					"group" 	  => __( "Slider Options", 'eshkool' ),
				
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Number of columns ( Tablets > 767px )", 'eshkool' ),
					"param_name" => "col_sm",
					"value" => array(							
								'1' => "1", 
								'2' => "2",
								'3' => "3",	
								'4' => "4",
								'5' => "5",
								'6' => "6",																						
							),
					"std" => "3",
					"group" 	  => __( "Slider Options", 'eshkool' ),
					
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Number of columns ( Phones < 768px )", 'eshkool' ),
					"param_name" => "col_xs",
					"value" => array(							
								'1' => "1", 
								'2' => "2",
								'3' => "3",	
								'4' => "4",
								'5' => "5",
								'6' => "6",																						
							),
					"std" => "2",
					"group" 	  => __( "Slider Options", 'eshkool' ),
					
					),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Number of columns ( Small Phones < 480px )", 'eshkool' ),
					"param_name" => "col_mobile",
					"value" => array(							
								'1' => "1", 
								'2' => "2",
								'3' => "3",	
								'4' => "4",
								'5' => "5",
								'6' => "6",																						
							),
					"std" => "1",
					"group" 	  => __( "Slider Options", 'eshkool' ),
					
					),

				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Navigation Dots", 'eshkool' ),
					"param_name" => "slider_dots",
					"value" => array(
						__( 'Disabled', 'eshkool' ) => 'false',
						__( 'Enabled', 'eshkool' )  => 'true',
						),
					"description" => __( "Enable or disable navigation dots. Default: Disable", 'eshkool' ),
					"group" => __( "Slider Options", 'eshkool' ),					
					
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Autoplay", 'eshkool' ),
					"param_name" => "slider_autoplay",
					"value" => array( 
						__( "Enable", "eshkool" )  => 'true',
						__( "Disable", "eshkool" ) => 'false',
						),
					"description" => __( "Enable or disable autoplay. Default: Enable", 'eshkool' ),
					"group" => __( "Slider Options", 'eshkool' ),
					
					),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Stop on Hover", 'eshkool' ),
					"param_name" => "slider_stop_on_hover",
					"value" => array( 
						__( "Enable", "eshkool" )  => 'true',
						__( "Disable", "eshkool" ) => 'false',
						),
					'dependency' => array(
						'element' => 'slider_autoplay',
						'value'   => array( 'true' ),
						),
					"description" => __( "Stop autoplay on mouse hover. Default: Enable", 'eshkool' ),
					"group" => __( "Slider Options", 'eshkool' ),
					
				),

				array(
					"type" 		  => "dropdown",
					"holder" 	  => "div",
					"class" 	  => "",
					"heading" 	  => __( "Autoplay Interval", 'eshkool' ),
					"param_name"  => "slider_interval",
					"value" 	  => array( 
						__( "5 Seconds", "eshkool" )  => '5000',
						__( "4 Seconds", "eshkool" )  => '4000',
						__( "3 Seconds", "eshkool" )  => '3000',
						__( "2 Seconds", "eshkool" )  => '4000',
						__( "1 Seconds", "eshkool" )  => '1000',
						),
					'dependency'  => array(
						'element' => 'slider_autoplay',
						'value'   => array( 'true' ),
						),
					"description" => __( "Set any value for example 5 seconds to play it in every 5 seconds. Default: 5 Seconds", 'eshkool' ),
					"group" 	  => __( "Slider Options", 'eshkool' ),
				
				),
				array(
					"type"		  => "textfield",
					"holder" 	  => "div",
					"class" 	  => "",
					"heading" 	  => __( "Autoplay Slide Speed", 'eshkool' ),
					"param_name"  => "slider_autoplay_speed",
					"value" 	  => 200,
					'dependency'  => array(
						'element' => 'slider_autoplay',
						'value'   => array( 'true' ),
						),
					"description" => __( "Slide speed in milliseconds. Default: 200", 'eshkool' ),
					"group" 	  => __( "Slider Options", 'eshkool' ),
				
				),	
				array(
					"type" 		 => "dropdown",
					"holder" 	 => "div",
					"class" 	 => "",
					"heading" 	 => __( "Loop", 'eshkool' ),
					"param_name" => "slider_loop",
					"value" 	 => array( 
						__( "Enable", "eshkool" )  => 'true',
						__( "Disable", "eshkool" ) => 'false',
						),
					"description"=> __( "Loop to first item. Default: Enable", 'eshkool' ),
					"group" 	 => __( "Slider Options", 'eshkool' ),
					
				),
				array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Bottom Color", "eshkool" ),
                    "param_name" => "bottom_color",
                    "value" => '',
                    "description" => __( "Bottom color here", "eshkool" ),
                    'dependency' => array(
                        'element' => 'team_style',
                        'value' => array('style3', 'style5'),
                    ),
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Title Color", "eshkool" ),
                    "param_name" => "name_color",
                    "value" => '',
                    "description" => __( "Team title color", "eshkool" ),
                    'dependency' => array(
                        'element' => 'team_style',
                        'value' => array('style3', 'style5'),
                    ),
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Designation Color", "eshkool" ),
                    "param_name" => "designation_color",
                    "value" => '',
                    "description" => __( "Designation color", "eshkool" ),
                    'dependency' => array(
                        'element' => 'team_style',
                        'value' => array('style3', 'style5'),
                    ),
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Social Icon Color", "eshkool" ),
                    "param_name" => "social_color",
                    "value" => '',
                    "description" => __( "Social icon color", "eshkool" ),
                    'dependency' => array(
                        'element' => 'team_style',
                        'value' => array('style3', 'style5'),
                    ),
                ),

				array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
		     
				),     
			); 
	        return $fields;                                   
	    }    

     
    // Element HTML
    public function shortcode( $atts, $content = '' ) {
        $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'title'                 => '',
					'degination'            => '',		
					'description'           => '',	
					'team_per'              => '6',				
					'team_style'            => 'style1',
					'col_lg'                => '3',
					'col_md'                => '3',
					'col_sm'                => '3',
					'col_xs'                => '2',
					'col_mobile'            => '1',
					'slider_nav'            => 'true',
					'slider_dots'           => 'false',
					'bottom_color'          => '',
					'name_color'            => '',
					'designation_color'     => '',
					'social_color'          => '',
					'slider_autoplay'       => 'true',
					'slider_stop_on_hover'  => 'true',
					'slider_interval'       => '5000',
					'slider_autoplay_speed' => '200',
					'slider_loop'           => 'true',				
					'css'                   => '' ,
					'cat'					=> '',                    
                ), 
                $atts,'rs_team_slider'
           		)
        	);	


			$owl_data = array( 
				'nav'                => ( $slider_nav === 'true' ) ? true : false,
				'navText'            => array( "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>" ),
				'dots'               => ( $slider_dots === 'true' ) ? true : false,
				'autoplay'           => ( $slider_autoplay === 'true' ) ? true : false,
				'autoplayTimeout'    => $slider_interval,
				'autoplaySpeed'      => $slider_autoplay_speed,
				'autoplayHoverPause' => ( $slider_stop_on_hover === 'true' ) ? true : false,
				'loop'               => ( $slider_loop === 'true' ) ? true : false,
				'margin'             => 20,
				'responsive'         => array(
					'0'    => array( 'items' => $col_mobile ),
					'480'  => array( 'items' => $col_xs ),
					'768'  => array( 'items' => $col_sm ),
					'992'  => array( 'items' => $col_md ),
					'1200' => array( 'items' => $col_lg ),
				)				
			);
			$owl_data = json_encode( $owl_data );

			$bottom_color = ($bottom_color) ? ' style="background: '.$bottom_color.'"' : '';
			$name_color = ($name_color) ? ' style="color: '.$name_color.'"' : '';
			$designation_color = ($designation_color) ? ' style="color: '.$designation_color.'"' : '';
			$social_color = ($social_color) ? ' style="background: '.$social_color.'"' : '';
        

        	switch ( $team_style ) {

				case 'style2':
					$template = 'team-slider-2';
					break;

				case 'style3':
					$template = 'team-slider-3';
					break;

				case 'style4':
					$template = 'team-slider-4';
					break;
				case 'style5':
					$template = 'team-slider-5';
					break;
				case 'style6':
					$template = 'team-slider-6';
					break;

				default:
					$template = 'team-slider-1';
					break;
			}

			return $this->template( $template, get_defined_vars() );


		}
	}
}

new RSTheme_VC_Team_Slider;