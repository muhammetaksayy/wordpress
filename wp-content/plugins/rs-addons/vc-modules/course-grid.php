<?php
/*
Element Description: Course Slider
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}     
    // Element Mapping
if ( !class_exists( 'RSTheme_VC_Course_Grid' ) ) {    
         
       class RSTheme_VC_Course_Grid extends RSTheme_VC_Modules {
		public function __construct(){
			$this->name = __( "Rs Course Grid", 'rs-options' );
			$this->base = 'vc_coursegrid';				
			parent::__construct();
		}       
       
		public function fields(){

			$category_dropdown = array( __( 'All Categories', 'eshkool' ) => '0' );	
	        $args = array(
	            'taxonomy' => array('course_category'),//ur taxonomy
	            'hide_empty' => false,                  
	        );

			$terms_= new WP_Term_Query( $args );
			foreach ( (array)$terms_->terms as $term ) {
				$category_dropdown[$term->name] = $term->slug;		
			} 

        	$fields = array(


                     
					array(
						"type" => "dropdown_multi",
						"holder" => "div",
						"class" => "",
						"heading" => __( "Categories", 'eshkool' ),
						"param_name" => "cat",
						'value' => $category_dropdown,
					),
					
					array(
                        "type" => "textfield",
                        "heading" => __("Course Per Pgae", "eshkool"),
                        "param_name" => "course_per",
                        'value' =>"6",
                        'description' => __( 'You can write how many course show. ex(2)', 'eshkool' ),                 
                    ),

                    array(
                        "type"       => "dropdown",
                        "heading"    => __("Show Short Description", 'eshkool'),
                        "param_name" => "show_des",
                        "value"      => array(  
                        	'Select' => "",                                                
                            'Yes' => "Yes", 
                            'No' => "",                                                                             
                        ),                      
                    ),
                    
                    array(
                        "type" => "dropdown",
                        "heading" => __("Select Course Grid", "eshkool"),
                        "param_name" => "course_col",
                        "value" => array(   
                        	'Select Column' => '',
                        	'1 Column' => "1 Column",                        
                            '2 Column' => "2 Column", 
                            '3 Column' => "3 Column",
                            '4 Column' => "4 Column",                                                               
                        ),                                            
                    ), 
                    array(
                        "type" => "dropdown",
                        "heading" => __("Select Course Grid Style", "eshkool"),
                        "param_name" => "grid_style",
                        "value" => array(                           
                            'Style 1' => "style1", 
                            'Style 2' => "style2",                       
                            'Style 3' => "style3",
                            'Style 4' => "style4"                       
                        ),
                                        
                    ),

                    array(
						"type"        => "textfield",
						"heading"     => __("Course Offset", "eshkool"),
						"param_name"  => "offset",
						'value'       => "0",
						'description' => __( 'You can write how many course offset. ex(2)', 'eshkool' ),                 
                    ),                

                    array(
						"type"        => "textfield",
						"heading"     => __("Apply Now Text", "eshkool"),
						"param_name"  => "apply_text",
						'value'       =>"",
						'description' => __( 'Apply now text here.', 'eshkool' ),	
						"dependency"  => Array('element' => 'grid_style', 'value' => array('style1')),
					),	                

                    array(
                        "type" => "textfield",
                        "heading" => __("View All Text", "eshkool"),
                        "param_name" => "view_text",
                        'value' =>"",
                        'description' => __( 'View All Text.', 'eshkool' ),
                        "dependency" => Array('element' => 'grid_style', 'value' => array('style2', 'style3')),
                    ),  
                    
                     array(
                        "type" => "textfield",
                        "heading" => __("View All Text Link", "eshkool"),
                        "param_name" => "view_text_link",
                        'value' =>"",
                        'description' => __( 'View All Text Link.', 'eshkool' ),
                        "dependency" => Array('element' => 'grid_style', 'value' => array('style2', 'style3')),
                    ),
                    array(
						'type' => 'css_editor',
						'heading' => __( 'CSS box', 'eshkool' ),
						'param_name' => 'css',
						'group' => __( 'Design Options', 'eshkool' ),
					),     
         
        );
        return $fields;                                   
    }    
  
     
    // Element HTML
    public function shortcode( $atts, $content = '' ) {
         $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(					
                    'course_per' => '6',
                    'course_col' => '2 Column',
                    'grid_style' => 'style1',
                    'offset'     => '0',
                    'show_des'   => '',
                    'apply_text' => '',
                    'apply_link' => '',
                    'css'        => '' ,
                    'cat'        => '', 
                    'view_text' =>'',
                    'view_text_link' => '',          
                ), 
                $atts,'vc_coursegrid'
           )
        );	
        

		
		//extract css edit box
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );



        $course_col_grid ='';     
        
        //echo $course_col;
        if($course_col == '1 Column'){
            $course_col_grid = 12;
        }
        if($course_col == '2 Column'){
            $course_col_grid = 6;
        }
        if($course_col == '3 Column'){
            $course_col_grid = 4;
        }
        if($course_col == '4 Column'){
            $course_col_grid = 3;
        }       
	
        //******************//
        // query post
        //******************//
        $cat;
        $arr_cats=array();
        $arr= explode(',', $cat);  

			for ($i=0; $i < count($arr) ; $i++) { 
           	//$cats = get_term_by('slug', $arr[$i], $taxonomy);
           	// if(is_object($cats)):
           	$arr_cats[]= $arr[$i];
           	//endif;
        }
		if(empty($cat)){
        	$best_wp = new wp_Query(array(
				'post_type'           => 'lp_course',
				'posts_per_page'      =>$course_per,
				'ignore_sticky_posts' => 1,
				'offset'              => $offset
					
			));	 
		} 
        
        else{
        	$best_wp = new wp_Query(array(
					'post_type'           => 'lp_course',
					'posts_per_page'      =>$course_per,
					'ignore_sticky_posts' => 1,
					'offset'              => $offset,
					'tax_query' => array(
				        array(
							'taxonomy' => 'course_category',
							'field'    => 'slug', //can be set to ID
							'terms'    => $arr_cats//if field is ID you can reference by cat/term number
				        ),
				    )
			));	  
        }  
        
        $dir = plugin_dir_path( __FILE__ );
        $template = 'course-grid';


        switch ( $grid_style ) {

            case 'style2':
                $template = 'course-grid2';
                break;
            case 'style3':
                $template = 'course-grid3';
                break;  

            case 'style4':
                $template = 'course-grid4';
                break;             

            default:
                $template = 'course-grid';
                break;
        }

            return $this->template( $template, get_defined_vars() );

        }
	}
}

new RSTheme_VC_Course_Grid;