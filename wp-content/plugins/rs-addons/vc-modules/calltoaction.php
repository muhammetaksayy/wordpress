<?php
/*
Element Description: Rs Call To Action*/

// Element Mapping
function vc_calltoaction_mapping() {
     
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
        return;
    }
     
    // Map the block with vc_map()
    vc_map( 
        array(
            'name' => __('Rs Calltoaction', 'eshkool'),
            'base' => 'rs_calltoaction',
            'description' => __('Insert calltoaction', 'eshkool'), 
            'category' => __('by RS Theme', 'eshkool'),   
            'icon' => get_template_directory_uri().'/framework/assets/img/vc-icon.png',           
            'params' => array(     
				array(
					'type'        => 'textfield',
					'holder'      => 'h3',
					'class'       => 'title-class',
					'heading'     => __( 'Title', 'eshkool'),
					'param_name'  => 'title',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Heading title area', 'eshkool'),
					'admin_label' => false,
					'weight'      => 0,				   
				),    
				array(
					'type'        => 'textfield',
					'heading'     => __( 'SubTitle', 'eshkool'),
					'param_name'  => 'subtitle',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Heading subtitle area', 'eshkool'),
					'admin_label' => false,
					'weight'      => 0,				   
				),   
				array(
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'eshkool'),
					'param_name'  => 'content',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Description text here', 'eshkool'),                    
				),
				array(
					"type"       => "dropdown",
					"heading"    => __("Show Read More", "eshkool"),
					"param_name" => "read_btn",
				    "value" => array(
						__( 'No', 'eshkool')  => '',
						__( 'Yes', 'eshkool') => 'yes',
				    ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Read More Text', 'eshkool'),
					'param_name'  => 'rm_text',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Your read more text here', 'eshkool'),
					"dependency"  => Array('element' => 'read_btn', 'value' => array('yes')),
				),
				array(
					'type'        => 'vc_link',
					'heading'     => __( 'Read More URL (Link)', 'eshkool'),
					'param_name'  => 'read_more_link',
					'description' => __( 'Add link read more.', 'eshkool'),
					'dependency'  => array('element' => 'read_btn', 'value' => array('yes')),
				),
				array(
						"type" => "dropdown",
						"heading" => __("Select Style", "eshkool"),
						"param_name" => "slider_style",
						"value" => array(							
							'Style 1' => "style1", 
							'Style 2' => "style2"						
						),
										
					),
				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Title color", "eshkool" ),
					"param_name" => "title_color",
					"value" => '',
					"description" => __( "Choose title color", "eshkool" ),
	                'group' => 'Styles',
				),

				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Sub Title color", "eshkool" ),
					"param_name" => "subtitle_color",
					"value" => '',
					"description" => __( "Choose sub title color", "eshkool" ),
	                'group' => 'Styles',
				),


		
				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Description Color", "eshkool" ),
					"param_name" => "desc_color",
					"value" => '',
					"description" => __( "Choose description text color here", "eshkool" ),
	                'group' => 'Styles',
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Extra class name', 'eshkool'),
					'param_name'  => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool'),
				),		                     		
				array(
				'type' => 'css_editor',
				'heading' => __( 'CSS box', 'eshkool'),
				'param_name' => 'css',
				'group' => __( 'Design Options', 'eshkool'),
				),                  
                    
     		),
  		)
		);                                
    
}
  
add_action( 'vc_before_init', 'vc_calltoaction_mapping' );
  
// Element HTML
function rs_calltoaction_html($atts, $content) {
         
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'style'          => '',
					'title'          => '',
					'subtitle'       => '',
					'title_color'    => '',
					'subtitle_color' => '',
					'desc_color'     => '',
					'align'          => '',
					'read_btn'       => '',
					'rm_text'        => '',
					'read_more_link' => '',
					'el_class'       =>'',
					'css'            => ''
                ), 
                $atts, 'rs_calltoaction'
            )
        );


       //parse link for vc linke		
		$read_more_link = ( '||' === $read_more_link ) ? '' : $read_more_link;
		$read_more_link = vc_build_link( $read_more_link );
		$use_link = false;
		if ( strlen( $read_more_link['url'] ) > 0 ) {
			$use_link = true;
			$a_href = $read_more_link['url'];
			$a_title = $read_more_link['title'];
			$a_target = $read_more_link['target'];
		}
		
		if ( $use_link ) {
			$attributes[] = 'href="' . esc_url( trim( $a_href ) ) . '"';
			$attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
			if ( ! empty( $a_target ) ) {
				$attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
			}
		}
		$attributes = implode( ' ', $attributes );     



		//for css edit box value extract
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
		
         //custom class added
		$wrapper_classes = array($el_class) ;			
		$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );		
		$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );

		$title_color = ($title_color) ? ' style="color: '.$title_color.'"' : '';
		$desc_color = ($desc_color) ? ' style="color: '.$desc_color.'"' : '';

		$subtitle_color = ($subtitle_color) ? ' style="color: '.$subtitle_color.'"' : '';

		$sub_title = ($subtitle) ? '<span'.$subtitle_color.'>'.wp_kses_post($subtitle).'</span>' : '';
		$main_title = ($title) ? '<h2'.$title_color.'>'.wp_kses_post($title).'</h2>' : '';
        

        $html = '
        	<div class="rs-calltoaction">
        	    <div class="rs-heading style1 center">
        	        '.$sub_title.'
        	        '.$main_title.'
        	        <p '.$desc_color.'>'.$content.'</p>
        	    </div>';

        	    if ($read_btn == 'yes') {
        	    	$html .= '<a class="cta-button" '.$attributes.'>'.$rm_text.'</a>';        	    	
        	    }

        	$html .= '</div>';
         
        return $html;         
    }
add_shortcode( 'rs_calltoaction', 'rs_calltoaction_html' );