<?php
/*
Element Description: Rs Custom Heading*/

// Element Mapping
function vc_infobox_mapping() {
     
    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
        return;
    }
     
    // Map the block with vc_map()
    vc_map( 
        array(
            'name' => __('Rs Heading', 'eshkool'),
            'base' => 'vc_infobox',
            'description' => __('Rs heading box', 'eshkool'), 
            'category' => __('by RS Theme', 'eshkool'),   
            'icon' => get_template_directory_uri().'/framework/assets/img/vc-icon.png',           
            'params' => array(   
                array(
                    "type" => "dropdown",
                    "heading" => __("Select Style", "eshkool"),
                    "param_name" => "style",
                    "value" => array(
                        __( 'Default', 'eshkool')  => '',
                        __( 'Style 1', 'eshkool')   => 'style1',
                        __( 'Style 2', 'eshkool') => 'style2',
                        __( 'Style 3', 'eshkool') => 'style3',
                        __( 'Style 4', 'eshkool') => 'style4',
                        __( 'Style 5', 'eshkool') => 'style5',
                    ),
                ), 

                array(
					"type"        => "attach_image",
					"heading"     => __( "Heading Image", "eshkool" ),
					"description" => __( "Add Heading image", "eshkool" ),
					"param_name"  => "headingbg",
					"value"       => "",
					"dependency" => Array('element' => 'style', 'value' => array('style3')),
				),

				array(
					'type'        => 'textfield',
					'holder'      => 'h3',
					'class'       => 'title-class',
					'heading'     => __( 'Title', 'eshkool'),
					'param_name'  => 'title',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Heading title area', 'eshkool'),
					'admin_label' => false,
					'weight'      => 0,				   
				),  
				 
				array(
					'type'        => 'textfield',
					'holder'      => 'h4',
					'class'       => 'text-class',
					'heading'     => __( 'Subtitle', 'eshkool'),
					'param_name'  => 'sub_text',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Sub title text here', 'eshkool'),
					'admin_label' => false,
					'weight'      => 0,                        
				),

				array(
					'type'        => 'textfield',
					'heading'     => __( 'Watermark Text', 'eshkool'),
					'param_name'  => 'watermark',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Watermark text here', 'eshkool'),                   
				),	

				array(
					'type'        => 'textarea_html',
					'heading'     => __( 'Text', 'eshkool'),
					'param_name'  => 'content',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Description text here', 'eshkool'),                    
				),
				array(
					"type"       => "dropdown",
					"heading"    => __("Show Read More", "eshkool"),
					"param_name" => "read_btn",
				    "value" => array(
						__( 'No', 'eshkool')  => '',
						__( 'Yes', 'eshkool') => 'yes',
				    ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Read More Text', 'eshkool'),
					'param_name'  => 'rm_text',
					'value'       => __( '', 'eshkool'),
					'description' => __( 'Your read more text here', 'eshkool'),
					"dependency"  => Array('element' => 'read_btn', 'value' => array('yes')),
				),
				array(
					'type'        => 'vc_link',
					'heading'     => __( 'Read More URL (Link)', 'eshkool'),
					'param_name'  => 'read_more_link',
					'description' => __( 'Add link read more.', 'eshkool'),
					'dependency'  => array('element' => 'read_btn', 'value' => array('yes')),
				),
				array(
				    "type" => "dropdown",
				    "heading" => __("Select Align", "eshkool"),
				    "param_name" => "align",
				    "value" => array(
				        __( 'Left', 'eshkool')   => '',
				        __( 'Center', 'eshkool') => 'center',
				        __( 'Right', 'eshkool')  => 'right',
				    ),
				),
				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Title color", "eshkool" ),
					"param_name" => "title_color",
					"value" => '',
					"description" => __( "Choose title color", "eshkool" ),
	                'group' => 'Styles',
				),

				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Sub Text Color", "eshkool" ),
					"param_name" => "sub_text_color",
					"value" => '',
					"description" => __( "Choose sub text color here", "eshkool" ),
	                'group' => 'Styles',
				),
				array(
					"type" => "colorpicker",
					"class" => "",
					"heading" => __( "Description Color", "eshkool" ),
					"param_name" => "desc_color",
					"value" => '',
					"description" => __( "Choose description text color here", "eshkool" ),
	                'group' => 'Styles',
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Extra class name', 'eshkool'),
					'param_name'  => 'el_class',
					'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'eshkool'),
				),		                     
							
				array(
				'type' => 'css_editor',
				'heading' => __( 'CSS box', 'eshkool'),
				'param_name' => 'css',
				'group' => __( 'Design Options', 'eshkool'),
				),                  
                    
     		),
  		)
		);                                
    
}
  
add_action( 'vc_before_init', 'vc_infobox_mapping' );
  
// Element HTML
function vc_infobox_html($atts, $content) {
         
        // Params extraction
        extract(
            shortcode_atts(
                array(
					'style'          => '',
					'title'          => '',
					'title_color'    => '',
					'sub_text'       => '',
					'sub_text_color' => '',
					'desc_color'     => '',
					'watermark'      => '',
					'headingbg'      => '',
					'description'    => '',
					'align'          => '',
					'read_btn'       => '',
					'rm_text'        => '',
					'read_more_link' => '',
					'el_class'       =>'',
					'css'            => ''
                ), 
                $atts, 'vc_infobox'
            )
        );
         $a = shortcode_atts(array(
          'headingbg' => 'headingbg',
        ), $atts);

        $img = wp_get_attachment_image_src($a["headingbg"], "large");
        $imgSrc = $img[0];	

         //parse link for vc linke		
		$read_more_link = ( '||' === $read_more_link ) ? '' : $read_more_link;
		$read_more_link = vc_build_link( $read_more_link );
		$use_link = false;
		if ( strlen( $read_more_link['url'] ) > 0 ) {
			$use_link = true;
			$a_href = $read_more_link['url'];
			$a_title = $read_more_link['title'];
			$a_target = $read_more_link['target'];
		}
		
		if ( $use_link ) {
			$attributes[] = 'href="' . esc_url( trim( $a_href ) ) . '"';
			$attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
			if ( ! empty( $a_target ) ) {
				$attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
			}
		}
		 $attributes="";
        $attributes = ($attributes) ? implode( ' ', $attributes ) : '';

		//for css edit box value extract
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $atts );
		
         //custom class added
		$wrapper_classes  = array($el_class) ;			
		$class_to_filter  = implode( ' ', array_filter( $wrapper_classes ) );		
		$css_class_custom = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $atts );
		
		$title_color      = ($title_color) ? ' style="color: '.$title_color.'"' : '';
		$sub_text_color   = ($sub_text_color) ? ' style="color: '.$sub_text_color.'"' : '';
		$desc_color       = ($desc_color) ? ' style="color: '.$desc_color.'"' : '';
		$titlebg          = $imgSrc ? 'style=background:url('.$imgSrc.')' : '';
		
		
		$watermark_text   = ($watermark) ? '<span class="watermark">'.wp_kses_post($watermark).'</span>' : '';
		$main_title       = ($title) ? '<h2'.$title_color.' '.$titlebg.'>'.$watermark_text.''.wp_kses_post($title).'</h2>' : '';
		$sub_text         = ($sub_text) ? '<span'.$sub_text_color.' class="sub-text">'.wp_kses_post($sub_text).'</span>' : '';
		
        // Fill $html var with data
        $html = '
        <div class="rs-heading '.$style.' '.$css_class.' '.$css_class_custom.' '.$align.'">
        	<div class="title-inner">
	            '.$sub_text.'
	            '.$main_title.'
	        </div>';
	        if ($content) {
            	$html .= '<div class="description"><p '.$desc_color.'>'.$content.'</p></div>';
        	}
        	if ($read_btn == 'yes') {
	        	$html .= '<div class="view-more">
            		<a '.$attributes.'>'.$rm_text.' <i class="fa fa-angle-double-right"></i></a>
            	</div>';
            }

        $html .= '</div>';  	
         
        return $html;         
    }
add_shortcode( 'vc_infobox', 'vc_infobox_html' );