<?php
/*
Element Description: Rs Team Box
*/
 if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
}     
// Element Mapping
if ( !class_exists( 'RSTheme_VC_Event_Lastest' ) ) {    
         
    class RSTheme_VC_Event_Lastest extends RSTheme_VC_Modules {
		public function __construct(){
			$this->name = __( "Rs Latest Event", 'eshkool' );
			$this->base = 'rs_event';				
			parent::__construct();
		} 

		
		public function fields(){
			$category_dropdown = array( __( 'All Categories', 'eshkool' ) => '0' );	
	        $args = array(
	            'taxonomy' => array('event-category'),//ur taxonomy
	            'hide_empty' => false,                  
	        );

			$terms_= new WP_Term_Query( $args );
			foreach ( (array)$terms_->terms as $term ) {
				$category_dropdown[$term->name] = $term->slug;		
			} 

			$fields = array(

				array(
                    "type" => "dropdown",
                    "heading" => __("Select Event Grid Style", "eshkool"),
                    "param_name" => "event_style",
                    "value" => array(                           
                        'Style 1' => "style1", 
                        'Style 2' => "style2"                     
                    ),
                                    
                ), 
				array(
					"type"        => "textfield",
					"heading"     => __("Event Per Pgae", "eshkool"),
					"param_name"  => "team_per",
					'value'       =>"6",
					'description' => __( 'You can write how many event show. ex(2)', 'eshkool' ),	
					'admin'       => false				
				),	

				array(
					"type" => "dropdown_multi",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Categories", 'eshkool' ),
					"param_name" => "cat",
					'value' => $category_dropdown,
					'admin'       => false	
				),

				array(
					"type"        => "textfield",
					"heading"     => __("Button text", "eshkool"),
					"param_name"  => "event_text"			
				),

				array(
					"type"        => "textfield",
					"heading"     => __("Button link", "eshkool"),
					"param_name"  => "event_link"			
				),	
	

				
				array(
					'type' => 'css_editor',
					'heading' => __( 'CSS box', 'eshkool' ),
					'param_name' => 'css',
					'group' => __( 'Design Options', 'eshkool' ),
		     
				),     
			); 
	        return $fields;                                   
	    }    

     
    // Element HTML
    public function shortcode( $atts, $content = '' ) {
        $attributes = array();
        // Params extraction
        extract(
            shortcode_atts(
                array(									
					'team_per'              => '6',									
					'css'                   => '' ,
					'cat'					=> '', 
					'event_style'			=> '',
					'event_text'			=> '',
					'event_link'		    => ''			                   
                ), 
                $atts,'rs_event'
           		)
        	);	


			
        
			$dir = plugin_dir_path( __FILE__ );
			switch ( $event_style ) {

	            case 'style2':
	                $template = 'latest-event2';
	                break;            

	            default:
	                $template = 'latest-event';
	                break;
	        }
			return $this->template( $template, get_defined_vars() );

		}
	}
}

new RSTheme_VC_Event_Lastest;