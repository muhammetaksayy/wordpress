<?php 
// Register and load the widget
function eshkool_course_category_widget() {
    register_widget( 'course_category_widget' );
}
add_action( 'widgets_init', 'eshkool_course_category_widget' );

//Contact info Widget 
class course_category_widget extends WP_Widget {
 
   /** constructor */
   function __construct() {
    parent::__construct(
      'course_category_widget', 
      __('Eshkool Course Caegory Info', 'eshkool'),
      array( 'description' => __( 'Display your course category info!', 'eshkool' ), )
    );
  }
 
    /** @see WP_Widget::widget */
    function widget($args, $instance) { 
        extract( $args );
        $title    = apply_filters('widget_title', $instance['title']);      
        echo ($before_widget); 
        if ( $title )
        echo ($before_title . $title . $after_title); ?>
 
      <?php 
          $args = array(
              'taxonomy' => array('course_category'),//ur taxonomy
              'hide_empty' => false,                  
          );

      $terms_= new WP_Term_Query( $args );
      foreach ( (array)$terms_->terms as $term ) {
        $arr[] = $term->slug;
        $arr_name[]=$term->name;    
      } 
      echo '<ul>';
      for ($i=0; $i < count($arr) ; $i++) { 
          $args = array(
             'post_type' => 'lp_course',
             'tax_query' => array(
                  array(
                    'taxonomy' => 'course_category',
                    'field' => 'slug',
                    'terms' => $arr[$i],
                  )
              )
          );
        $obj_name = new WP_Query($args);
        $n = $obj_name->post_count; 
        $term_name = get_term_by('slug', $arr[$i], 'course_category');  

        $term_link = get_term_link( $term_name);
        $term_id="";      
        $term_id= $term_name->term_id;
         wp_reset_query();
         echo '<li>
                <a href="'.esc_url($term_link).'">'.esc_attr($arr_name[$i]).'</a>
                <span class="courses-cat-amount">('.esc_attr($n).')</span>
            
        </li>';
      } 
      echo '</ul>';
      echo wp_kses_post($after_widget); ?>
     <?php
    }
 
  /** @see WP_Widget::update  */
  function update($new_instance, $old_instance) {   
        $instance            = $old_instance;
        $instance['title']   = strip_tags($new_instance['title']);
       
        return $instance;
    }
 
    /** @see WP_Widget::form */
    function form($instance) {  
		
     $title   = ($instance['title'])? $instance['title'] : '';    
    
     ?>
      
        <p>
          <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'eshkool'); ?></label> 
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title); ?>" />
        </p>        
              
       
        <?php 
    }

 
} // end class
