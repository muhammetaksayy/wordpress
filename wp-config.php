<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'eltturkey_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345678' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fZ*_Yb@<7!qDFG8-}i9=CgiV&l2Yob%_3-VQGw?-RyH?i:?^#v~W?5x`.?Z{lNMK' );
define( 'SECURE_AUTH_KEY',  '^-O/EbI!f,%~A.]:>dpsd9x|L]ie+o(o<Lk!,,8X(QDlsof;Z0j`yJ (g+=]O:Zw' );
define( 'LOGGED_IN_KEY',    'm+tlx_lM,a+=*`RM tB#?GzDxh{kn|9qHVBLK{3/SwpHkA(,q=H> 2UJ!J049=6.' );
define( 'NONCE_KEY',        'BIiDX$%7xCe3q hC%hem5J8{E+*Jf7j-WdX!i? B;n[04aPL^qYnK~jN9;LYY|x_' );
define( 'AUTH_SALT',        'Jbn;4{^ 0v?tZUtX!mOdQ9y-O][e9T Ry.Cu@D19>],b+e3A]MCIrS1Eh.I@;yWl' );
define( 'SECURE_AUTH_SALT', 'F|O]^qx0L$yya*iPG#9dHG{|H6<vIko(!ko:af,)c 0s^i[~(RqOE4!~N4[-wyRw' );
define( 'LOGGED_IN_SALT',   'nMz9?W_>h2L_ E*lkc<PoVTtw5p;b),=!ON%cNvKk<J8;WZlG_M*wfzMw%NK=2Ww' );
define( 'NONCE_SALT',       'b;#ke9H1+$GG#0ZF0R(WNq.w>+#|(z)*y^1CtYU&}Hi3O_wI9$icGHm&S3VHe8Ez' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
